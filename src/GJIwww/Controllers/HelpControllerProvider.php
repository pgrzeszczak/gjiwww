<?php
namespace GJIwww\Controllers;

use Silex\Application;
use Silex\ControllerProviderInterface;

class HelpControllerProvider implements ControllerProviderInterface
{
    public function connect(Application $app)
    {
        $controllers = $app['controllers_factory'];

        $controllers->get('/faq', function (Application $app) {            
            return $app['twig']->render('User/faq.html.twig');
        })->bind('faq');
        $controllers->get('/contact', function (Application $app) {            
            return $app['twig']->render('User/contact.html.twig');
        })->bind('contact');
        $controllers->get('/', function (Application $app) {            
            return $app['twig']->render('User/help.html.twig');
        })->bind('help');

        return $controllers;
    }
}
