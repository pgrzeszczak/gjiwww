<?php

function registerParameters(Silex\Application $app) {
    $app['params.debug_mode'] = true;
    $app['params.assets_relative_path'] = '/public';
    $app['params.assets_path'] = __DIR__ . '/../web' . $app['params.assets_relative_path'];
    $app['params.assets_src_path'] = __DIR__ . '/../src/resources';
    $app['params.views_path'] = __DIR__ . '/../src/views';
    $app['params.trans_path'] = __DIR__ . '/../src/translations';
    $app['params.entity_path'] = __DIR__ . '/../src/GJIwww/Entities';
    $app['params.user_assets_path'] = '/GJIwww/userAssets';
    $app['params.assets_scenario_path'] = 'assets';
}

function registerConstants() {
    define("DEBUG_MODE", TRUE);
    define("ASSETS_RELATIVE_PATH", '/public');
    define("ASSETS_PATH", __DIR__ . '/../web' . ASSETS_RELATIVE_PATH);
    define("ASSETS_SRC_PATH", __DIR__ . '/../src/resources');
    define("VIEWS_PATH", __DIR__ . '/../src/views');
    define("TRANS_PATH", __DIR__ . '/../src/translations');
    define("ENTITY_PATH",  __DIR__ . '/../src/GJIwww/Entities');
    define("USER_ASSETS_PATH",  '/GJIwww/userAssets');
    define("ASSETS_SCENARIO_PATH", 'assets');
}