<?php
require_once __DIR__.'/../vendor/autoload.php';

$app = require __DIR__.'/../app/registerProviders.php';

$app->before(function () use ($app) {
    if ($app["session"]->has("locale") && $app["session"]->get("locale") !== null) {
        $app["locale"] = $app["session"]->get("locale");
    } else {
        $app["locale"] = 'pl';
    }
    $app['twig']->addGlobal('assetsPath', $app['request']->getBasePath() . $app['params.assets_relative_path'] . '/');
    $app['twig']->addGlobal('assetsScenarioPath', $app['params.assets_scenario_path'] .'/');
});

// definitions

$app->get('/hello/{name}', function($name) use($app) { 
    return $app['twig']->render('hello.html.twig', array(
        'name' => $name,
    ));
})->bind('test_hello'); //tego nie będzie

$app->mount('/creator', new \GJIwww\Controllers\CreatorControllerProvider());
$app->mount('/help', new \GJIwww\Controllers\HelpControllerProvider());
$app->mount('/assets', new \GJIwww\Controllers\AssetsControllerProvider());
$app->mount('/mobile', new \GJIwww\Controllers\MobileControllerProvider());
$app->mount('/', new \GJIwww\Controllers\ScenarioControllerProvider());
$app->mount('/', new \GJIwww\Controllers\HomeControllerProvider());

$app->run();
