<?php
namespace GJIwww\Controllers;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\Response;

class ScenarioControllerProvider implements ControllerProviderInterface
{
    public function connect(Application $app)
    {
        $controllers = $app['controllers_factory'];

        $controllers->get('/scenario/{id}', function (Application $app, $id) {
            $scenario = $app['db.orm.em']->getRepository('GJIwww\Entities\Scenario')->find($id);
            $user = null;
            if ($app['security']->isGranted('IS_AUTHENTICATED_FULLY')) {
                $user = $app['security']->getToken()->getUser();
            }
            if ($scenario === null) {
                $app->abort(404);
            }            
            if ($scenario->isPrivate()) {
                if (!$app['security']->isGranted('IS_AUTHENTICATED_FULLY')) {
                    $app->abort(403);
                }
                if ($scenario->getAuthor()->getId() !== $user->getId()) {
                    $app->abort(403);
                }
            }
            return $app['twig']->render('User/scenario.html.twig', array('scenario' => $scenario, 'user' => $user));
        })->bind('scenario');
        
        $controllers->get('/scenariolist', function (Application $app) {
            $user = $app['security']->getToken()->getUser();
            $scenarios = $app['db.orm.em']->getRepository('GJIwww\Entities\Scenario')->getUserScenariosFull($user);
            return $app['twig']->render('User/scenarioList.html.twig', array('scenarios' => $scenarios, 'user' => $user));
        })->bind('scenariolist');
        
        $controllers->get('/publiclist', function (Application $app) {
            $user = null;
            if ($app['security']->isGranted('IS_AUTHENTICATED_FULLY')) {
                $user = $app['security']->getToken()->getUser();
            }
            $scenarios = $app['db.orm.em']->getRepository('GJIwww\Entities\Scenario')->getPublicScenariosFull();
            return $app['twig']->render('User/scenarioPublicList.html.twig', array('scenarios' => $scenarios, 'user' => $user));
        })->bind('publiclist');
        
        $controllers->get('/recent', function (Application $app) {
            $scenarios = $app['db.orm.em']->getRepository('GJIwww\Entities\Scenario')->getRecentScenarios();
            return $app['twig']->render('Scenario/recent.html.twig', array('scenarios'=>$scenarios));
        })->bind('recent_scenarios');
        
        $controllers->get('/getsplash/{id}', function (Application $app, $id) {
            $scenario = $app['db.orm.em']->getRepository('GJIwww\Entities\Scenario')->find($id);
            if ($scenario === null) {
                $app->abort(404);
            }            
            if ($scenario->isPrivate()) {
                if (!$app['security']->isGranted('IS_AUTHENTICATED_FULLY')) {
                    $app->abort(403);
                }
                $user = $app['security']->getToken()->getUser();
                if ($scenario->getAuthor()->getId() !== $user->getId()) {
                    $app->abort(403);
                }
            }
            $asset = $scenario->getSplashAsset();
            if ($asset === null) {
                $app->abort(404);
            }
            $length = filesize($asset->getAbsolutePath($app));
            $content = file_get_contents($asset->getAbsolutePath($app));
            $headers = array('Content-Type' => $asset->getMimeType(),
                             'Content-Length' => $length,
                             'Content-Disposition' => 'inline; filename="splash' . $id . '.' . $asset->getFileExtension() . '"');
            return new Response($content, 200, $headers);
        })->bind('www_getsplash');

        return $controllers;
    }
}
