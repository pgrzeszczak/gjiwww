<?php

namespace GJIwww\Entities;

/**
 * @Table(name="asset")
 * @Entity(repositoryClass="AssetRepository")
 */
class Asset {
    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @var string
     * @Column(name="path", type="string")
     */
    protected $path;
    /**
     * @ManyToOne(targetEntity="GJIwww\Entities\User", inversedBy="assets")
     * @JoinColumn(name="owner_id", referencedColumnName="id")
     */
    protected $owner;
    /**
     * @var string
     * @Column(name="name", type="string")
     */
    protected $name;
    /**
     * @var string
     * @Column(name="mime_type", type="string", nullable=true)
     */
    protected $mimeType;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set path
     *
     * @param string $path
     * @return Asset
     */
    public function setPath($path)
    {
        $this->path = $path;
    
        return $this;
    }

    /**
     * Get path
     *
     * @return string 
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Asset
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * Set owner
     *
     * @param User $owner
     * @return Asset
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;
    
        return $this;
    }

    /**
     * Get owner
     *
     * @return User 
     */
    public function getOwner()
    {
        return $this->owner;
    }
    
    /**
     * Set mimeType
     *
     * @param string $mimeType
     * @return Asset
     */
    public function setMimeType($mimeType)
    {
        $this->mimeType = $mimeType;
    
        return $this;
    }

    /**
     * Get mimeType
     *
     * @return string 
     */
    public function getMimeType()
    {
        return $this->mimeType;
    }
    
    public function getAbsolutePath(\Silex\Application $app)
    {
        return $this->getOwner()->getAssetsPath($app) . '/' . $this->getPath();
    }
    
    public function getFileType()
    {
        $mimeType = $this->getMimeType();
        $type = explode('/', $mimeType);
        if (!empty($type) && in_array($type[0], array('audio', 'video', 'image'))) {
            return $type[0];
        } else {
            return 'unknown';
        }
    }
    
    public function getFileExtension()
    {
        return pathinfo($this->getPath(), PATHINFO_EXTENSION);
    }
}