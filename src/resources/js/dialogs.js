/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

var ZoneDialogContent;
var zedytowalemZone=false;
var conditionSpan=false;
var conditionSpanEdit=false;
var closeDialogValue=false;
var assetSpan=false;
function createEditZone(dialog){
    var idname=$(dialog).find("#idname");
    var name=$(dialog).find("#name");
    var visible=$(dialog).find("#visibility").is(':checked');
    var state=$(dialog).find("#state");
    var description=$(dialog).find("#description");
    var icon=$(dialog).find("span.choose-media").attr("data-value")
    var owned=$(dialog).find("#owned").is(':checked');
    var points="s";
    var message=$('').html("s");
    var isValid=true;
    $("div.error", dialog).empty();
    $(".ui-state-error", dialog).removeClass( "ui-state-error" );
    
            
                
    isValid=isValid && validLength($("label[for=\"idname\"]", dialog).text(),idname,1,100,$(dialog).find("div.error"));
    isValid=isValid && validLength($("label[for=\"name\"]", dialog).text(),name,1,100,$(dialog).find("div.error"));
    isValid=isValid && validLength($("label[for=\"description\"]", dialog).text(),description,1,1000,$(dialog).find("div.error"));
 
    if(isValid){
        if(edytowanaZona>=0){
            edytowanyObject.idname=idname.val();
            edytowanyObject.name=name.val();
            edytowanyObject.visible=visible;
            edytowanyObject.description=description.val();
            edytowanyObject.state=state.val();
            edytowanyObject.poly=edytowanyPolygon;
            //alert(edytowanyPolygon);
            edytowanyObject.icon=icon;
            edytowanyObject.owned=owned;
            zedytowalemZone=true;
        }
        else{
            //alert("ed: "+edytowanyPolygon + " "+edytowanyPolygon.getPath().getLength());
            Zones.push(new Zone(idname.val(), name.val(), description.val(), state.val(), visible,edytowanyPolygon,owned,icon));
            //alert("zn: "+Zones[0].poly+ " "+Zones[0].poly.getPath().getLength());
        }
        showZoneList(); 
    }
    return isValid;
}

function createEditCharacter(dialog){
    var idname=$(dialog).find("#idname");
    var name=$(dialog).find("#name");
    var visible=$(dialog).find("#visibility").is(':checked');
    var owned=$(dialog).find("#owned").is(':checked');
    var state=$(dialog).find("#state");
    var description=$(dialog).find("#description");
    var zone=$(dialog).find("span.choose-zone").attr("data-value")
    var icon=$(dialog).find("span.choose-media").attr("data-value")
    var sex=$("#sex option:selected", dialog);
    var isValid=true;
    
    $("div.error", dialog).empty();
    $(".ui-state-error", dialog).removeClass( "ui-state-error" );
    isValid=isValid && validLength($("label[for=\"idname\"]", dialog).text(),idname,1,100,$(dialog).find("div.error"));
    isValid=isValid && validLength($("label[for=\"name\"]", dialog).text(),name,1,100,$(dialog).find("div.error"));
    isValid=isValid && validLength($("label[for=\"description\"]", dialog).text(),description,1,1000,$(dialog).find("div.error"));
    
    //isValid=isValid && validLength($("label[for=\"state\"]", dialog).text(),state,1,1000,$(dialog).find("div.error"));
    
    isValid=isValid && validLength($("label[for=\"sex\"]", dialog).text(),sex,1,1000,$(dialog).find("div.error"));
    if(zone==null){
        $("div.error", dialog).append("Wybierz strefę")
        isValid=false;
    }
    
    if(isValid){
        if(edytowanyCharacter>=0){
            edytowanyObject.idname=idname.val();
            edytowanyObject.name=name.val();
            edytowanyObject.visible=visible;
            edytowanyObject.description=description.val();
            edytowanyObject.sex=sex.val();
            edytowanyObject.state=state.val();
            edytowanyObject.zone=zone;
            edytowanyObject.icon=icon;
            edytowanyObject.owned=owned;
            edytowanyCharacter=-1;
        }
        else{
            Characters.push(new Character(idname.val(), name.val(), sex.val(), description.val(), zone, state.val(), visible,owned,icon));
        }
        showCharacterList();    
    }
    return isValid;
}

function createEditItem(dialog){
    var idname=$(dialog).find("#idname");
    var name=$(dialog).find("#name");
    var visible=$(dialog).find("#visibility").is(':checked');
    var state=$(dialog).find("#state");
    var description=$(dialog).find("#description");
    var zone=$(dialog).find("span.choose-zone").attr("data-value")
    var icon=$(dialog).find("span.choose-media").attr("data-value")
    var owned=$(dialog).find("#owned").is(':checked');
    var isValid=true;
    
    $("div.error", dialog).empty();
    $(".ui-state-error", dialog).removeClass( "ui-state-error" );
    isValid=isValid && validLength($("label[for=\"idname\"]", dialog).text(),idname,1,100,$(dialog).find("div.error"));
    isValid=isValid && validLength($("label[for=\"name\"]", dialog).text(),name,1,100,$(dialog).find("div.error"));
    isValid=isValid && validLength($("label[for=\"description\"]", dialog).text(),description,1,1000,$(dialog).find("div.error"));
    // isValid=isValid && validLength($("label[for=\"state\"]", dialog).text(),state,1,1000,$(dialog).find("div.error"));
    if(zone==null){
        $("div.error", dialog).append("Wybierz strefę")
        isValid=false;
    }
    if(isValid){
        if(edytowanyItem>=0){
            edytowanyObject.idname=idname.val();
            edytowanyObject.name=name.val();
            edytowanyObject.visible=visible;
            edytowanyObject.description=description.val();
            edytowanyObject.state=state.val();
            edytowanyObject.zone=zone;
            edytowanyObject.icon=icon;
            edytowanyObject.owned=owned;
            edytowanyItem=-1;
        }
        else{
            Items.push(new Item(idname.val(), name.val(), description.val(), zone, visible, state.val(),owned,icon));
        }
        showItemList();   
    }
    return isValid;
}
function createEditQuest(dialog){
    var idname=$(dialog).find("#idname");
    var name=$(dialog).find("#name");
    var visible=$(dialog).find("#visibility").is(':checked');
    var state=$(dialog).find("#state");
    var description=$(dialog).find("#description");
    var icon=$(dialog).find("span.choose-media").attr("data-value")
    var zone=$(dialog).find("#zone");
    var isValid=true;
    
    $("div.error", dialog).empty();
    $(".ui-state-error", dialog).removeClass( "ui-state-error" );
    isValid=isValid && validLength($("label[for=\"idname\"]", dialog).text(),idname,1,100,$(dialog).find("div.error"));
    isValid=isValid && validLength($("label[for=\"name\"]", dialog).text(),name,1,100,$(dialog).find("div.error"));
    isValid=isValid && validLength($("label[for=\"description\"]", dialog).text(),description,1,1000,$(dialog).find("div.error"));
    //isValid=isValid && validLength($("label[for=\"state\"]", dialog).text(),state,1,1000,$(dialog).find("div.error"));
  
    if(isValid){ 
        if(edytowanyQuest>=0){
            edytowanyObject.idname=idname.val();
            edytowanyObject.name=name.val();
            edytowanyObject.visible=visible;
            edytowanyObject.description=description.val();
            edytowanyObject.state=state.val();
            edytowanyObject.zone=zone.val();
            edytowanyObject.icon=icon;
            edytowanyQuest=-1;
        }
        else{
            Quests.push(new Quest(idname.val(), name.val(), description.val(), state.val(),visible,icon));
        }
        showQuestList(); 
    }  
    return isValid;
}
 
function createEditMedia(dialog){
    var idname=$(dialog).find("#idname");
    //var path=$(dialog).find("#path");
    
    var pathid=$("#dialog-media .mediaSpan").attr("data-id");
    var pathname= $("#dialog-media .mediaSpan").text();
    var type=$("#type option:selected", dialog);   
    var isValid=true;
    
    //  $(pathid).val(pathid)
    $("div.error", dialog).empty();
    $(".ui-state-error", dialog).removeClass( "ui-state-error" );
    if(pathname.length<=0){
        $("div.error", dialog).append($("#choosefile-error-caption").text())
        return false;
    }
    isValid=isValid && validLength($("label[for=\"idname\"]", dialog).text(),idname,1,100,$(dialog).find("div.error"));
    isValid=isValid && validLength($("label[for=\"type\"]", dialog).text(),type,1,1000,$(dialog).find("div.error"));
    // isValid=isValid && validLength($("label[for=\"path\"]", dialog).text(),pathid,1,1000,$(dialog).find("div.error"));
    /* isValid=isValid && validLength($("label[for=\"name\"]", dialog).text(),name,1,100,$(dialog).find("div.error"));
    isValid=isValid && validLength($("label[for=\"description\"]", dialog).text(),description,1,1000,$(dialog).find("div.error"));
     */ 
    
    //   $(pathid).val("")
    if(isValid){
        if(edytowaneMedia>=0){
            edytowanyObject.idname=idname.val();
            edytowanyObject.type=type.val();
            edytowanyObject.pathid=pathid;
            edytowanyObject.pathname=pathname;
            edytowaneMedia=-1;
        }
        else{
            Medias.push(new Media(idname.val(), pathid, pathname, type.val()));
        }
        showMediaList();    
    }
    return isValid;
}
function createEditTimer(dialog){
    var idname=$(dialog).find("#idname");
    var interval=$(dialog).find("#time"); 
    var isValid=true;
    
    $("div.error", dialog).empty();
    $(".ui-state-error", dialog).removeClass( "ui-state-error" );
    isValid=isValid && validLength($("label[for=\"idname\"]", dialog).text(),idname,1,100,$(dialog).find("div.error"));
    isValid=isValid && validLength($("label[for=\"type\"]", dialog).text(),interval,1,1000,$(dialog).find("div.error"));
    /* isValid=isValid && validLength($("label[for=\"name\"]", dialog).text(),name,1,100,$(dialog).find("div.error"));
    isValid=isValid && validLength($("label[for=\"description\"]", dialog).text(),description,1,1000,$(dialog).find("div.error"));
     */ 
    if(isValid){
        if(edytowanyTimer>=0){
            edytowanyObject.idname=idname.val();
            edytowanyObject.type=interval.val();
            edytowanyTimer=-1;
        }
        else{
            Timers.push(new Timer(idname.val(), interval.val()));
        }
        showTimerList();    
    }
    return isValid;
}
function createEditVariable(dialog){
    var idname=$(dialog).find("#idname");
    var type=$(dialog).find("#variable-type"); 
    var isValid=true;
    var value=null;
    
    $("div.error", dialog).empty();
    $(".ui-state-error", dialog).removeClass( "ui-state-error" );
    isValid=isValid && validLength($("label[for=\"idname\"]", dialog).text(),idname,1,100,$(dialog).find("div.error"));
    isValid=isValid && validLength($("label[for=\"variable-type\"]", dialog).text(),type,1,1000,$(dialog).find("div.error"));
    /* isValid=isValid && validLength($("label[for=\"name\"]", dialog).text(),name,1,100,$(dialog).find("div.error"));
    isValid=isValid && validLength($("label[for=\"description\"]", dialog).text(),description,1,1000,$(dialog).find("div.error"));
     */
    var bool=false;
    if(type.val()=="INTEGER"){
        value=$("#number-variable").val();
    }
    else if(type.val()=="BOOLEAN"){
        value=$("#checkbox-variable").is(":checked");
        bool=true;
    }
    else{
        value=$("#text-variable").val();
              
    }
    if(value==null || value=="") {
        if(!bool){
            isValid=false;
            $("div.error",dialog).append("Uzupełnij wartość")
        }
    }

    if(isValid){
        if(edytowanaVariable>=0){
            edytowanyObject.idname=idname.val();
            edytowanyObject.type=type.val();
            edytowanyObject.value=value;
            edytowanaVariable=-1;
        }
        else{
            Variables.push(new Variable(idname.val(), type.val(),value));
        }
        showVariableList();    
    }
    return isValid;
}
function validLength(n, o,min, max , m) {
    if ( o.val().length > max || o.val().length < min ) {
        o.addClass( "ui-state-error" );
        if(o.val().length==0){
            $(m).append(n+$("#mandatoryfield-error-caption").text());
        }
        else{
            
            $(m).append(n+$("#lengthfield-button-caption").text()+ min+"-"+max);
        }
        //  alert(m);
        return false;
    } else {
        return true;
    }
}

function validSelect(o, m){
    if(o.selectedIndex==0){
        o.addClass( "ui-state-error" );
        return false;
    }
    return true;
    
}
function validCondition(condition){
    if(condition==null){
        return false;
    }
    if(condition.type=="simple"){
        if(condition.eastSideValue.length>0 && condition.westSideValue.length>0){
            return true;
        }
        return false;
    }
    else {
        var result=true;
        if(condition.conditions==null){
            return false;
        }
        if(condition.conditions.length==0){
            return false;
        }
        for(var i=0;i<condition.conditions.length;i++){
            result=(result&&validCondition(condition.conditions[i]))
        }
        return result;
    }
    return true;
}
 
function defaultClosingDialog(dialog){
    $(dialog).dialog("close");
}

function clearDialogsFields(dialog){
    $("input",dialog).each(function() {
        if(this.type=="text"){
            this.value="";
        }
        else if(this.type=="number"){
            this.value="0";
        }
        else if(this.type=="checkbox"){
            this.value="false";
        }
        else if(this.type=="file"){
            this.value="";
        }
    });
    $('select', dialog).each(function (){
        this.selectedIndex="0";
    });
};

function initializeDialogs(){
    //edytowanyAction=new CallFunction("add", new Argument("VALUE","zone1.test"), "MATH", new Array(new Argument("VALUE", "zone1"),new Argument("STRING", "value2")))
    $('#dialog-zone').tooltip();
    $('#dialog-character').tooltip();
    $('#dialog-item').tooltip();
    $('#dialog-media').tooltip();
    $('#dialog-quest').tooltip();
    $('#dialog-timer').tooltip();
    $('#dialog-variable').tooltip();
    $( "#dialog-zone" ).tooltip( "option", "disabled", true );
    $( "#dialog-character" ).tooltip( "option", "disabled", true );
    $( "#dialog-item" ).tooltip( "option", "disabled", true );
    $( "#dialog-media" ).tooltip( "option", "disabled", true );
    $( "#dialog-timer" ).tooltip( "option", "disabled", true );
    $('#points').button();
    $('input.choose-media').button();
    $('input.choose-media').click(function(){
        createValueDialog("MEDIA", false, $(this).parent().find("span.choose-media"))
        $("#dialog-value").dialog("open")
        
    }) 
    $('input.choose-zone').button();
    $('input.choose-zone').click(function(){
        createValueDialog("ZONE", false, $(this).parent().find("span.choose-zone"))
        $("#dialog-value").dialog("open")
    })
    
    $('#dialog-zone').dialog({
        autoOpen: false,
        modal: true,
        show: 'clip',
        hide: 'clip',
        buttons: [
        {
            text: $("#submit-button-caption").text(), 
            click: function() {
                if(createEditZone(this)){
                    if(newZone!=null) newZone.setMap(null);
                    if(zedytowalemZone) {
                        if(oryginalnyPolygon!=null) oryginalnyPolygon.setMap(null);
                        oryginalnyPolygon=edytowanyPolygon;
                    } 
                    //alert("or: "+oryginalnyPolygon+ " "+oryginalnyPolygon.getPath().getLength());
                    //alert("ed: "+edytowanyPolygon + " "+edytowanyPolygon.getPath().getLength());
                    //alert("zn: "+edytowanyObject.poly+ " "+edytowanyObject.poly.getPath().getLength());
                    
                    if(edytowanyPolygon!=null) {
                        edytowanyPolygon.setMap(null);
                        edytowanyPolygon=null;
                    }
                    //alert("or: "+oryginalnyPolygon+ " "+oryginalnyPolygon.getPath().getLength());
                    //alert("ed: "+edytowanyPolygon + " "+edytowanyPolygon.getPath().getLength());
                    if(oryginalnyPolygon!=null) oryginalnyPolygon.setMap(map);
                    edytowanaZona=-1;
                    //alert("zn: "+Zones[0].poly+ " "+Zones[0].poly.getPath().getLength());
                    $(this).dialog("close");
                }
                
            }
        },

        {
            text: $("#cancel-button-caption").text(), 
            click: function() {
                $(this).dialog("close");
            }
        }
        ],
        open: function(){
            zedytowalemZone=false;
            $( "#dialog-zone" ).tooltip( "option", "disabled", false );
            if(edytowanaZona>=0){
                $("#dialog-zone" ).dialog( "option", "title", $("#edit-zone-title").text());
                $("#idname",this).attr("value", edytowanyObject.idname);
                $("#name",this).attr("value", edytowanyObject.name);
                // $('#choiceeast').attr('checked', false);
                $("#visibility",this).attr("checked", edytowanyObject.visible);
                $("#state",this).attr("value", edytowanyObject.state);
                $("#description",this).attr("value", edytowanyObject.description);
                $("#owned",this).attr("checked", edytowanyObject.owned);
                $("span.choose-media",this).attr("data-value", edytowanyObject.icon);
                $("span.choose-media",this).text(getVisibleArgumentValue(edytowanyObject.icon));
                
                oryginalnyPolygon=edytowanyObject.poly;

                if(oryginalnyPolygon!=null && oryginalnyPolygon!="points") {
                    var pathekCopy=new google.maps.MVCArray();
                    for(var i=0; i<oryginalnyPolygon.getPath().getLength(); i++) {
                        pathekCopy.push(oryginalnyPolygon.getPath().getAt(i));
                    }
                    edytowanyPolygon=new google.maps.Polygon({
                        paths: pathekCopy,
                        strokeColor: "#FF0000",
                        strokeOpacity: 0.8,
                        strokeWeight: 2,
                        fillColor: "#FF0000",
                        fillOpacity: 0.35
                    });
                    oryginalnyPolygon.setMap(null);
                    edytowanyPolygon.setMap(map);
                }
                else edytowanyPolygon=null;
            }
            else{
                $( "#dialog-zone" ).dialog( "option", "title", $("#add-zone-title").text());
            }
        //alert("Ta strefa nie jest jeszcze zaznaczona na mapie.");
        },
        close: function(){
            $( "#dialog-zone" ).tooltip( "option", "disabled", true );

        }
    });
        
    $('#dialog-character').dialog({
        autoOpen: false,
        modal: true,
        show: 'clip',
        hide: 'clip',
        buttons: [
        {
            text: $("#submit-button-caption").text(), 
            click: function() {
                if(createEditCharacter(this)){
                    $(this).dialog("close");
                }
            }
        },

        {
            text: $("#cancel-button-caption").text(), 
            click: function() {
                $(this).dialog("close")
            }
        }
        ],
        open: function(){
            $( "#dialog-character" ).tooltip( "option", "disabled", false );
            if(edytowanyCharacter>=0){
                $( "#dialog-character" ).dialog( "option", "title", $("#edit-character-title").text());
                $("#idname",this).attr("value", edytowanyObject.idname);
                $("#name",this).attr("value", edytowanyObject.name);
                $("#visibility",this).attr("checked", edytowanyObject.visible);
                $("#state",this).attr("value", edytowanyObject.state);
                $("#description",this).attr("value", edytowanyObject.description);
                $("#sex",this).attr("value", edytowanyObject.sex);
                $("#owned",this).attr("checked", edytowanyObject.owned);
                $("span.choose-media",this).attr("data-value", edytowanyObject.icon);
                $("span.choose-media",this).text(getVisibleArgumentValue(edytowanyObject.icon));
                $("span.choose-zone",this).attr("data-value", edytowanyObject.zone);
                $("span.choose-zone",this).text(getVisibleArgumentValue(edytowanyObject.zone));
            }
            else{
                $( "#dialog-character" ).dialog( "option", "title", $("#add-character-title").text());
            }
        },
        close: function(){
            $( "#dialog-character" ).tooltip( "option", "disabled", true );
            edytowanyCharacter=-1;
        }
    });
    
    $('#dialog-item').dialog({
        autoOpen: false,
        modal: true,
        show: 'clip',
        hide: 'clip',
        buttons: [
        {
            text: $("#submit-button-caption").text(), 
            click: function() {
                if(createEditItem(this)){
                    $(this).dialog("close");
                }
            }
        },

        {
            text: $("#cancel-button-caption").text(), 
            click: function() {
                $(this).dialog("close")
            }
        }
        ],
        open: function(){
            $( "#dialog-item" ).tooltip( "option", "disabled", false );
            if(edytowanyItem>=0){
                $( "#dialog-item" ).dialog( "option", "title", $("#edit-item-title").text());
                $("#idname",this).attr("value", edytowanyObject.idname);
                $("#name",this).attr("value", edytowanyObject.name);
                $("#visibility",this).attr("checked", edytowanyObject.visible);
                $("#state",this).attr("value", edytowanyObject.state);
                $("#description",this).attr("value", edytowanyObject.description);
                $("#owned",this).attr("checked", edytowanyObject.owned);
                $("span.choose-media",this).attr("data-value", edytowanyObject.icon);
                $("span.choose-media",this).text(getVisibleArgumentValue(edytowanyObject.icon));
                $("span.choose-zone",this).attr("data-value", edytowanyObject.zone);
                $("span.choose-zone",this).text(getVisibleArgumentValue(edytowanyObject.zone));
            }
            else{
                $( "#dialog-item" ).dialog( "option", "title", $("#add-item-title").text());
            }
        },
        close: function(){
            $( "#dialog-item" ).tooltip( "option", "disabled", true );
            edytowanyItem=-1;
            
        }
    });
    $('#path').button()
    $('#path').click(function(){
        assetSpan=$('#dialog-media .mediaSpan');
        createAssetsList();
        $('#assets_widget').dialog("open");
    })
    $('#dialog-media').dialog({
        autoOpen: false,
        modal: true,
        show: 'clip',
        hide: 'clip',
        buttons: [
        {
            text: $("#submit-button-caption").text(), 
            click: function() {
                if(createEditMedia(this)){
                    $(this).dialog("close");
                }
            }
        },

        {
            text: $("#cancel-button-caption").text(), 
            click: function() {
                $(this).dialog("close")
            }
        }
        ],
        open: function(){
            $( "#dialog-media" ).tooltip( "option", "disabled", false );
            if(edytowaneMedia>=0){
                $( "#dialog-media" ).dialog( "option", "title", $("#edit-media-title").text());
                $("#idname",this).attr("value", edytowanyObject.idname);
                $("#dialog-media .mediaSpan").attr("data-id", edytowanyObject.pathid);
                $("#dialog-media .mediaSpan").text(edytowanyObject.pathname);
                $("#type",this).attr("value", edytowanyObject.type);
            }
            else{
                
                $("#dialog-media .mediaSpan").attr("data-id", "");
                $("#dialog-media .mediaSpan").text("");
                $( "#dialog-media" ).dialog( "option", "title", $("#add-media-title").text());
            }
        },
        close: function(){
            $( "#dialog-media" ).tooltip( "option", "disabled", true );
            edytowanyMedia=-1;
            
        }
    });
    $('#dialog-quest').dialog({
        autoOpen: false,
        modal: true,
        show: 'clip',
        hide: 'clip',
        buttons: [
        {
            text: $("#submit-button-caption").text(), 
            click: function() {
                if(createEditQuest(this)){
                    $(this).dialog("close");
                }
            }
        },

        {
            text: $("#cancel-button-caption").text(), 
            click: function() {
                $(this).dialog("close")
            }
        }
        ],
        open: function(){
            $( "#dialog-quest" ).tooltip( "option", "disabled", false );
            if(edytowanyQuest>=0){
                $( "#dialog-quest" ).dialog( "option", "title", $("#edit-quest-title").text());
                $("#idname",this).attr("value", edytowanyObject.idname);
                $("#name",this).attr("value", edytowanyObject.name);
                $("#visibility",this).attr("checked", edytowanyObject.visible);
                $("#state",this).attr("value", edytowanyObject.state);
                $("#description",this).attr("value", edytowanyObject.description);
                $("span.choose-media",this).attr("data-value", edytowanyObject.icon);
                $("span.choose-media",this).text(getVisibleArgumentValue(edytowanyObject.icon));
            }
            else{
                $( "#dialog-quest" ).dialog( "option", "title", $("#add-quest-title").text());
            }
        },
        close: function(){
            $( "#dialog-quest" ).tooltip( "option", "disabled", true );
            edytowanyQuest=-1;
        }
    });
    $('#dialog-timer').dialog({
        autoOpen: false,
        modal: true,
        show: 'clip',
        hide: 'clip',
        buttons: [
        {
            text: $("#submit-button-caption").text(), 
            click: function() {
                if(createEditTimer(this)){
                    $(this).dialog("close");
                }
            }
        },

        {
            text: $("#cancel-button-caption").text(), 
            click: function() {
                $(this).dialog("close");
            }
        }
        ],
        open: function(){
            $( "#dialog-timer" ).tooltip( "option", "disabled", false );
            if(edytowanyTimer>=0){
                $( "#dialog-timer" ).dialog( "option", "title", $("#edit-timer-title").text());
                $("#idname",this).attr("value", edytowanyObject.idname);
                $("#interval",this).attr("value", edytowanyObject.interval);
            }
            else{
                $( "#dialog-timer" ).dialog( "option", "title",$("#add-timer-title").text());
            }
        },
        close: function(){
            $( "#dialog-timer" ).tooltip( "option", "disabled", true );
            edytowanyTimer=-1;
        }
    });
    
    $('#variable-type').click(function (){
        var value=$(this).val();
        $("#number-variable").hide()
        $("#checkbox-variable").hide()
        $("#text-variable").hide()
        $("#dialog-variable label[for=variable-value]").hide();
        if(value!=null && value!=""){
          
            $("#dialog-variable label[for=variable-value]").show();
            if(value=="INTEGER"){
                $("#number-variable").show()
            }
            else if(value=="BOOLEAN"){
                $("#checkbox-variable").show()
            }
            else{
                $("#text-variable").show()
            }
        }
    });
    
    $('#dialog-variable').dialog({
        autoOpen: false,
        modal: true,
        show: 'clip',
        hide: 'clip',
        buttons: [
        {
            text: $("#submit-button-caption").text(), 
            click: function() {
                if(createEditVariable(this)){
                    $(this).dialog("close");
                }
            }
        },

        {
            text: $("#cancel-button-caption").text(), 
            click: function() {
                $(this).dialog("close");
            }
        }
        ],
        open: function(){
            $("#dialog-variable label[for=variable-value]").hide();
            $("#number-variable",this).hide()
            $("#checkbox-variable",this).hide()
            $("#text-variable",this).hide()
            $( "#dialog-variable" ).tooltip( "option", "disabled", false );
            if(edytowanaVariable>=0){
                $("#dialog-variable" ).dialog( "option", "title", $("#edit-variable-title").text());
                $("#idname",this).attr("value", edytowanyObject.idname);
                $("#variable-type",this).attr("value", edytowanyObject.type);
                if(edytowanyObject.type=="INTEGER"){
                    $("#number-variable",this).attr("value", edytowanyObject.value);
                    $("#number-variable",this).show()
                }
                else if(edytowanyObject.type=="BOOLEAN"){
                    $("#checkbox-variable",this).attr("checked", edytowanyObject.value);
                    $("#checkbox-variable",this).show()
                }
                else{
                    $("#text-variable",this).attr("value", edytowanyObject.value);
                    $("#text-variable",this).show()
              
                }
            }
            else{
                $( "#dialog-variable" ).dialog( "option", "title",$("#add-variable-title").text());
            }
        },
        close: function(){
            $( "#dialog-variable" ).tooltip( "option", "disabled", true );
            edytowanaVariable=-1;
        }
    });
    createConditionDialog();
    createCallFunctionDialog();
    $( "#dialog-complex-condition" ).tooltip();
    $("#dialog-complex-condition").dialog({
        autoOpen: false,
        height:400,
        maxHeight: 600,
        buttons: [
        {
            text: $("#submit-button-caption").text(),
            click:function (){
                var tmpCondition=null;
                tmpCondition=parseComplexCondition("#conditions",tmpCondition)
                if(validCondition(tmpCondition)){
                    edytowanyCondition=tmpCondition;
                    poprawNazweWarunku($(edytowanyConditionBlock), edytowanyCondition);
                    $("#dialog-complex-condition").dialog("close")
                }
                else{
                    $("#dialog-complex-condition div.error").append($("#conditions-error-caption").text())
                }
                    
            }
        },
        {
            text: $("#cancel-button-caption").text(),
            click:function (){
                $("#dialog-complex-condition").dialog("close")
            }
        }
        ],
        open: function(){ 
            $( "#dialog-complex-condition" ).tooltip( "option", "disabled", false );
            $("#dialog-complex-condition div.error").empty()
            creatComplex(edytowanyCondition);
        },
        close: function(){
            $( "#dialog-complex-condition" ).tooltip( "option", "disabled", true );
        }
    });  
    createAssetsList();
    $("#assets_widget").dialog({
        autoOpen: false,
        height:400,
        maxHeight: 600,
        buttons: [
        {
            text: $("#submit-button-caption").text(),
            click:function (){
                /* var index = $("#category li").index($("#category .ui-selected"));
                if($( "li.ui-selected",this ).size()>1){
                    $( "li.ui-selected",this  ).removeClass("ui-selected");
                }*/
                var selected=$("#listAssetsDiv .ui-selected");
                var id=selected.attr("data-id")
                var text=selected.text();
                if(assetSpan!=false){
                    assetSpan.attr("data-id",id);
                    assetSpan.text(text)
                    assetSpan=false;
                }
                // alert(id+" "+text)
                $("#assets_widget").dialog("close")
                    
            }
        },
        {
            text: $("#cancel-button-caption").text(),
            click:function (){
                $("#assets_widget").dialog("close")
            }
        }
        ]
    });
    $("#frmUploadAssetDiv button").button();
    $("#form_attachment").button()
    
    return false;
}

function createAssetsList(){
    $("#listAssetsDiv ul").selectable();
    $("button.asset_preview").button({
        icons: {
            primary: "ui-icon-search"
        },
        text: false,
        click: function(){
            
            window.open($(this).attr("data-href"), 'window name', 'window settings');
            return false;
        }
    });
    $("button.asset_preview").click(function(){
        window.open($(this).attr("data-href"), 'window name', 'window settings');
        return false;
    });
}

function parseComplexCondition(parent,condition){
    var index=0;
    var start=false;
    $(parent+'>div').each(function(){
        if($(this).hasClass("complexCondition")){
            var childCondition=new ComplexCondition($("h3>span.complexType",this).attr("data-type"), new Array());
            if(condition==null){
                condition=childCondition;
                start=true;
            }
            parseComplexCondition(parent+">div.complexCondition:nth-child("+(++index)+")>div>div.subconditions",childCondition);
            if(!start){
                condition.conditions.push(childCondition);
            }
        }
        else{
            var span=$("h3>span.conditionval",this);
            ++index;
            var childCondition=new SimpleCondition("simple",span.attr("data-eastsidetype"),span.attr("data-eastdatatype"),
                span.attr("data-eastsidevalue"), span.attr("data-operator"), span.attr("data-westsidetype"), span.attr("data-westdatatype"), 
                span.attr("data-westsidevalue"));
            condition.conditions.push(childCondition);
        }
    //  alert( $(parent+'>div.complexCondition span.complexType:first').attr("data-type"))
        
    })
    
    return condition;
}

$(function () {
    $("#createZoneButton").click(function() {
        clearDialogsFields($('#dialog-zone'));
        $('#dialog-zone').dialog('open');
        //ZoneDialog.tooltip();
        return false;
    });
    
    
    $("#createCharacterButton").click(function() {
        
        clearDialogsFields($('#dialog-character'));
        $('#dialog-character').dialog('open');
        return false;
    });
    
    $("#createItemButton").click(function() {
        clearDialogsFields($('#dialog-item'));
        $('#dialog-item').dialog('open');
        return false;
    });
    
    $("#createMediaButton").click(function() {
        clearDialogsFields($('#dialog-media'));
        $('#dialog-media').dialog('open');
        return false;
    });
    $("#createQuestButton").click(function() {
        
        clearDialogsFields($('#dialog-quest'));
        $('#dialog-quest').dialog('open');
        return false;
    });
    
    $("#createTimerButton").click(function() {
       
        clearDialogsFields($('#dialog-timer'));
        $('#dialog-timer').dialog('open');
    });  
    $("#createVariableButton").click(function() {
       
        clearDialogsFields($('#dialog-variable'));
        $('#dialog-variable').dialog('open');
    });    
    $("a:contains('add function')").click(function() {
        $('#dialog-callfunction').dialog('open');
    });
  
    
    $("a:contains('get field')").click(function() {
        createValueDialog("String", true);
        $("#dialog-value").dialog('open');
    });   
    $("a:contains('add condition')").click(function() {
        $("#dialog-condition").dialog('open');
    }); 
    
    edytowanyCondition=null;
    /*  edytowanyCondition=new ComplexCondition("AND", new Array(
new ComplexCondition("OR", new Array( new SimpleCondition("simple", "VALUE","DOUBLE", "zone1.id", "=", "VALUE","DOUBLE", "zone1.id"),new SimpleCondition("simple", "VALUE","DOUBLE", "zone1.id", "=", "VALUE","DOUBLE", "zone1.id"))),
new SimpleCondition("simple", "VALUE","DOUBLE", "zone1.id", "=", "VALUE","DOUBLE", "zone1.id")))*/
    $("a:contains('add complex')").click(function() {
        
        $("#dialog-complex-condition").dialog('open');
    }); 
    $("a:contains('add asset')").click(function() {
        $("#assets_widget").dialog('open');
    });    
});

function creatComplex(condition){
    $("#conditions").empty();
    $("#conditions").append(createComplexCondition(condition));
    $("#conditions button").button();
    $("#conditions .complexCondition").accordion({
        collapsible: true,
        autoHeight: false
    });
    $("#conditions .simpleCondition").accordion({
        collapsible: true,
        autoHeight: false
    });
    
    appendComplexConditionActions();
}

function appendComplexConditionActions(){
    $("#conditions button").unbind("click");
    $("#conditions button.or,#conditions button.and").click(function(){
        var div = $(this).closest('div.complexCondition');
        //alert(div.html())
        var clearCondition;
        if($(this).hasClass("and")){
            clearCondition=new ComplexCondition("AND", new Array())
        }
        if($(this).hasClass("or")){
            clearCondition=new ComplexCondition("OR", new Array())
        }
        var accordion=       $(createComplexCondition(clearCondition)).accordion({
            collapsible: true,
            autoHeight: false
        });
        $("button",accordion).button();
        div.find("div.subconditions:first").append(accordion)
        if(div.length==0){
            $("#conditions").empty();
            $("#conditions").append(accordion)
        }
        appendComplexConditionActions();
    });
    $("#conditions button.delete").click(function(){
        $(this).closest('div.complexCondition').remove();
        if($("#conditions").children("*").length==0){
            $("#conditions").append(getStartCondtionButton())
            $("#conditions button").button();
            appendComplexConditionActions()
        }
    })
    $("#conditions button.simpledelete").click(function(){
        $(this).closest('div.simpleCondition').remove();
    })
    $("#conditions button.simpleedit").click(function(){
        conditionSpan=$(this).closest('div.simpleCondition').find("span.conditionval");
        conditionSpanEdit=true;
        $("#dialog-condition").dialog("open")
    })
    $("#conditions button.simple").click(function(){
        var accordion=$(createSimpleCondition()).accordion({
            collapsible: true,
            autoHeight: false
        });
        accordion.find("button").button();
        accordion.hide();
        $(this).closest('div.complexCondition').find('div.subconditions:first').append(accordion)
       
        conditionSpan=$(this).closest('div.complexCondition').find('div.subconditions:first>div.simpleCondition:last span.conditionval');
        //conditionSpan.text("eloelo")
        //alert(conditionSpan.html())
        $("#dialog-condition").dialog("open")
        appendComplexConditionActions();
    })
}
function createSimpleCondition(){
    var content='<div class="simpleCondition"><h3><span class="conditionval"></span></h3>';
    content+=getSimpleConditionButton()+'</div>';
    return content;
}
function createComplexCondition(condition){
    var content=""
    if(condition==null){
        
        return getStartCondtionButton();
    }
    else if(condition.type=="simple"){
        var cond=$("<div></div>")
        cond.append(createSimpleCondition());
        var span=cond.find("span.conditionval");
        span.attr("data-type",condition.type)
        span.attr("data-eastSideType",condition.eastSideType)
        span.attr("data-eastDataType",condition.eastDataType)
        span.attr("data-eastSideValue",condition.eastSideValue)
        span.attr("data-operator",condition.operator)
        span.attr("data-westSideType",condition.westSideType)
        span.attr("data-westDataType",condition.westDataType)
        span.attr("data-westSideValue",condition.westSideValue)
        span.text(condition.eastSideValue+" "+condition.operator+" "+condition.westSideValue); 
        content=cond.html();
    }
    else{
        content='<div class="complexCondition"><h3><span class="complexType" data-type="'+condition.type+'">\
'+condition.type+'</span></h3>\
   <div> <div class="subconditions">';
        if(condition.conditions!=null){
            for(var i=0;i<condition.conditions.length;i++){

                content+=createComplexCondition(condition.conditions[i]);

            }
        }
        content+='</div>'+getConditionButton()+'</div></div>';
    
    }
    return content;
}
function getStartCondtionButton(){
    return '<div class="buttons"><button class="and">\
'+$("#and-button-caption").text()+'</button><button class="or">'+$("#or-button-caption").text()+'</button></div>'
}
function getConditionButton(){
    return '<div class="buttons"><button class="and">\
'+$("#and-button-caption").text()+'</button><button class="or">\
'+$("#or-button-caption").text()+'</button><button class="simple">\
'+$("#condition-button-caption").text()+'</button><button class="delete">\
'+$("#delete-button-caption").text()+'</button></div>'
}
function getSimpleConditionButton(){
    var content= '<div class="buttons"><button class="simpleedit">\
'+$("#edit-button-caption").text()+'</button><button class="simpledelete">\
'+$("#delete-button-caption").text()+'</button></div>'
   
    return content;
}
function getAccordionCondition(condition){
    return '<div class="complexCondition"><h3>'+condition+'</h3>\
    <div>'+getConditionButton()+'\
    </div>\
    </div>'
}
function createConditionDialog(){
    $('#dialog-condition .inputselect').buttonset();
    $('#dialog-condition input[type=checkbox]').removeClass('ui-helper-hidden-accessible')
    // $('#westtype').hide();
    //$('#easttype').hide();
    $('#dialog-condition input[type!=radio]').hide();
    $('#dialog-condition input[type=radio]').click(function (){
        
        if(this.id=="manualwest"){
            $('#westtype, #dialog-condition label[for=westtype]').show();
            $('#westval').text("");
            $('#westval').attr("data-type","");
            $('#dialog-condition').dialog()
            $('#westtype').click();
        }
        else if(this.id=="manualeast"){
            $('#easttype, #dialog-condition label[for=easttype]').show();
            $('#eastval').text("");
            $('#eastval').attr("data-type","");
            $('#easttype').click();
                
        }
        else if(this.id=="choiceeast"){
            if($('#easttype').attr("value").length>0 && !closeDialogValue){
                $('#checkboxeast').hide();
                $('#texteast').hide();
                $('#numbereast').hide();
                createValueDialog($('#easttype').attr("value"), true, $('#eastval'));
                $("#dialog-value").dialog("open");
            }
            else{
                closeDialogValue=false;
            }
            
        }
        else if(this.id=="choicewest" && !closeDialogValue){
            if($('#westtype').attr("value").length>0){
                $('#checkboxwest').hide();
                $('#textwest').hide();
                $('#numberwest').hide();
                createValueDialog($('#westtype').attr("value"), true, $('#westval'));
                $("#dialog-value").dialog("open");
            }
            else{
                closeDialogValue=false;
            }
        }
        
    })
    $('#easttype').click(function (){
        if($('div.eastside :radio:checked').length>0){
            if(this.value=="BOOLEAN"){
                $('#checkboxeast').show();
                $('#texteast').hide();
                $('#numbereast').hide();
            }
            else if(this.value=="INTEGER"){
                $('#checkboxeast').hide();
                $('#texteast').hide();
                $('#numbereast').show();
            }
            else {
                $('#checkboxeast').hide();
                $('#texteast').show();
                $('#numbereast').hide();
            }
        }
        else {
            $('#checkboxeast').hide();
            $('#texteast').hide();
            $('#numbereast').hide();
        }
    }) 
    $('#westtype').click(function (){
        if($('div.westside :radio:checked').length>0){
            if(this.value=="BOOLEAN"){
                $('#checkboxwest').show();
                $('#textwest').hide();
                $('#numberwest').hide();
            }
            else if(this.value=="INTEGER"){
                $('#checkboxwest').hide();
                $('#textwest').hide();
                $('#numberwest').show();
            }
            else {
                $('#checkboxwest').hide();
                $('#textwest').show();
                $('#numberwest').hide();
            }
        }
        else{
            $('#checkboxwest').hide();
            $('#textwest').hide();
            $('#numberwest').hide();
            
        }
    })
    $('#dialog-condition').tooltip();
    $('#dialog-condition').dialog({
        autoOpen: false,
        modal: true,
        show: 'clip',
        hide: 'clip',
        width: 700,
        buttons: [
        {
            text: $("#submit-button-caption").text(),
            click: function(){
                var eastSideType;
                var westSideType;
                var eastSideValue;
                var eastDataType="";
                var westDataType="";
                var westSideValue;
                var operator;
                $('#dialog-condition div.error').empty();
                if($('#eastval').text().length>0){
                    eastDataType=$('#easttype').val()
                    eastSideValue=$('#eastval').attr("data-value")
                    eastSideType="VALUE"
                }
                else{
                    eastSideType=$('#easttype').val()
                    if(eastSideType=="BOOLEAN"){
                        eastSideValue=$("#checkboxeast").is(':checked')
                    }
                    else if(eastSideType=="INTEGER"){
                        eastSideValue=$("#numbereast").val()
                    }
                    else{
                        eastSideValue=$("#texteast").val()
                    }
                }
                if($('#westval').text().length>0){
                    westDataType=$('#westtype').val()
                    westSideValue=$('#westval').attr("data-value")
                    westSideType="VALUE"
                }
                else{
                    westSideType=$('#westtype').val()
                    if(westSideType=="BOOLEAN"){
                        westSideValue=$("#checkboxwest").is(':checked')
                    }
                    else if(westSideType=="INTEGER"){
                        westSideValue=$("#numberwest").val()
                    }
                    else{
                        westSideValue=$("#textwest").val()
                    }
                }
                var valid=true;
                if(westDataType.length>0 && eastDataType.length>0){
                    //alert("1")
                    if(westDataType!=eastDataType) valid=false;
                }
                else if(westDataType.length>0 ){
                    //   alert("2")
                    if(westDataType!=eastSideType) valid=false;
                }
                else if( eastDataType.length>0){
                    //alert(eastDataType+"3"+westSideType)
                    if(westSideType!=eastDataType) valid=false;
                }
                else{
                    //alert(westSideType+" "+eastSideType)
                    if(westSideType!=eastSideType) valid=false;
                }
                
                if(!valid){
                    $('#dialog-condition div.error').append($("#type-error-caption").text()+"<BR>")
                }
                
                if(eastSideValue.length<=0 ||westSideValue.length<=0  ){
                    alert(eastSideValue+" "+westSideValue)
                    $('#dialog-condition div.error').append($("#fillvalue-error-caption").text()+"<BR>")
                    valid=false;
                }
                if($("#operator").val().length>0){
                    operator=$("#operator")
                }
                else{
                    $('#dialog-condition div.error').append($("#chooseoperator-error-caption").text()+"<BR>");
                    valid=false;
                }
                if(valid){
                    conditionSpan.attr("data-type","simple")
                    conditionSpan.attr("data-eastSideType",eastSideType)
                    conditionSpan.attr("data-eastDataType",eastDataType)
                    conditionSpan.attr("data-eastSideValue",eastSideValue)
                    conditionSpan.attr("data-operator",operator.val())
                    conditionSpan.attr("data-westSideType",westSideType)
                    conditionSpan.attr("data-westDataType",westDataType)
                    conditionSpan.attr("data-westSideValue",westSideValue)
                    var eastVisiblename;
                    if($('#eastval').text().length>0){
                        eastVisiblename=getVisibleArgumentValue(eastSideValue);
                    }
                    else{
                        eastVisiblename=eastSideValue;
                    }
                    var westVisiblename;
                    if($('#westval').text().length>0){
                        westVisiblename=getVisibleArgumentValue(westSideValue);
                    }
                    else{
                        westVisiblename=westSideValue;
                    }
                    conditionSpan.text(eastVisiblename+" "+operator.val()+" "+westVisiblename)
                    $(conditionSpan).closest("div.simpleCondition").show()
                    conditionSpan=false;
                    $('#dialog-condition').dialog('close')
                }
                
            }
        },
        {
            text:"Cancel",
            click: function(){
                $('#dialog-condition').dialog('close')
            }
        }
        ],
        open: function(){
            $( "#dialog-condition" ).tooltip( "option", "disabled", false );
            $('#dialog-condition div.error').empty();
            if(conditionSpanEdit){
                var eastSideType=conditionSpan.attr("data-eastSideType")
                var westSideType=conditionSpan.attr("data-westSideType")
                var eastSideValue=conditionSpan.attr("data-eastSideValue");
                var westSideValue=conditionSpan.attr("data-westSideValue");
                    
                $("#easttype").val(conditionSpan.attr("data-eastDataType"));
                $("#westtype").val(conditionSpan.attr("data-westDataType"));
                $("#operator").val(conditionSpan.attr("data-operator"));
                if(eastSideType=="VALUE"){
                    closeDialogValue=true;
                    $('#choiceeast').click();
                    $('#eastval').text(getVisibleArgumentValue(eastSideValue))
                    $('#eastval').attr("data-value",eastSideValue)
                    $('dialog-condition').dialog('close')
                }
                else {
                    $('#manualeast').click()
                    if(eastSideType=="INTEGER"){
                        $("#numbereast").val(eastSideValue)
                        $("#numbereast").show()
                    }
                    else if(eastSideType=="BOOLEAN"){
                        $("#checkboxeast").val(eastSideValue)
                        $("#checkboxeast").show()
                    }
                    else{
                        $("#texteast").val(eastSideValue)
                        $("#texteast").show()
                    }
                }
                
                if(westSideType=="VALUE"){
                    closeDialogValue=true;
                    $('#choicewest').click()
                    $('#westval').text(getVisibleArgumentValue(westSideValue))
                    $('#westval').attr("data-value",westSideValue)
                    $('dialog-condition').dialog('close')
                }
                else{
                    $('#manualwest').click();
                    if(westSideType=="INTEGER"){
                        $("#numberwest").val(westSideValue)
                        $("#numberwest").show();
                    }
                    else if(westSideType=="BOOLEAN"){
                        $("#checkboxwest").val(westSideValue)
                        $("#checkboxwest").show()
                    }
                    else{
                        $("#textwest").val(westSideValue)
                        $("#textwest").show()
                    }
                }
                
            }
            else{
                $('#manualwest').attr('checked', false);
                $('#manualeast').attr('checked', false);
                $('#choicewest').attr('checked', false);
                $('#choiceeast').attr('checked', false);
                $('#dialog-condition .inputselect ').buttonset('refresh');
                $("#numberwest").val("");
                $("#numberwest").hide();
                $("#textwest").val("");
                $("#textwest").hide();
                $("#checkboxwest").val("");
                $("#checkboxwest").hide();
                $("#numbereast").val("");
                $("#numbereast").hide();
                $("#texteast").val("");
                $("#texteast").hide();
                $("#checkboxeast").val("");
                $("#checkboxeast").hide();
                $("#easttype").val("");
                $("#westtype").val("");
                $("#operator").val("");
                $('#westval').text("")
                $('#eastval').text("")
            }
        },
        close: function(){
            $( "#dialog-condition" ).tooltip( "option", "disabled", true );
            if(!conditionSpanEdit){
                if(conditionSpan!=false){
                    $(conditionSpan).closest("div.simpleCondition").remove()
                    conditionSpan=false;
                }
            }
            else{
                conditionSpanEdit=false;
                conditionSpan=false;
            }
        }
    })
}

function getVisibleValue(dataValue,groupName){
    var field;
    var value;
    if(dataValue==null || dataValue=="") return "";
    var split=dataValue.split(".")
    for(var i=0;i<GroupList.length;i++){
        if(GroupList[i].name==groupName){
            for(var j=0;j<GroupList[i].values.length;j++){
                if(GroupList[i].values[j].id==split[0]){
                    value=GroupList[i].values[j].idname;
                }
            }
            for(var k=0;k<GroupList[i].fields.length;k++){
                if(GroupList[i].fields[k].field==split[1]){
                    field=GroupList[i].fields[k].visibleName;
                }
            }
        }
    }
    return groupName+"."+value+"."+field;
}
function getVisibleArgumentValue(dataValue){
    var field=false;
    var value;
    var groupName;
    if(dataValue==null || dataValue=="") return "";
    var split=dataValue.split(".")
    for(var i=0;i<GroupList.length;i++){
        for(var j=0;j<GroupList[i].values.length;j++){
            if(GroupList[i].values[j].id==split[0]){
                value=GroupList[i].values[j].idname;
                groupName=GroupList[i].name;
                for(var k=0;k<GroupList[i].fields.length;k++){
                    if(GroupList[i].fields[k].field==split[1]){
                        field=GroupList[i].fields[k].visibleName;
                    }
                }
                break;
            }
        }
    }
    if(!field){
        return groupName+"."+value;
    }
    else{
        return groupName+"."+value+"."+field;
    }
}
function isCorrectType(dataValue,type){
    var field=false;
    var result=false;
    var groupName;
    var split=dataValue.split(".")
    for(var i=0;i<GroupList.length;i++){
        for(var j=0;j<GroupList[i].values.length;j++){
            if(GroupList[i].values[j].id==split[0]){
                for(var z=0;z<GroupList[i].type.length;z++){
                    if(!result && type==GroupList[i].type[z]){
                        result=true;
                        for(var k=0;k<GroupList[i].fields.length;k++){
                            if(GroupList[i].fields[k].field==split[1]){
                                result=(type==GroupList[i].fields[k].type);
                                return result;
                            }
                        }
                    }
                }
            }
        }
    }
    return result;
}

function createCallFunctionDialog(){
    var oldFunctionIndex=-1;
    var oldCategoryIndex=-1;
    var functionChoiceChanged=true;
    var selectedFunctionSpan;
    $('#category').selectable({
        stop: function() {
            
            var index = $("#category li").index($("#category .ui-selected"));
            if($( "li.ui-selected",this ).size()>1){
                $( "li.ui-selected",this  ).removeClass("ui-selected");
            }
            else{
                if(oldCategoryIndex!=index){
                    oldCategoryIndex=index;
                    oldFunctionIndex=-1;
                    functionChoiceChanged=true;
                }
                
                $("#category"+index).show();
            }
        },
        unselecting: function() {
            
            $("#function ol").hide();
        }
    
    });
    $('#function ol').selectable({
        
        stop: function() {
            var categoryIndex = $("#category li").index($("#category .ui-selected"));
                
            var functionIndex = $("#category"+categoryIndex+">li").index($("#category"+categoryIndex+">li.ui-selected"));
                
            if($( "li.ui-selected",this ).size()>1){
                $( "li.ui-selected",this  ).removeClass("ui-selected");
            }
            else{
                if(oldFunctionIndex!=functionIndex){
                    oldFunctionIndex=functionIndex;
                    functionChoiceChanged=true;
                }                
            }
        }
    
    });
    
    $('#function span').hide();
    $('#function ol').hide();
    
    var pageNumber=0;
    var argumentEdit=true;
    var targetEdit=true;
    $('#dialog-callfunction').tooltip();
    $('#dialog-callfunction').dialog({
        autoOpen: false,
        modal: true,
        show: 'clip',
        hide: 'clip',
        width: 600,
        buttons: [
        {
            text: $("#previous-button-caption").text(), 
            click: function() {
                if(pageNumber==1){     
                    var categoryIndex = $("#category li").index($("#category .ui-selected"));
                
                    var functionIndex = $("#category"+categoryIndex+">li").index($("#category"+categoryIndex+">li.ui-selected"));
                
                    $("#argument").hide();
                    $("#category").show();
                    $("#category"+categoryIndex).show();
                    pageNumber--;
                        
                }
                else if(pageNumber==2){
                    $("#target").hide();                        
                    $("#argument").show();
                    pageNumber--;
                }
            }
        },{
            text: $("#next-button-caption").text(),            
            click: function() {
                var categoryIndex = $("#category li").index($("#category .ui-selected"));
                var functionIndex = $("#category"+categoryIndex+">li").index($("#category"+categoryIndex+">li.ui-selected"));
                var closeDialog=false; 
                if(pageNumber==0){
                    var multipleargs=false;
                    if(categoryIndex>=0 && functionIndex>=0){
                        if(functionChoiceChanged){
                            functionChoiceChanged=false;
                            $("#argument").empty();

                            var counterArgs=0;
                            selectedFunctionSpan=$("#category"+categoryIndex+">li.ui-selected span.function-details")

                            $("#category"+categoryIndex+">li.ui-selected ol.arguments li").each(function(){
                                if($("span ",this).attr("multiple-args")){
                                    multipleargs=true;
                                    $("#argument").append(addArgumentButton(createArgumentButton('arg'+counterArgs,$("span",this),true)));
                                    var span=$("span",this);
                                    $("#addargument").click(function(){
                                        var remove=$('<button id="remove'+counterArgs+'">'+$("#delete-button-caption").text()+'</button>');
                                        remove.button();
                                        $("#argument").append(createArgumentButton('arg'+counterArgs,span,true).append(remove));
                                        $("#argument")
                                        $("#argument").append("<br>");
                                        $("#arg"+counterArgs).buttonset();
                                        $("#remove"+counterArgs).click(function(){
                                            $(this).closest("div").remove();
                                        })
                                        counterArgs++;
                                    })
                                }
                                else{
                                    $("#argument").append("Argument- "+$(this).text());
                                    $("#argument").append(createArgumentButton('arg'+counterArgs,$("span",this),true));
                                    $("#argument").append("<br>");
                                    $("#arg"+counterArgs).buttonset();
                                    counterArgs++;
                                }

                            })
                            $("#argument").append("</div>")
                            $("#target").empty();
                            $("#category"+categoryIndex+">li.ui-selected span.target").each(function(){

                                $("#target").append($(this).text());
                                $("#target").append(createArgumentButton('target'+$(selectedFunctionSpan).attr("data-name"),$(this),false));

                                $("#target").append("<br>");
                            })

                        }
                        $("#target").hide()
                        $("#category").hide();
                        $("#function ol").hide();
                        $("#argument").show();
                        if(edytowanyAction!=null && argumentEdit){
                            
                            var nameValid=$("#function ol li.ui-selected span").attr("data-name")==edytowanyAction.name;
                            var targetValid= true;
                            if(edytowanyAction.target!=null)
                                targetValid=isCorrectType(edytowanyAction.target.value, $("#function ol li.ui-selected span.target").attr("data-type"));

                            if(nameValid && targetValid ){

                                argumentEdit=false;
                                for(var i=0;i<edytowanyAction.arguments.length;i++){
                                    if(multipleargs){
                                        $("#addargument").click();
                                    }
                                    if(edytowanyAction.arguments[i].type=="VALUE"){
                                        $("label[for=choicearg"+i+"]").addClass("ui-state-active");
                                        $("#valarg"+i).show()     
                                        var visibleValue=getVisibleArgumentValue(edytowanyAction.arguments[i].value);
                                        $($("#argument span.argvalue")[i]).text(visibleValue);
                                        $($("#argument span.argvalue")[i]).attr("data-value",edytowanyAction.arguments[i].value);
                                        $($("#argument span.argvalue")[i]).attr("data-type",edytowanyAction.arguments[i].type);
                                    }
                                    else{
                                        $("#manualarg"+i).click();
                                        if(edytowanyAction.arguments[i].type=="INTEGER"){
                                            $("#numberarg"+i).val(edytowanyAction.arguments[i].value);
                                        }
                                        else if(edytowanyAction.arguments[i].type=="BOOLEAN"){
                                            if(edytowanyAction.arguments[i].value=="true"){
                                                $( "#checkbox"+i).removeClass("ui-helper-show-accessible");
                                                $( "#checkbox"+i).set(":checked")
                                            }
                                        }
                                        else{
                                            $("#textarg"+i).val(edytowanyAction.arguments[i].value);
                                        }

                                        $($("#argument span.argvalue")[i]).attr("data-value",edytowanyAction.arguments[i].value);
                                        $($("#argument span.argvalue")[i]).attr("data-type",edytowanyAction.arguments[i].type);
                                    }
                                }
                            }
                        }
                        pageNumber++;

                    }
                }
                else if(pageNumber==1){
                    
                    var validationArg=true;
                    $("#dialog-callfunction div.error").empty();
                    if($('#argument span.argvalue').each(function(){
                        if($(this).attr("data-value").length==0){
                            validationArg=false;
                        }
                        
                    }))
                        if(validationArg){
                            $("#target").show();
                            $("#argument").hide();
                            if($("#target *").length==0){
                                closeDialog=true;
                            }
                            else{
                                if(edytowanyAction!=null && targetEdit){
                                    targetEdit=false;
                                    $("label[for=choicetarget"+edytowanyAction.name+"]").addClass("ui-state-active");
                                    $("#valtarget"+edytowanyAction.name).show()     
                                    var visibleTargetValue=getVisibleArgumentValue(edytowanyAction.target.value);
                                    $("#target span.argvalue").text(visibleTargetValue);
                                    $($("#target span.argvalue")).attr("data-value",edytowanyAction.target.value);
                                    $($("#target span.argvalue")).attr("data-type",edytowanyAction.target.type);
                                          
                                }
                                pageNumber++;
                            }
                        }
                        else{
                            $("#dialog-callfunction div.error").append($("#fillargs-error-caption").text());
                        }
                }else if(pageNumber==2){
                    $("#dialog-callfunction div.error").empty();
                    if($('#target span.argvalue').attr("data-value").length>0){
                        closeDialog=true;
                    }
                    else{
                        $("#dialog-callfunction div.error").append($("#filltarget-error-caption").text());
                    }
                }
                
                if(closeDialog){
                    var args=new Array();
                    // alert($(selectedFunctionSpan).attr("data-type")+$(selectedFunctionSpan).attr("data-name"))
                    $('#argument span.argvalue').each(function(){
                        args.push(new Argument($(this).attr("data-type"),$(this).attr("data-value")))
                        
                    });
                    var target=$('#target span.argvalue').attr("data-value");
                    //alert("jestem")
                    edytowanyAction=new CallFunction($(selectedFunctionSpan).attr("data-name"), new Argument("VALUE",target), $(selectedFunctionSpan).attr("data-type"), args);
                    $('#dialog-callfunction').dialog("close");
                    poprawNazweAkcji($(edytowanyActionBlock).parent().parent(), edytowanyAction);
                    functionChoiceChanged=true;
                }
               
            } 
                    
        }
        ]
        ,
        open: function() {
            $( "#dialog-callfunction" ).tooltip( "option", "disabled", false );
            $('#function ol .ui-selected').removeClass("ui-selected")
            $('#category li.ui-selected').removeClass("ui-selected")
            pageNumber=0;
            $("#category").show();
            $("#target").hide()
            $("#function ol").hide();
            $("#argument").hide();
            if(edytowanyAction!=null){
                argumentEdit=true;
                targetEdit=true;
                var selected=$(".function-details[data-type="+edytowanyAction.type+"][data-name="+edytowanyAction.name+"]");
                if(selected.size()>1){
                    argumentEdit=false;
                    targetEdit=false;
                    selected.each(function(){
                        var targetValid= true;
                        if(edytowanyAction.target!=null){
                            targetValid=isCorrectType(edytowanyAction.target.value, $("span.target",this).attr("data-type"));
                        }
                        if(targetValid){
                            var category=$(this).closest("ol").attr("id");
                            $($("#category li")[category[8]]).addClass("ui-selected");
                            $(this).closest("ol").show();
                            $(this).closest("li").addClass("ui-selected")
                            argumentEdit=true;
                            targetEdit=true;
                        }
                    })
                }
                else{
                    var category=selected.closest("ol").attr("id");

                    $($("#category li")[category[8]]).addClass("ui-selected");

                    selected.closest("ol").show();
                    selected.closest("li").addClass("ui-selected")
                    argumentEdit=true;
                    targetEdit=true;
                }
            }
        },
        close: function(){
            $( "#dialog-callfunction" ).tooltip( "option", "disabled", true );
        }
    });
}    

function createArgumentButton(name,spanArg,withManual){
    var set=$('<div><span id="'+name+'">\
        <label >'+$("#insertvalue-label-caption").text() +'</label><br>\
        <input type="radio" id="manual'+name+'" name="'+name+'" /><label for="manual'+name+'">'+$("#manual-label-caption").text()+'</label>\
        <input type="radio" id="choice'+name+'" name="'+name+'" /><label for="choice'+name+'">'+$("#choice-label-caption").text()+'</label>\
        </span>\
          <br>  <label id="val'+name+'">'+$("#value-label-caption").text()+'</label><br>\
        <span id=man'+name+'><form><fieldset>\
        <input type="text" id="text'+name+'" name="text'+name+'" class="ui-widget"/>\
       <input type="checkbox" id="checkbox'+name+'" name="checkbox'+name+'" class="ui-widget"/>\
       <input type="number" id="number'+name+'" name="number'+name+'" class="ui-widget"/>\
        </form></fieldset></span>\
        <span class="argvalue" data-value="" data-type=""></span></div>')
    // alert($(name,set).length)
    if(!withManual){
        $("#manual"+name,set).remove();
        $("label[for=\"manual"+name+"\"]",set).remove();
    }
    $('#val'+name,set).hide();
    $("#"+name,set).buttonset();
    $("form",set).hide();
    /*    $( "#number"+name, set).hide();
    $( "#checkbox"+name, set).removeClass("ui-helper-hidden-accessible");
    $( "#text"+name, set).hide*/
    set.append('<br>');
    $( "input", set).each(function(){
        $(this).click(function(){ 
                                        
            if(this.id=="choice"+name){
                $("form",set).hide();
                $('#val'+name,set).show();
                $(".argvalue",set).attr("data-value","")
                //$( "span.argvalue", set).remove();
                // $( "span.argvalue", set).text("bylem");
                createValueDialog(spanArg.attr("data-type"), true, $("span.argvalue",set));
                
                $("#dialog-value").dialog("open");
            }
            else if(this.id=="manual"+name){
                
                $('#val'+name,set).show();
                $("span.argvalue",set).attr("data-value","")
                $( "span.argvalue", set).attr("data-type",spanArg.attr("data-type"))
               
                $("form",set).show();
                if(spanArg.attr("data-type").toUpperCase()=="INTEGER"){
                    $( "#number"+name, set).show();
                    $( "#checkbox"+name, set).removeClass("ui-helper-show-accessible");
                    $( "#checkbox"+name, set).addClass("ui-helper-hidden-accessible");
                    $( "#text"+name, set).hide();
                }
                else if(spanArg.attr("data-type").toUpperCase()=="BOOLEAN"){
                    $( ".argvalue", set).attr("data-value","false");
                    $( "#number"+name, set).hide();
                    $( "#checkbox"+name, set).removeClass("ui-helper-hidden-accessible");
                    $( "#checkbox"+name, set).addClass("ui-helper-show-accessible");
                    $( "#text"+name, set).hide();
                }
                else{
                    $( "#number"+name, set).hide();
                    $( "#checkbox"+name, set).removeClass("ui-helper-show-accessible");
                    $( "#checkbox"+name, set).addClass("ui-helper-hidden-accessible");
                    $( "#text"+name, set).show();
                }
            /*$( "#man"+name+" *", set).show();
                $( "#number"+name, set).hide();
                $( "#checkbox"+name, set).removeClass("ui-helper-hidden-accessible");
                $( "#text"+name, set).hide();*/
            }
            else if(this.id=="checkbox"+name){
                $( ".argvalue", set).attr("data-value",this.value);
            }
        })
    })
    $( "#text"+name, set).keyup(function (){
        $("span.argvalue",set).attr("data-value",this.value);
    })
    $( "#number"+name, set).keyup(function (){
        $("span.argvalue",set).attr("data-value",this.value);
    })
    $( "#number"+name, set).click(function (){
        $("span.argvalue",set).attr("data-value",this.value);
    })
    $( "#checkbox"+name, set).click(function (){
        $("span.argvalue",set).attr("data-value",$("#checkbox"+name).is(":checked"));
    })
    return set;
}

function addArgumentButton(name,spanArg,withManual){
    var caption= $("#addargument-button-caption").text(); 
    return $('<button id="addargument">'+caption+'</button>').button();
}

function createValueDialog(type, showFields, resultSpan){
    var GroupSelect=$(createGroupList(type));
    var hasFields=false;
    var selectedGroup;
    var selectedField;
    var selectedValue;
    var groupName;
    //var GroupSelect=createGroupList(type);
    //$(ValueDialogContent).append(GroupSelect);
    $("#dialog-value").empty();
    GroupSelect.selectable({
        stop: function() {
            if($( "li.ui-selected",this ).size()>1){
                $( "li.ui-selected",this  ).removeClass("ui-selected");
            }
            else{
                
                groupName=$( "li.ui-selected",this ).text();
                for(var j=0;j<GroupList.length;j++){
                    if(GroupList[j].name==groupName){
                        selectedGroup=GroupList[j];
                        if(selectedGroup.fields.length>0){
                            hasFields=true;
                        // alert(hasFields);
                        }
                        else{
                            hasFields=false;
                        // alert(hasFields);
                        }
                        break;
                    }
                }
                
                var valueSelectable="";
                if(selectedGroup.values.length>0){
                    valueSelectable=$(createValueList(selectedGroup.values));
                    valueSelectable.selectable({
                        stop: function() {
                            if($( "li.ui-selected",this ).size()>1){
                                $( "li.ui-selected",this  ).removeClass("ui-selected");
                            }
                            else{
                                if(showFields && hasFields){
                                    var fieldSelectable=$(createFieldList(type, selectedGroup.fields));
                                
                                    $("#field").remove();
                                    if($("li",fieldSelectable).length>0){
                                        fieldSelectable.selectable({
                                            stop: function() {
                                                for(var i=0;i<selectedGroup.fields.length;i++){
                                                    if($("li.ui-selected",this).text()==selectedGroup.fields[i].visibleName){
                                                        selectedField=selectedGroup.fields[i]
                                                        break;
                                                    }
                                                }
                                            }
                                        });
                                    }
                                    else{
                                        hasFields=false;
                                        $("#field").remove();
                                    }
                                    $("#dialog-value").append(fieldSelectable);
                                }
                                if(!showFields || !hasFields){
                                    $("#field").remove();
                                }
                                selectedValue=selectedGroup.values[$( "li",this  ).index($("li.ui-selected",this))];
                            }
                        }
                    });
                }
                $("#field").remove();
                $("#value").remove();
                $("#dialog-value").append(valueSelectable);
            }
        }
    });
    $("#dialog-value").append(GroupSelect);
    $("#dialog-value").dialog({
        autoOpen: false,
        modal: true,
        show: 'clip',
        hide: 'clip',
        width: 500,
        buttons: [
        {
            text: $("#submit-button-caption").text(), 
            click: function() {
                if(showFields && hasFields){
                    resultSpan.text(selectedGroup.name+ "."+selectedValue.idname+"."+selectedField.visibleName);
                    resultSpan.attr("data-value", selectedValue.id+"."+selectedField.field);
                    resultSpan.attr("data-groupName", selectedGroup.name);
                }
                else{
                    resultSpan.text(selectedGroup.name+ "."+selectedValue.idname);
                    resultSpan.attr("data-value", selectedValue.id);
                    resultSpan.attr("data-groupName", selectedGroup.name);
                }
                resultSpan.attr("data-type", "VALUE");
                // alert(resultSpan.html())
                $(this).dialog("close");
            }
        },

        {
            text: $("#cancel-button-caption").text(), 
            click: function() {
                $(this).dialog("close");
            }
        }
        ]
    });
    return false;
    
    
}

function createGroupList(type){
    var result="<ol id=\"group\" class=\"groups\"> "+$("#group-label-caption").text();
               
    for(var i=0;i<GroupList.length;i++){
        var typeBool=false;
        for(var j=0;j<GroupList[i].type.length;j++){
            if(GroupList[i].type[j]==type){
                typeBool=true;
                break;
            }
        }
        if(typeBool){
            
            result+="<li class=\"ui-widget-content\">"+GroupList[i].name+ "</li>";
        }
    }
    
    result+="</ol>";
    return result;
}

function createValueList(valueList){
    var result="<ol id=\"value\" class=\"values\">"+$("#object-label-caption").text();
               
    for(var i=0;i<valueList.length;i++){
        result+="<li class=\"ui-widget-content\">"+valueList[i].idname+ "</li>";
    }
    
    result+="</ol>";
    return result;
}

function createFieldList(type, fieldList){
    var result="<ol id=\"field\" class=\"fields\">"+$("#field-label-caption").text();
               
    for(var i=0;i<fieldList.length;i++){
        if(fieldList[i].type==type){
            result+="<li class=\"ui-widget-content\">"+fieldList[i].visibleName+ "</li>";
        }
    }
    result+="</ol>";
    return result;
}
function getType(value1, value2){
    var field=false;
    var result=false;
    var groupName;
    var type1=null;
    var type2=null;
    var split;
    var hasField=false;
    if(value1.contains(".")){
        split=value1.split(".")
        hasField=true;
    }        
    for(var i=0;i<GroupList.length;i++){
        for(var j=0;j<GroupList[i].values.length;j++){
            if((hasField && GroupList[i].values[j].id==split[0])){
                for(var k=0;k<GroupList[i].fields.length;k++){
                    if(GroupList[i].fields[k].field==split[1]){
                        type1=new Array(GroupList[i].fields[k].type);
                    }
                }
            }
            else if(GroupList[i].values[j].id==split){
                type1=GroupList[i].type;
            }
            if(type1!=null) break;
        }
        if(type1!=null) break;
    }
    hasField=false;
    if(value2.contains(".")){
        split=value1.split(".")
        hasField=true;
    }
    for(var i=0;i<GroupList.length;i++){
        for(var j=0;j<GroupList[i].values.length;j++){
            if((hasField && GroupList[i].values[j].id==split[0])){
                for(var k=0;k<GroupList[i].fields.length;k++){
                    if(GroupList[i].fields[k].field==split[1]){
                        type2=new Array(GroupList[i].fields[k].type);
                    }
                }
            }
            else if(GroupList[i].values[j].id==split){
                type2=GroupList[i].type;
            }
            if(type2!=null) break;
        }
        if(type2!=null) break;
    }
    
    for(var i=0;i<type1.length;i++){
        for(var j=0;j<type2.length;j++){
            if(type1[i]==type2[j]){
                return type1[i];
            }
        }
    }
}
