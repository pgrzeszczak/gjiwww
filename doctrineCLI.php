#!/usr/bin/env php5
<?php
// Load your bootstrap or instantiate application and setup your config here
 
require_once __DIR__ .'/vendor/autoload.php';
require_once __DIR__ .'/app/config.php';
 
$app        = new Silex\Application();

registerParameters($app);
 
// Doctrine DBAL
$app->register(new Silex\Provider\DoctrineServiceProvider(), array(
    'db.options' => array(
        'driver'     => 'pdo_pgsql',
        'dbname'     => 'gjiwww',
        'host'       => 'localhost',
        'user'       => 'postgres',
        'password'   => 'postgres',
    ),
));
 
// Doctrine ORM, I like the defaults, so I've only modified the proxies dir and entities path / namespace
$app->register(new Nutwerk\Provider\DoctrineORMServiceProvider(), array(
    'db.orm.proxies_dir'           => __DIR__ . '/../cache/doctrine/proxy',
    'db.orm.proxies_namespace'     => 'DoctrineProxy',
    'db.orm.auto_generate_proxies' => true,
    'db.orm.entities'              => array(array(
        'type'      => 'annotation',       // entity definition 
        'path'      => $app['params.entity_path'],   // path to your entity classes
        'namespace' => 'GJIwww\Entities', // your classes namespace
    )),
));
 
use Doctrine\DBAL\Tools\Console\Helper\ConnectionHelper;
use Doctrine\ORM\Tools\Console\Helper\EntityManagerHelper;
use Doctrine\ORM\Tools\Console\ConsoleRunner;
use Symfony\Component\Console\Helper\HelperSet;
 
$helperSet = new HelperSet(array(
    'db' => new ConnectionHelper($app['db.orm.em']->getConnection()),
    'em' => new EntityManagerHelper($app['db.orm.em'])
));
 
ConsoleRunner::run($helperSet);