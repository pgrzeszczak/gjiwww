/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
var map;
var poly;
var firstPoint; 

function initialize() {
    var mapOptions = {
        center: new google.maps.LatLng(52.422, 16.936),
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    map = new google.maps.Map(document.getElementById("map_canvas"),
        mapOptions);
        
    var polyOptions = {
        strokeColor: '#000000',
        strokeOpacity: 1.0,
        strokeWeight: 3
    }
    poly = new google.maps.Polyline(polyOptions);
    poly.setMap(map);
    google.maps.event.addListener(map, 'click', addLatLng);
}

function calcXY(point) {
    var scale = Math.pow(2, map.getZoom());
    var nw = new google.maps.LatLng(
        map.getBounds().getNorthEast().lat(),
        map.getBounds().getSouthWest().lng()
        );
    var worldCoordinateNW = map.getProjection().fromLatLngToPoint(nw);
    var worldCoordinate = map.getProjection().fromLatLngToPoint(point);
    var pixelOffset = new google.maps.Point(
        Math.floor((worldCoordinate.x - worldCoordinateNW.x) * scale),
        Math.floor((worldCoordinate.y - worldCoordinateNW.y) * scale)
        );
    return pixelOffset;
}

function dist(a, b) {
    return Math.sqrt((a.x-b.x)*(a.x-b.x)+(a.y-b.y)*(a.y-b.y));
}

var markersArray = [];
var odcinki = new Array();

function clearOverlays() {
    for (var i = 0; i < markersArray.length; i++ ) {
        markersArray[i].setMap(null);
    }
}

var clone = (function(){ 
    return function (obj) {
        Clone.prototype=obj;
        return new Clone()
    };
    function Clone(){}
}());

function Odcinek(A, B) {
    this.A=A;
    this.B=B;
}

function crossing(odc) {
    var C=odc.A;
    var D=odc.B;
    for(var i=0; i<Zones.length; i++) {
        if(i==edytowanaZona) continue;
        if(Zones[i].poly == undefined) continue;
        var n=Zones[i].poly.getPath().getLength();
        for(var j=0; j<n; j++) {
            var A=Zones[i].poly.getPath().getAt(j%n);
            var B=Zones[i].poly.getPath().getAt((j+1)%n);
            if(((D.lat()-C.lat())*(A.lng()-D.lng())-(D.lng()-C.lng())*(A.lat()-D.lat()))  * ((D.lat()-C.lat())*(B.lng()-D.lng())-(D.lng()-C.lng())*(B.lat()-D.lat())) <0 &&
                ((B.lat()-A.lat())*(C.lng()-B.lng())-(B.lng()-A.lng())*(C.lat()-B.lat()))  * ((B.lat()-A.lat())*(D.lng()-B.lng())-(B.lng()-A.lng())*(D.lat()-B.lat())) <0
                    ) return true;
        }        
    }
    for(var i=0; i<odcinki.length; i++) {
        var A=odcinki[i].A;
        var B=odcinki[i].B;
        if(((D.lat()-C.lat())*(A.lng()-D.lng())-(D.lng()-C.lng())*(A.lat()-D.lat()))  * ((D.lat()-C.lat())*(B.lng()-D.lng())-(D.lng()-C.lng())*(B.lat()-D.lat())) <0 &&
            ((B.lat()-A.lat())*(C.lng()-B.lng())-(B.lng()-A.lng())*(C.lat()-B.lat()))  * ((B.lat()-A.lat())*(D.lng()-B.lng())-(B.lng()-A.lng())*(D.lat()-B.lat())) <0
                ) return true;
        
    }
    return false;
}
var zaznaczamyZone=true;
var newZone;

function addLatLng(event) {

    if(zaznaczamyZone) {
        var path = poly.getPath();
        path.push(event.latLng);
        var marker = new google.maps.Marker({
            position: event.latLng,
            title: '#' + path.getLength(),
            map: map
        });
        markersArray.push(marker);
        if(path.getLength()==1) firstPoint=event.latLng;
        else {
        
            if(crossing(new Odcinek(event.latLng, path.getAt(path.getLength()-2)))) 
            {
                path.pop();
                marker.setMap(null);
            }
            else
            {
                odcinki.push(new Odcinek(path.getAt(path.getLength()-1),path.getAt(path.getLength()-2)));
                if(path.getLength()>=4) {
                    if(dist(calcXY(firstPoint), calcXY(event.latLng))<=20) {
                        path.pop();
                        path.push(firstPoint);
                        odcinki.pop();
                        odcinki.push(new Odcinek(firstPoint,path.getAt(path.getLength()-2)));

                        var pathCopy = new google.maps.MVCArray();
                        for(var i=0; i<path.getLength(); i++) {
                            pathCopy.push(path.getAt(i));
                        }
        
                        newZone = new google.maps.Polygon({
                            paths: pathCopy,
                            strokeColor: "#0000FF",
                            strokeOpacity: 0.8,
                            strokeWeight: 2,
                            fillColor: "#0000FF",
                            fillOpacity: 0.35
                        });
                        newZone.setMap(map);

                        zaznaczamyZone=false;
                        path.clear();
                        clearOverlays();
                        odcinki.length=0;
                    }
                }
            }
        }
    }

}