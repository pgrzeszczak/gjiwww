<?php
namespace GJIwww\Controllers;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\NotBlank;

class AssetsControllerProvider implements ControllerProviderInterface
{
    public function connect(Application $app)
    {
        $controllers = $app['controllers_factory'];

        $controllers->get('/widget', function (Application $app) {       
            return $app['twig']->render('Assets/widget.html.twig');
        })->bind('assets_widget');
        
        $controllers->match('/upload', function (Application $app, Request $request) {
            $isAjax = $request->isXmlHttpRequest();
            $formBuilder = $app['form.factory']->createBuilder('form');
            $formBuilder->add('name', 'text',
                              array(
                                  'label' => 'form.upload_asset.name',
                                  'constraints' => new NotBlank(),
                              )
            );
            $formBuilder->add('attachment', 'file', 
                              array( 
                                  'label' => 'form.upload_asset.choose_file',
                                  'constraints' => new NotBlank(),
                              )
            );
            $form = $formBuilder->getForm();
            if ('POST' == $request->getMethod()) {
                $form->bindRequest($request);
                if ($form->isValid()) {
                    $user = $app['security']->getToken()->getUser();
                    $oldumask = umask(0);
                    $data = $form->getData();
                    $mimeType = $data['attachment']->getMimeType(); //wykonywać koniecznie przed move!
                    $newFileName = $this->moveAsset($app, $data['attachment'], $user);

                    $asset = new \GJIwww\Entities\Asset;
                    $asset->setPath($newFileName);
                    $asset->setOwner($user);
                    $asset->setName($data['name']);
                    $asset->setMimeType($mimeType);

                    $app['db.orm.em']->persist($asset);
                    $app['db.orm.em']->flush();
                    $app['session']->setFlash('notify-assets', 'notify.assets.success');

                    umask($oldumask);

                    if ($isAjax) {
                        return 'OK';
                    } else {
                        $url = $request->server->get('HTTP_REFERER');
                        if ($url == null) {
                          $url = $app['url_generator']->generate('homepage');
                        }
                        return $app->redirect($url);
                    }
                } else {
                    if ($isAjax) {
                        return 'NIEOK';
                    }
                }
            }
            return $app['twig']->render('Assets/upload.html.twig', array('form' => $form->createView()));
        })->bind('assets_upload');
        
        $controllers->get('/list', function (Application $app) {
            $user = $app['security']->getToken()->getUser();
            $assets = $user->getAssets();
            return $app['twig']->render('Assets/list.html.twig', array('assets' => $assets));
        })->bind('assets_list');
        
        $controllers->get('/view/{id}', function (Application $app, $id) {
            $user = $app['security']->getToken()->getUser();
            $asset = $app['db.orm.em']->getRepository('GJIwww\Entities\Asset')->find($id);
            if ($asset === null) {
                $app->abort(404);
            }
            if ($asset->getOwner()->getId() !== $user->getId()) {
                $app->abort(403);
            }
            $content = file_get_contents($asset->getAbsolutePath($app));
            $headers = array('Content-Type' => $asset->getMimeType());
            return new Response($content, 200, $headers);
        })->bind('assets_view');

        return $controllers;
    }
    
    protected function moveAsset(Application $app, $attachment, \GJIwww\Entities\User $user)
    {
        $dirName = $user->getAssetsPath($app);
        if (!file_exists($dirName)) {
            mkdir($dirName);
        }
        
        $filename = $this->buildFilename($attachment, $this->generateRandomFilename());
        while (file_exists($dirName . '/' . $filename)) {
            $filename = $this->buildFilename($attachment, $this->generateRandomFilename());
        }
        
        $attachment->move($dirName, $filename);
        
        return $filename;
    }
    
    protected function buildFilename(\Symfony\Component\HttpFoundation\File\UploadedFile $attachment, $filename)
    {
        $extension = pathinfo($attachment->getClientOriginalName(), PATHINFO_EXTENSION);
        if ($extension) {
            $filename .= '.' . $extension;
        }
        return $filename;
    }
    
    protected function generateRandomFilename($length = 10) {    
        return substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
    }
}
