/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
var xml_result="";
function generate_xml() {
    xml_result="";
    xml_result+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
    xml_result+="<scenario name=\""+
        $("#scenarioNameText").val()+
        "\" description=\""+
        $("#scenarioDescriptionText").val()+
        "\">\n";

    xml_result+="<objects>\n";
    generate_xml_zones();
    generate_xml_characters();
    generate_xml_items();
    generate_xml_quests();
    generate_xml_medias();
    generate_xml_timers();
    generate_xml_primitives();
    xml_result+="</objects>\n";
    
    if(scenario.events.length>0) {
        generate_xml_events(scenario);
    }
    
    xml_result+="</scenario>\n";
    $("#xml-result").empty()
    $("#xml-result").text(xml_result);
    scenarioJSON={"private":false,"name":$("#scenarioNameText").val(),"description":$("#scenarioDescriptionText").val(),"tags":"","splash":-1,"assets":[],"xml":xml_result,"password":null};
    //return xml_result;
// alert(xml_result);
}

function generate_xml_zones() {
    xml_result+="<zones>\n";
    for(var i=0; i<Zones.length; i++) {
        xml_result+="<zone id=\"";
        xml_result+=Zones[i].id;
        xml_result+="\" idname=\"";
        xml_result+=Zones[i].idname;
        xml_result+="\" name=\"";
        xml_result+=Zones[i].name;
        xml_result+="\" description=\"";
        xml_result+=Zones[i].description;
        xml_result+="\" state=\"";
        xml_result+=Zones[i].state;
        xml_result+="\" visible=\"";
        xml_result+=Zones[i].visible;
        xml_result+="\" owned=\"";
        xml_result+=Zones[i].owned;
        if(Zones[i].icon!=null && Zones[i].icon!="") {
            xml_result+="\" icon=\"";
            xml_result+=Zones[i].icon;
        }
        xml_result+="\">\n";
        if(Zones[i].poly != undefined) {
            xml_result+="<points>\n";
            for(var j=0; j<Zones[i].poly.getPath().getLength(); j++) {
                xml_result+="<point latitude=\""+Zones[i].poly.getPath().getAt(j).lat()+"\" longitude=\""+Zones[i].poly.getPath().getAt(j).lng()+"\"/>\n";
            }
            xml_result+="</points>\n";
        }
        if(Zones[i].events.length>0) {
            generate_xml_events(Zones[i]);
        }
        xml_result+="</zone>\n";        
    }
    xml_result+="</zones>\n";
}

function generate_xml_items() {
    xml_result+="<items>\n";
    for(var i=0; i<Items.length; i++) {
        xml_result+="<item id=\"";
        xml_result+=Items[i].id;
        xml_result+="\" idname=\"";
        xml_result+=Items[i].idname;
        xml_result+="\" name=\"";
        xml_result+=Items[i].name;
        xml_result+="\" description=\"";
        xml_result+=Items[i].description;
        xml_result+="\" zone=\"";
        xml_result+=Items[i].zone;
        xml_result+="\" state=\"";
        xml_result+=Items[i].state;
        xml_result+="\" visible=\"";
        xml_result+=Items[i].visible;
        xml_result+="\" owned=\"";
        xml_result+=Items[i].owned;
        if(Items[i].icon!=null && Items[i].icon!="") {
            xml_result+="\" icon=\"";
            xml_result+=Items[i].icon;
        }
        xml_result+="\">\n";
        if(Items[i].events.length>0) {
            generate_xml_events(Items[i]);
        }
        xml_result+="</item>\n";        
    }
    xml_result+="</items>\n";
}

function generate_xml_characters() {
    xml_result+="<characters>\n";
    for(var i=0; i<Characters.length; i++) {
        xml_result+="<character id=\"";
        xml_result+=Characters[i].id;
        xml_result+="\" idname=\"";
        xml_result+=Characters[i].idname;
        xml_result+="\" name=\"";
        xml_result+=Characters[i].name;
        xml_result+="\" sex=\"";
        xml_result+=Characters[i].sex;
        xml_result+="\" description=\"";
        xml_result+=Characters[i].description;
        xml_result+="\" zone=\"";
        xml_result+=Characters[i].zone;
        xml_result+="\" state=\"";
        xml_result+=Characters[i].state;
        xml_result+="\" visible=\"";
        xml_result+=Characters[i].visible;
        xml_result+="\" owned=\"";
        xml_result+=Characters[i].owned;
        if(Characters[i].icon!=null && Characters[i].icon!="") {
            xml_result+="\" icon=\"";
            xml_result+=Characters[i].icon;
        }
        xml_result+="\">\n";
        if(Characters[i].events.length>0) {
            generate_xml_events(Characters[i]);
        }
        xml_result+="</character>\n";        
    }
    xml_result+="</characters>\n";
}

function generate_xml_quests() {
    xml_result+="<quests>\n";
    for(var i=0; i<Quests.length; i++) {
        xml_result+="<quest id=\"";
        xml_result+=Quests[i].id;
        xml_result+="\" idname=\"";
        xml_result+=Quests[i].idname;
        xml_result+="\" name=\"";
        xml_result+=Quests[i].name;
        xml_result+="\" description=\"";
        xml_result+=Quests[i].description;
        xml_result+="\" state=\"";
        xml_result+=Quests[i].state;
        if(Quests[i].icon!=null && Quests[i].icon!="") {
            xml_result+="\" icon=\"";
            xml_result+=Quests[i].icon;
        }
        xml_result+="\">\n";
        if(Quests[i].events.length>0) {
            generate_xml_events(Quests[i]);
        }
        xml_result+="</quest>\n";
    }
    xml_result+="</quests>\n";
}

function generate_xml_medias() {
    xml_result+="<medias>\n";
    for(var i=0; i<Medias.length; i++) {
        xml_result+="<media id=\"";
        xml_result+=Medias[i].id;
        xml_result+="\" idname=\"";
        xml_result+=Medias[i].idname;
        xml_result+="\" path=\"";
        xml_result+=Medias[i].pathname;
        xml_result+="\" type=\"";
        xml_result+=Medias[i].type;
        xml_result+="\">\n";
        if(Medias[i].events.length>0) {
            generate_xml_events(Medias[i]);
        }
        xml_result+="</media>\n";
    }
    xml_result+="</medias>\n";
}

function generate_xml_timers() {
    xml_result+="<timers>\n";
    for(var i=0; i<Timers.length; i++) {
        xml_result+="<timer id=\"";
        xml_result+=Timers[i].id;
        xml_result+="\" idname=\"";
        xml_result+=Timers[i].idname;
        xml_result+="\" interval=\"";
        xml_result+=Timers[i].interval;
        xml_result+="\">\n";
        if(Timers[i].events.length>0) {
            generate_xml_events(Timers[i]);
        }
        xml_result+="</timer>\n";
    }
    xml_result+="</timers>\n";
}

function generate_xml_primitives() {
    xml_result+="<primitives>\n";
    for(var i=0; i<Variables.length; i++) {
        if(Variables[i].type=="STRING") xml_result+="<string id=\"";
        else if(Variables[i].type=="BOOLEAN") xml_result+="<boolean id=\"";
        else if(Variables[i].type=="CHAR") xml_result+="<char id=\"";
        else if(Variables[i].type=="DOUBLE") xml_result+="<double id=\"";
        else if(Variables[i].type=="INTEGER") xml_result+="<integer id=\"";
        xml_result+=Variables[i].id;
        xml_result+="\" idname=\"";
        xml_result+=Variables[i].idname;
        xml_result+="\" value=\"";
        xml_result+=Variables[i].value;
        xml_result+="\">\n";
        if(Variables[i].events.length>0) {
            generate_xml_events(Variables[i]);
        }
        if(Variables[i].type=="STRING") xml_result+="</string>\n";
        else if(Variables[i].type=="BOOLEAN") xml_result+="</boolean>\n";
        else if(Variables[i].type=="CHAR") xml_result+="</char>\n";
        else if(Variables[i].type=="DOUBLE") xml_result+="</double>\n";
        else if(Variables[i].type=="INTEGER") xml_result+="</integer>\n";
    }
    xml_result+="</primitives>\n";
}


function generate_xml_events(object) {
    xml_result+="<events>\n";
    for(var i=0; i<object.events.length; i++) {
        xml_result+="<event type=\""+object.events[i].name+"\">\n";
        generate_xml_actions(object.events[i].actions);
        xml_result+="</event>\n";
    }
    xml_result+="</events>\n";
}

function generate_xml_arguments(action) {
    xml_result+="<arguments>\n";
    for(var i=0; i<action.arguments.length; i++) {
        xml_result+="<argument type=\""+action.arguments[i].type+"\" value=\""+action.arguments[i].value+"\"/>\n";
    }
    xml_result+="</arguments>\n";
}

function generate_xml_conditions(conditions) {
    if(conditions.lol=="complex") {
        
        if(conditions.type=="AND") {
            xml_result+="<and>\n";
            for(var i=0; i<conditions.conditions.length; i++) {
                generate_xml_conditions(conditions.conditions[i]);
            }        
            xml_result+="</and>\n";
        }
        else if(conditions.type=="OR") {
            xml_result+="<or>\n";
            for(var i=0; i<conditions.conditions.length; i++) {
                generate_xml_conditions(conditions.conditions[i]);
            }        
            xml_result+="</or>\n";         
        }
        
    
    }
    else if(conditions.lol=="simple"){
        xml_result+="<condition eastSide=\"";
        xml_result+=conditions.eastSideValue;
        xml_result+="\" eastSideType=\"";
        xml_result+=conditions.eastSideType;
        xml_result+="\" westSide=\"";
        xml_result+=conditions.westSideValue;
        xml_result+="\" westSideType=\"";
        xml_result+=conditions.westSideType;
        xml_result+="\" operator=\"";
        xml_result+=conditions.operator;
        xml_result+="\">\n";
        xml_result+="</condition>\n";
    }
    else {
        xml_result+="<error>\n";
        xml_result+="</error>\n";
    }
}

function generate_xml_actions(abek) {
    //xml_result+=abek.actions.length+"\n";
    if(abek.type=="block") {
        for(var i=0; i<abek.actions.length; i++) {
            generate_xml_actions(abek.actions[i]);
        }
    }
    else if(abek.type=="for") {
        xml_result+="<for>\n";
        generate_xml_actions(abek.toDo);
        xml_result+="</for>\n";
    }
    else if(abek.type=="if") {
        xml_result+="<ifStatement>\n";
        xml_result+="<conditions>\n";
        generate_xml_conditions(abek.cond);
        xml_result+="</conditions>\n";
        xml_result+="<if>\n";
        generate_xml_actions(abek.doIf);
        xml_result+="</if>\n";
        xml_result+="<else>\n";
        generate_xml_actions(abek.doElse);
        xml_result+="</else>\n";
        xml_result+="</ifStatement>\n";
    }    
    else if(abek.type=="while") {
        xml_result+="<while>\n";
        xml_result+="<conditions>\n";
        generate_xml_conditions(abek.cond);
        xml_result+="</conditions>\n";
        xml_result+="<do>\n";
        generate_xml_actions(abek.toDo);
        xml_result+="</do>\n";
        xml_result+="</while>\n";        
    }
    else if(abek.type=="action") {
        xml_result+="<callFunction name=\""+abek.action.name+"\" type=\""+abek.action.type+"\">\n";
        generate_xml_arguments(abek.action);
        xml_result+="</callFunction>\n";
    }
}