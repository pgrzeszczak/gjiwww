var xmlDoc;
var lll=5;
function parse_xml(xml_string) {
    //alert(xml_string);
    jxmlDoc=$.parseXML(xml_string);
    if (jxmlDoc.documentElement.nodeName=="parsererror")
    {
        errStr=jxmlDoc.documentElement.childNodes[0].nodeValue;
        errStr=errStr.replace(/</g, "&lt;");
        document.write(errStr);
    }
    else
    {
    //document.write("XML is valid");
    } 
    parse_xml_scenario();
    parse_xml_zones();
    parse_xml_characters();
    parse_xml_items();
    parse_xml_quests();
    parse_xml_medias();
    parse_xml_timers();
    parse_xml_variables();
    parse_xml_events($(this).children("events"), scenario);
}

function parse_xml_scenario() {
    var name=$(jxmlDoc).find("scenario").first().attr("name");
    var desc=$(jxmlDoc).find("scenario").first().attr("description");
    $("#scenarioNameText").val(name);
    $("#scenarioDescriptionText").val(desc);
}

function parse_xml_zones() {
    $(jxmlDoc).find("zone").each(function() {
        var id=$(this).attr("id");
        var idname=$(this).attr("idname");
        var name=$(this).attr("name");
        var description=$(this).attr("description");
        var state=$(this).attr("state");
        var visible=$(this).attr("visible");
        var owned=$(this).attr("owned");
        var icon=$(this).attr("icon");
        var zone=new Zone(idname, name, description, state, visible,"points",owned,icon);
        zone.id=id;
        parse_xml_points(this, zone);
        parse_xml_events(this, zone);
        Zones.push(zone);
    });
}

function parse_xml_characters() {
    $(jxmlDoc).find("character").each(function() {
        var id=$(this).attr("id");
        var idname=$(this).attr("idname");
        var name=$(this).attr("name");
        var sex=$(this).attr("sex");
        var description=$(this).attr("description");
        var zone=$(this).attr("zone");
        var state=$(this).attr("state");
        var visible=$(this).attr("visible");
        var owned=$(this).attr("owned");
        var icon=$(this).attr("icon");
        var character=new Character(idname, name, sex, description, zone, state, visible, owned, icon);
        character.id=id;
        parse_xml_events(this, character);
        Characters.push(character);
    });
}

function parse_xml_items() {
    $(jxmlDoc).find("item").each(function() {
        var id=$(this).attr("id");
        var idname=$(this).attr("idname");
        var name=$(this).attr("name");
        var description=$(this).attr("description");
        var zone=$(this).attr("zone");
        var state=$(this).attr("state");
        var visible=$(this).attr("visible");
        var owned=$(this).attr("owned");
        var icon=$(this).attr("icon");
        var item=new Item(idname, name, description, zone, visible, state, owned, icon);
        item.id=id;
        parse_xml_events(this, item);
        Items.push(item);
    });
}

function parse_xml_quests() {
    $(jxmlDoc).find("quest").each(function() {
        var id=$(this).attr("id");
        var idname=$(this).attr("idname");
        var name=$(this).attr("name");
        var description=$(this).attr("description");
        var state=$(this).attr("state");
        var visible=$(this).attr("visible");
        var icon=$(this).attr("icon");
        var quest=new Quest(idname, name, description, state,visible, icon);
        quest.id=id;
        parse_xml_events(this, quest);
        Quests.push(quest);
    });
}

function parse_xml_medias() {
    $(jxmlDoc).find("media").each(function() {
        var id=$(this).attr("id");
        var idname=$(this).attr("idname");
        var pathname=$(this).attr("path");
        var type=$(this).attr("type");
        var media=new Media(idname, "pathid",pathname, type);
        media.id=id;
        parse_xml_events(this, media);
        Medias.push(media);
    });
}

function parse_xml_timers() {
    $(jxmlDoc).find("timer").each(function() {
        var id=$(this).attr("id");
        var idname=$(this).attr("idname");
        var interval=$(this).attr("interval");
        var timer=new Timer(idname, interval);
        timer.id=id;
        parse_xml_events(this, timer);
        Timers.push(timer);
    });
}

function parse_xml_variables() {
    $(jxmlDoc).find("integer").each(function() {
        var id=$(this).attr("id");
        var idname=$(this).attr("idname");
        var value=$(this).attr("value");
        var type="INTEGER";
        var variable=new Variable(idname, type,value);
        variable.id=id;
        parse_xml_events(this, variable);
        Variables.push(variable);
    });
    $(jxmlDoc).find("string").each(function() {
        var id=$(this).attr("id");
        var idname=$(this).attr("idname");
        var value=$(this).attr("value");
        var type="STRING";
        var variable=new Variable(idname, type,value);
        variable.id=id;
        parse_xml_events(this, variable);
        Variables.push(variable);
    });
    $(jxmlDoc).find("boolean").each(function() {
        var id=$(this).attr("id");
        var idname=$(this).attr("idname");
        var value=$(this).attr("value");
        var type="BOOLEAN";
        var variable=new Variable(idname, type,value);
        variable.id=id;
        parse_xml_events(this, variable);
        Variables.push(variable);
    });
    $(jxmlDoc).find("char").each(function() {
        var id=$(this).attr("id");
        var idname=$(this).attr("idname");
        var value=$(this).attr("value");
        var type="CHAR";
        var variable=new Variable(idname, type,value);
        variable.id=id;
        parse_xml_events(this, variable);
        Variables.push(variable);
    });
    $(jxmlDoc).find("double").each(function() {
        var id=$(this).attr("id");
        var idname=$(this).attr("idname");
        var value=$(this).attr("value");
        var type="DOUBLE";
        var variable=new Variable(idname, type,value);
        variable.id=id;
        parse_xml_events(this, variable);
        Variables.push(variable);
    });
}

function parse_xml_events(where, object) {
    $(where).find("event").each(function() {
        var event = new Event("...");
        event.name=$(this).attr("type");
        //alert(event.name);
        //alert($(this)[0].nodeName);
        event.actions=new ActionBlock();
        parse_xml_actions(this, event.actions);
        object.events.push(event);
    //alert("dodałem event!");
    }); 
}

function parse_xml_actions(where, obj) {
    var cond="";
    //console.log("parse_xml_actions!");
    if($(where).children().length>0) {
        //obj.actions=new ActionBlock();
        $(where).children().each(function() {
            //alert("estem w child");
            var abek=new ActionBlock();
            //console.log($(this)[0].nodeName);
            if($(this)[0].nodeName=="for") {
                abek=new ForBlock();
                abek.toDo=new ActionBlock();
                parse_xml_actions(this,abek.toDo);
            
            } 
            else if($(this)[0].nodeName=="while") {
                abek=new WhileBlock();
                cond="";
                parse_xml_conditions($(this).children("conditions"), cond);
                abek.cond=cond;
                abek.toDo=new ActionBlock();
            }
            else if($(this)[0].nodeName=="ifStatement") {
                if(lll>0) {
                    //alert($(this)[0].nodeName + "   "+$(this).nodeName);
                    lll--;
                }
                abek=new IfBlock();
                //$("if")
                //else if($(this)[0].nodeName=="and")
                //var cond=new ComplexCondition("AND", new Array());
                cond=new ComplexCondition("new", new Array());
                parse_xml_conditions($(this).children("conditions"), cond);
                console.log("Wstawiam:"); 
                console.log(cond);
                abek.cond=cond;
                abek.doIf=new ActionBlock();
                parse_xml_actions($(this).children("if").last(), abek.doIf);
                abek.doElse=new ActionBlock();
                parse_xml_actions($(this).children("else").last(), abek.doElse);
            }
            else if($(this)[0].nodeName=="callFunction") {
                abek=new Action();
                var name=$(this).attr("name");
                var type=$(this).attr("type");
                var args = new Array();
                //argumens.push(new Argument("atype", "avalue"));
                //console.log(arguments);
                $(this).find("argument").each(function(){
                    var atype=$(this).attr("type");
                    var aval=$(this).attr("value");
                    args.push(new Argument(atype,aval));
                });
                abek.action=new CallFunction(name,"target", type, args);
                
            }
            //abek.type=$(this).nodeName;
            
            //alert($(this)[0].nodeName);
            //parse_xml_action(this, event);
            
            obj.actions.push(abek);
        // console.log(obj.actions.length);
        });
    }
    else {
        obj.actions=new ActionBlock();
    }
}

function parse_xml_conditions(where, cond) {
    
    $(where).children().each(function(){
        console.log("parsam - "+$(this)[0].nodeName);
        console.log(cond);
        //onsole.log("cond: "+cond);
        if($(this)[0].nodeName=="condition") {
            var operator=$(this).attr("operator");
            var eastSideValue=$(this).attr("eastSide");
            var eastSideType=$(this).attr("eastSideType");
            var westSideValue=$(this).attr("westSide");
            var westSideType=$(this).attr("westSideType");
            var eastDataType;
            var westDataType;
            if(westSideType=="VALUE" && eastSideType=="VALUE") {
                vall=getType(westSideValue, eastSideValue);
                eastDataType=vall;
                westDataType=vall;
            }
            else
            {
                var vall;
                if(westSideType!="VALUE") vall=westSideType;
                else vall=eastSideType;
                eastDataType=vall;
                westDataType=vall;
            }
            
            //cond
            if(cond.type!="new") {
                console.log(cond);
                cond.conditions.push(new SimpleCondition(type, eastSideType,eastDataType, eastSideValue, operator, westSideType, westDataType,westSideValue));
                console.log("Wstawiłem simpleCondition.");
                console.log(cond);
            }
            else {
                cond=new SimpleCondition(type, eastSideType,eastDataType, eastSideValue, operator, westSideType, westDataType,westSideValue);
            }
        }
        else if($(this)[0].nodeName=="and") {
            if(cond.type!="new") {
                cond.conditions.push(new ComplexCondition("AND", new Array()));
            }
            else {
                cond.type="AND";
            }
            parse_xml_conditions(this, cond);
        }
        else if($(this)[0].nodeName=="or") {
            
        }
        else if($(this)[0].nodeName=="conditions") {
            
        }
    });
    console.log("Wychodzę: ");
    console.log(cond);
    
}

function parse_xml_points(obj, zone) {
    var path = new google.maps.MVCArray();
    $(obj).find("point").each(function(){
        var lat=$(this).attr("latitude");
        var lng=$(this).attr("longitude");
        path.push(new google.maps.LatLng(lat,lng));
           
    });
        
    newZone = new google.maps.Polygon({
        paths: path,
        strokeColor: "#0000FF",
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: "#0000FF",
        fillOpacity: 0.35
    });
    newZone.setMap(map);
    zone.poly=newZone;
}