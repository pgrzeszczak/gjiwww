<?php
namespace GJIwww\Forms;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

class RegisterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add("username", "text", array(
            "label" => "form.register.username",
             "constraints" => array(
                new Assert\MinLength(array('limit'=>3, 
                    "message"=>"form.register.errors.username_to_short")),
                new Assert\MaxLength(array('limit'=>50,
                    "message"=>"form.register.errors.username_to_long"))
             ),
        ))->add("plainPassword", "repeated", array(
            "type" => "password",
            "first_options" => array(
                "label" => "form.register.password",
            ),
            "second_options" => array(
                "label" => "form.register.password_repeat",
            ),
            'invalid_message' => 'form.register.errors.password_dont_match',
            "constraints" => array(
                new Assert\MinLength(array('limit'=>3, 
                    "message"=>"form.register.errors.password_to_short")),
                new Assert\MaxLength(array('limit'=>50,
                    "message"=>"form.register.errors.password_to_long"))
             ),
        ))->add("firstname", "text", array(
            "label" => "form.register.firstname",
            "constraints" => array(
                new Assert\MinLength(array('limit'=>3, 
                    "message"=>"form.register.errors.firstname_to_short")),
                new Assert\MaxLength(array('limit'=>50,
                    "message"=>"form.register.errors.firstname_to_long"))
             ),
        ))->add("lastname", "text", array(
            "label" => "form.register.lastname",
            "constraints" => array(
                new Assert\MinLength(array('limit'=>3, 
                    "message"=>"form.register.errors.lastname_to_short")),
                new Assert\MaxLength(array('limit'=>50,
                    "message"=>"form.register.errors.lastname_to_long"))
             ),
        ))->add("email", "email", array(
            "label" => "form.register.email",
            "constraints" => array(
                new Assert\Callback(array(
                    'methods' => array(array($this, 'isEmailValid')))),
             ),
        ))->add("termsAccepted", "checkbox", array(
            "label" => "form.register.termsAccepted",
            "constraints" => array(
                new Assert\True(array(
                    "message"=>"form.register.errors.terms_not_accepted")),
             ),
        ));
    }

    public function getName()
    {
        return "register";
    }
    
    /**
     * Walidacja maila
     * @param type $email
     * @param \Symfony\Component\Validator\ExecutionContext $context
     * @return boolean
     */
    public function isEmailValid($email, \Symfony\Component\Validator\ExecutionContext $context) {
        if(filter_var($email, FILTER_VALIDATE_EMAIL)){
            return true;
        } else {
            $context->addViolationAtSubPath('email', 'form.register.errors.wrong_email', array(), null);
            return true;
        }
    }
}

?>
