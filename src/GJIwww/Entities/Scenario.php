<?php

namespace GJIwww\Entities;

/**
 * @Table(name="scenario")
 * @Entity(repositoryClass="ScenarioRepository")
 */
class Scenario {
    
    const ORDERING_NEWEST = 1;
    
    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @var string
     * @Column(name="name", type="string")
     */
    protected $name;
    /**
     * @ManyToOne(targetEntity="GJIwww\Entities\User", inversedBy="scenarios")
     * @JoinColumn(name="author_id", referencedColumnName="id")
     */
    protected $author;
    /**
     * @var DateTime
     * @Column(name="created", type="datetime")
     */
    protected $created;
    /**
     * @var string
     * @Column(name="tags", type="string", nullable=true)
     */
    protected $tags;
    /**
     * @var string
     * @Column(name="description", type="string", nullable=true)
     */
    protected $description;
    /**
     * @ManyToOne(targetEntity="GJIwww\Entities\Asset")
     * @JoinColumn(name="splash_asset_id", referencedColumnName="id", nullable=true)
     */
    protected $splashAsset;
    /**
     * @ManyToMany(targetEntity="GJIwww\Entities\Asset")
     * @OrderBy({"id" = "ASC"})
     */
    protected $usedAssets;
    /**
     * @var boolean
     * @Column(name="private", type="boolean")
     */
    protected $private;
    /**
     * @var string
     * @Column(name="content", type="text", nullable=true)
     */
    protected $content;
    /**
     * @var string
     * @Column(name="password", type="string", nullable=true)
     */
    protected $password;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->usedAssets = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Scenario
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Scenario
     */
    public function setCreated($created)
    {
        $this->created = $created;
    
        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set tags
     *
     * @param string $tags
     * @return Scenario
     */
    public function setTags($tags)
    {
        $this->tags = $tags;
    
        return $this;
    }

    /**
     * Get tags
     *
     * @return string 
     */
    public function getTags()
    {
        return $this->tags;
    }
    
    /**
     * Get tags
     *
     * @return array 
     */
    public function getTagsArray()
    {
        return explode(',', $this->tags);
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Scenario
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return Scenario
     */
    public function setContent($content)
    {
        $this->content = $content;
    
        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent()
    {
        return $this->content;
    }
    
    /**
     * Set password
     *
     * @param string $password
     * @return Scenario
     */
    public function setPassword($password)
    {
        $this->password = $password;
    
        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }
    
    /**
     * Set private
     *
     * @param boolean $private
     * @return Scenario
     */
    public function setPrivate($private)
    {
        $this->private = $private;
    
        return $this;
    }

    /**
     * Is private
     *
     * @return boolean 
     */
    public function isPrivate()
    {
        return $this->private;
    }

    /**
     * Set author
     *
     * @param \GJIwww\Entities\User $author
     * @return Scenario
     */
    public function setAuthor(\GJIwww\Entities\User $author = null)
    {
        $this->author = $author;
    
        return $this;
    }

    /**
     * Get author
     *
     * @return \GJIwww\Entities\User 
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set splashAsset
     *
     * @param \GJIwww\Entities\Asset $splashAsset
     * @return Scenario
     */
    public function setSplashAsset(\GJIwww\Entities\Asset $splashAsset = null)
    {
        $this->splashAsset = $splashAsset;
    
        return $this;
    }

    /**
     * Get splashAsset
     *
     * @return \GJIwww\Entities\Asset 
     */
    public function getSplashAsset()
    {
        return $this->splashAsset;
    }

    /**
     * Add usedAssets
     *
     * @param \GJIwww\Entities\Asset $usedAssets
     * @return Scenario
     */
    public function addUsedAsset(\GJIwww\Entities\Asset $usedAssets)
    {
        $this->usedAssets[] = $usedAssets;
    
        return $this;
    }

    /**
     * Remove usedAssets
     *
     * @param \GJIwww\Entities\Asset $usedAssets
     */
    public function removeUsedAsset(\GJIwww\Entities\Asset $usedAssets)
    {
        $this->usedAssets->removeElement($usedAssets);
    }

    /**
     * Get usedAssets
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUsedAssets()
    {
        return $this->usedAssets;
    }
    
    /**
     * Set usedAssets
     *
     * $param \Doctrine\Common\Collections\Collection $assets
     */
    public function setUsedAssets(\Doctrine\Common\Collections\Collection $assets)
    {
        $this->usedAssets = $assets;
        
        return $this;
    }
    
    public function getArrayOfAssetIds() {
        $ids = array();
        foreach ($this->usedAssets as $asset) {
            $ids[] = $asset->getId();
        }
        return $ids;
    }
    
    public function getXMLInfoNode($dd) {
        $scenario = $dd->createElement('scenario');
        $scenario->setAttribute('id', $this->id);
        $scenario->setAttribute('name', $this->name);
        $scenario->setAttribute('description', $this->description);
        $scenario->setAttribute('author', $this->getAuthor()->getFirstname() . ' ' . $this->getAuthor()->getLastname());
        $scenario->setAttribute('created', $this->getCreated()->format("Y-m-d H:i"));
        return $scenario;
    }
}