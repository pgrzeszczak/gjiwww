#!/bin/sh

#tworzenie wymaganych folderów
if [ ! -d "web/public" ]; then
    mkdir -p web/public
fi

if [ ! -d "cache/doctrine/proxy" ]; then
    mkdir -p cache/doctrine/proxy
fi

#czyszczenie cache
./cleanCache.sh

#ustawianie praw dostępu
if [ -n "$(getent passwd apache)" ]; then
# user apache exists
    setfacl -R -m u:apache:rwx web/public
    setfacl -dR -m u:apache:rwx web/public
    setfacl -R -m u:apache:rwx cache/doctrine/proxy
    setfacl -dR -m u:apache:rwx cache/doctrine/proxy
fi

if [ -n "$(getent passwd www-data)" ]; then
# user www-data exists
    setfacl -R -m u:www-data:rwx web/public
    setfacl -dR -m u:www-data:rwx web/public
    setfacl -R -m u:www-data:rwx cache/doctrine/proxy
    setfacl -dR -m u:www-data:rwx cache/doctrine/proxy
fi

setfacl -R -m u:$currentUser:rwx web/public
setfacl -dR -m u:$currentUser:rwx web/public
setfacl -R -m u:$currentUser:rwx cache/doctrine/proxy
setfacl -dR -m u:$currentUser:rwx cache/doctrine/proxy

#composer update
sudo composer.phar self-update

#composer install
#curl -s https://getcomposer.org/installer | php
#
#vendors update
composer.phar update