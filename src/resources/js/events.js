function addZoneEventsFunctions() {
    $("#zonesList ol li").each(function() {
        if(!hasEvent(Zones[$(this).parent().attr("data-idek")],$(this).attr("data-event"))) {
            $(this).remove();
        }
    });
    $(".addZoneEvent").click(function(){
        edytowanaZona=this.getAttribute("data-idek");
        edytowanyObject=Zones[this.getAttribute("data-idek")];
        $("#zoneEvents li").each(function() {
            if(hasEvent(edytowanyObject,$(this).attr("data-event"))) {
                $(this).css("display", "none");
            }
            else{
                $(this).css("display", "block");
            }
        })
        $("#addZoneEvent").dialog("open");
    });
    $("#zoneEvents").selectable({
        stop: function() {
            if($( "li.ui-selected",this ).size()>1){
                $( "li.ui-selected",this  ).removeClass("ui-selected");
            }
            else{
                var tempik=$( "li.ui-selected",this ).first();
                edytowanyEvent=tempik.attr("data-event"); 
            } 
        }
    });
    $(".addedZoneEvents").selectable({
        stop: function() {
            if($( "li.ui-selected",this ).size()>1){
                $( "li.ui-selected",this  ).removeClass("ui-selected");
            }
            else{
                var tempik=$( "li.ui-selected",this ).first();
                edytowanaZona=this.getAttribute("data-idek");
                edytowanyObject=Zones[this.getAttribute("data-idek")];
                edytowanyEvent=tempik.attr("data-event"); 
                edytowanyEventObject=getEvent(edytowanyObject, edytowanyEvent);
                $("#editZoneEvent").dialog("open"); 
            } 
        }
    });
}

function addCharacterEventsFunctions() {
    $("#charactersList ol li").each(function() {
        if(!hasEvent(Characters[$(this).parent().attr("data-idek")],$(this).attr("data-event"))) {
            $(this).remove();
        }
    });
    $(".addCharacterEvent").click(function(){
        edytowanyCharacter=this.getAttribute("data-idek");
        edytowanyObject=Characters[this.getAttribute("data-idek")];
        $("#characterEvents li").each(function() {
            if(hasEvent(edytowanyObject,$(this).attr("data-event"))) {
                $(this).css("display", "none");
            }
            else{
                $(this).css("display", "block");
            }
        })
        $("#addCharacterEvent").dialog("open");
    });    

    $("#characterEvents").selectable({
        stop: function() {
            if($( "li.ui-selected",this ).size()>1){
                $( "li.ui-selected",this  ).removeClass("ui-selected");
            }
            else{
                var tempik=$( "li.ui-selected",this ).first();
                edytowanyEvent=tempik.attr("data-event"); 
            } 
        //alert(edytowanyEvent);
        }
    });
    $(".addedCharacterEvents").selectable({
        stop: function() {
            if($( "li.ui-selected",this ).size()>1){
                $( "li.ui-selected",this  ).removeClass("ui-selected");
            }
            else{
                var tempik=$( "li.ui-selected",this ).first();
                edytowanyCharacter=this.getAttribute("data-idek");
                edytowanyObject=Characters[this.getAttribute("data-idek")];
                edytowanyEvent=tempik.attr("data-event"); 
                edytowanyEventObject=getEvent(edytowanyObject, edytowanyEvent);
                $("#editCharacterEvent").dialog("open"); 
            } 
        }
    });
}

function addItemEventsFunctions() {
    $("#itemsList ol li").each(function() {
        if(!hasEvent(Items[$(this).parent().attr("data-idek")],$(this).attr("data-event"))) {
            $(this).remove();
        }
    });
    $(".addItemEvent").click(function(){
        edytowanyItem=this.getAttribute("data-idek");
        edytowanyObject=Items[this.getAttribute("data-idek")];
        $("#itemEvents li").each(function() {
            if(hasEvent(edytowanyObject,$(this).attr("data-event"))) {
                $(this).css("display", "none");
            }
            else{
                $(this).css("display", "block");
            }
        })
        $("#addItemEvent").dialog("open");
    });    

    $("#itemEvents").selectable({
        stop: function() {
            if($( "li.ui-selected",this ).size()>1){
                $( "li.ui-selected",this  ).removeClass("ui-selected");
            }
            else{
                var tempik=$( "li.ui-selected",this ).first();
                edytowanyEvent=tempik.attr("data-event"); 
            } 
        //alert(edytowanyEvent);
        }
    });
    $(".addedItemEvents").selectable({
        stop: function() {
            if($( "li.ui-selected",this ).size()>1){
                $( "li.ui-selected",this  ).removeClass("ui-selected");
            }
            else{
                var tempik=$( "li.ui-selected",this ).first();
                edytowanyItem=this.getAttribute("data-idek");
                edytowanyObject=Items[this.getAttribute("data-idek")];
                edytowanyEvent=tempik.attr("data-event"); 
                edytowanyEventObject=getEvent(edytowanyObject, edytowanyEvent);
                $("#editItemEvent").dialog("open"); 
            } 
        }
    });
}

function addQuestEventsFunctions() {
    $("#questsList ol li").each(function() {
        if(!hasEvent(Quests[$(this).parent().attr("data-idek")],$(this).attr("data-event"))) {
            $(this).remove();
        }
    });
    $(".addQuestEvent").click(function(){
        edytowanyQuest=this.getAttribute("data-idek");
        edytowanyObject=Quests[this.getAttribute("data-idek")];
        $("#questEvents li").each(function() {
            if(hasEvent(edytowanyObject,$(this).attr("data-event"))) {
                $(this).css("display", "none");
            }
            else{
                $(this).css("display", "block");
            }
        })
        $("#addQuestEvent").dialog("open");
    });    

    $("#questEvents").selectable({
        stop: function() {
            if($( "li.ui-selected",this ).size()>1){
                $( "li.ui-selected",this  ).removeClass("ui-selected");
            }
            else{
                var tempik=$( "li.ui-selected",this ).first();
                edytowanyEvent=tempik.attr("data-event"); 
            } 
        //alert(edytowanyEvent);
        }
    });
    $(".addedQuestEvents").selectable({
        stop: function() {
            if($( "li.ui-selected",this ).size()>1){
                $( "li.ui-selected",this  ).removeClass("ui-selected");
            }
            else{
                var tempik=$( "li.ui-selected",this ).first();
                edytowanyQuest=this.getAttribute("data-idek");
                edytowanyObject=Quests[this.getAttribute("data-idek")];
                edytowanyEvent=tempik.attr("data-event"); 
                edytowanyEventObject=getEvent(edytowanyObject, edytowanyEvent);
                $("#editQuestEvent").dialog("open"); 
            } 
        }
    });
}

function addMediaEventsFunctions() {
    $("#mediasList ol li").each(function() {
        if(!hasEvent(Medias[$(this).parent().attr("data-idek")],$(this).attr("data-event"))) {
            $(this).remove();
        }
    });
    $(".addMediaEvent").click(function(){
        edytowaneMedia=this.getAttribute("data-idek");
        edytowanyObject=Medias[this.getAttribute("data-idek")];
        $("#mediaEvents li").each(function() {
            if(hasEvent(edytowanyObject,$(this).attr("data-event"))) {
                $(this).css("display", "none");
            }
            else{
                $(this).css("display", "block");
            }
        })
        $("#addMediaEvent").dialog("open");
    });    

    $("#mediaEvents").selectable({
        stop: function() {
            if($( "li.ui-selected",this ).size()>1){
                $( "li.ui-selected",this  ).removeClass("ui-selected");
            }
            else{
                var tempik=$( "li.ui-selected",this ).first();
                edytowanyEvent=tempik.attr("data-event"); 
            } 
        //alert(edytowanyEvent);
        }
    });
    $(".addedMediaEvents").selectable({
        stop: function() {
            if($( "li.ui-selected",this ).size()>1){
                $( "li.ui-selected",this  ).removeClass("ui-selected");
            }
            else{
                var tempik=$( "li.ui-selected",this ).first();
                edytowaneMedia=this.getAttribute("data-idek");
                edytowanyObject=Medias[this.getAttribute("data-idek")];
                edytowanyEvent=tempik.attr("data-event"); 
                edytowanyEventObject=getEvent(edytowanyObject, edytowanyEvent);
                $("#editMediaEvent").dialog("open"); 
            } 
        }
    });
}

function addTimerEventsFunctions() {
    $("#timersList ol li").each(function() {
        if(!hasEvent(Timers[$(this).parent().attr("data-idek")],$(this).attr("data-event"))) {
            $(this).remove();
        }
    });
    $(".addTimerEvent").click(function(){
        edytowanyTimer=this.getAttribute("data-idek");
        edytowanyObject=Timers[this.getAttribute("data-idek")];
        $("#timerEvents li").each(function() {
            if(hasEvent(edytowanyObject,$(this).attr("data-event"))) {
                $(this).css("display", "none");
            }
            else{
                $(this).css("display", "block");
            }
        })
        $("#addTimerEvent").dialog("open");
    });    

    $("#timerEvents").selectable({
        stop: function() {
            if($( "li.ui-selected",this ).size()>1){
                $( "li.ui-selected",this  ).removeClass("ui-selected");
            }
            else{
                var tempik=$( "li.ui-selected",this ).first();
                edytowanyEvent=tempik.attr("data-event"); 
            } 
        //alert(edytowanyEvent);
        }
    });
    $(".addedTimerEvents").selectable({
        stop: function() {
            if($( "li.ui-selected",this ).size()>1){
                $( "li.ui-selected",this  ).removeClass("ui-selected");
            }
            else{
                var tempik=$( "li.ui-selected",this ).first();
                edytowanyTimer=this.getAttribute("data-idek");
                edytowanyObject=Timers[this.getAttribute("data-idek")];
                edytowanyEvent=tempik.attr("data-event"); 
                edytowanyEventObject=getEvent(edytowanyObject, edytowanyEvent);
                $("#editTimerEvent").dialog("open"); 
            } 
        }
    });
}

function addVariableEventsFunctions() {
    $("#variablesList ol li").each(function() {
        if(!hasEvent(Variables[$(this).parent().attr("data-idek")],$(this).attr("data-event"))) {
            $(this).remove();
        }
    });
    $(".addVariableEvent").click(function(){
        edytowanaVariable=this.getAttribute("data-idek");
        edytowanyObject=Variables[this.getAttribute("data-idek")];
        $("#variableEvents li").each(function() {
            if(hasEvent(edytowanyObject,$(this).attr("data-event"))) {
                $(this).css("display", "none");
            }
            else{
                $(this).css("display", "block");
            }
        })
        $("#addVariableEvent").dialog("open");
    });    

    $("#variableEvents").selectable({
        stop: function() {
            if($( "li.ui-selected",this ).size()>1){
                $( "li.ui-selected",this  ).removeClass("ui-selected");
            }
            else{
                var tempik=$( "li.ui-selected",this ).first();
                edytowanyEvent=tempik.attr("data-event"); 
            } 
        //alert(edytowanyEvent);
        }
    });
    $(".addedVariableEvents").selectable({
        stop: function() {
            if($( "li.ui-selected",this ).size()>1){
                $( "li.ui-selected",this  ).removeClass("ui-selected");
            }
            else{
                var tempik=$( "li.ui-selected",this ).first();
                edytowanaVariable=this.getAttribute("data-idek");
                edytowanyObject=Variables[this.getAttribute("data-idek")];
                edytowanyEvent=tempik.attr("data-event"); 
                edytowanyEventObject=getEvent(edytowanyObject, edytowanyEvent);
                $("#editVariableEvent").dialog("open"); 
            } 
        }
    });
}

function addScenarioEventsFunctions() {
    $("#scenariosList ol li").each(function() {
        if(!hasEvent(scenario,$(this).attr("data-event"))) {
            $(this).remove();
        }
    });
    $(".addScenarioEvent").click(function(){
        //edytowanaVariable=this.getAttribute("data-idek");
        edytowanyObject=scenario;
        $("#scenarioEvents li").each(function() {
            if(hasEvent(edytowanyObject,$(this).attr("data-event"))) {
                $(this).css("display", "none");
            }
            else{
                $(this).css("display", "block");
            }
        })
        $("#addScenarioEvent").dialog("open");
    });    

    $("#scenarioEvents").selectable({
        stop: function() {
            if($( "li.ui-selected",this ).size()>1){
                $( "li.ui-selected",this  ).removeClass("ui-selected");
            }
            else{
                var tempik=$( "li.ui-selected",this ).first();
                edytowanyEvent=tempik.attr("data-event"); 
            } 
        //alert(edytowanyEvent);
        }
    });
    $(".addedScenarioEvents").selectable({
        stop: function() {
            if($( "li.ui-selected",this ).size()>1){
                $( "li.ui-selected",this  ).removeClass("ui-selected");
            }
            else{
                var tempik=$( "li.ui-selected",this ).first();
                //edytowanaVariable=this.getAttribute("data-idek");
                edytowanyObject=scenario;
                edytowanyEvent=tempik.attr("data-event"); 
                edytowanyEventObject=getEvent(edytowanyObject, edytowanyEvent);
                $("#editScenarioEvent").dialog("open"); 
            } 
        }
    });
}

function readyZoneEventsFunctions() {
    $("#addZoneEvent").dialog({
        autoOpen: false,
        modal: true,
        width: $(window).width()*0.4, 
        title: "Wybierz zdarzenie",
        buttons: {
            "Edytuj" : function () {
                $(this).dialog("close");
                edytowanyEventObject=null;
                $("#editZoneEvent").dialog("open");
            }
        }
    });
    $("#editZoneEvent").dialog({
        autoOpen: false,
        modal: true,
        title: "Edytuj zdarzenie",
        width: $(window).width()*0.8, 
        open: function(event, ui) {
            if(edytowanyEventObject==null) $("#editZoneEvent").html(stringMainBlock());
            else {
                //alert(edytowanyEvent);
                rozpiszEvent($("#editZoneEvent"), edytowanyEventObject);
            }
            addButtonFunctions();
            
        },
        close: function() {
            showZoneList();
        },
        buttons: {
            "Zatwierdź" : function () {
                eventJestPoprawny=true;
                if(!hasEvent(edytowanyObject, edytowanyEvent)) {
                    newEvent=new Event(edytowanyEvent);
                    newEvent.actions=deeper($("#editZoneEvent > .actionBlock"));
                    if(eventJestPoprawny==true) edytowanyObject.events.push(newEvent);
                }
                else {
                    newEvent=getEvent(edytowanyObject, edytowanyEvent);
                    if(eventJestPoprawny==true) newEvent.actions=deeper($("#editZoneEvent > .actionBlock"));
                }                   
                if(eventJestPoprawny) {
                    $(this).dialog("close");
                    generate_xml();
                }
                else {
                    alert("Zdarzenie jest niepoprawne.");
                }
            }
        }
    });

}

function readyCharacterEventsFunctions() {
    $("#addCharacterEvent").dialog({
        autoOpen: false,
        modal: true,
        width: $(window).width()*0.4, 
        title: "Wybierz zdarzenie",
        buttons: {
            "Edytuj" : function () {
                $(this).dialog("close");
                edytowanyEventObject=null;
                $("#editCharacterEvent").dialog("open");
            }
        }
    });
    $("#editCharacterEvent").dialog({
        autoOpen: false,
        modal: true,
        title: "Edytuj zdarzenie",
        width: $(window).width()*0.8, 
        open: function(event, ui) {
            if(edytowanyEventObject==null) $("#editCharacterEvent").html(stringMainBlock());
            else {
                //alert(edytowanyEvent);
                rozpiszEvent($("#editCharacterEvent"), edytowanyEventObject);
            }
            addButtonFunctions();
            
        },
        close: function() {
            showCharacterList();
        },
        buttons: {
            "Zatwierdź" : function () {
                eventJestPoprawny=true;
                if(!hasEvent(edytowanyObject, edytowanyEvent)) {
                    newEvent=new Event(edytowanyEvent);
                    newEvent.actions=deeper($("#editZoneEvent > .actionBlock"));
                    if(eventJestPoprawny==true) edytowanyObject.events.push(newEvent);
                }
                else {
                    newEvent=getEvent(edytowanyObject, edytowanyEvent);
                    if(eventJestPoprawny==true) newEvent.actions=deeper($("#editCharacterEvent > .actionBlock"));
                }                   
                if(eventJestPoprawny) {
                    $(this).dialog("close");
                    generate_xml();
                }
                else {
                    alert("Zdarzenie jest niepoprawne.");
                }
            }
        }
    });

    
}

function readyItemEventsFunctions() {
    $("#addItemEvent").dialog({
        autoOpen: false,
        modal: true,
        width: $(window).width()*0.4, 
        title: "Wybierz zdarzenie",
        buttons: {
            "Edytuj" : function () {
                $(this).dialog("close");
                edytowanyEventObject=null;
                $("#editItemEvent").dialog("open");
            }
        }
    });
    $("#editItemEvent").dialog({
        autoOpen: false,
        modal: true,
        title: "Edytuj zdarzenie",
        width: $(window).width()*0.8, 
        open: function(event, ui) {
            if(edytowanyEventObject==null) $("#editItemEvent").html(stringMainBlock());
            else {
                //alert(edytowanyEvent);
                rozpiszEvent($("#editItemEvent"), edytowanyEventObject);
            }
            addButtonFunctions();
            
        },
        close: function() {
            showItemList();
        },
        buttons: {
            "Zatwierdź" : function () {
                eventJestPoprawny=true;
                if(!hasEvent(edytowanyObject, edytowanyEvent)) {
                    newEvent=new Event(edytowanyEvent);
                    newEvent.actions=deeper($("#editItemEvent > .actionBlock"));
                    if(eventJestPoprawny==true) edytowanyObject.events.push(newEvent);
                }
                else {
                    newEvent=getEvent(edytowanyObject, edytowanyEvent);
                    if(eventJestPoprawny==true) newEvent.actions=deeper($("#editItemEvent > .actionBlock"));
                }                   
                if(eventJestPoprawny) {
                    $(this).dialog("close");
                    generate_xml();
                }
                else {
                    alert("Zdarzenie jest niepoprawne.");
                }
            }
        }
    });

}

function readyQuestEventsFunctions() {
    $("#addQuestEvent").dialog({
        autoOpen: false,
        modal: true,
        width: $(window).width()*0.4, 
        title: "Wybierz zdarzenie",
        buttons: {
            "Edytuj" : function () {
                $(this).dialog("close");
                edytowanyEventObject=null;
                $("#editQuestEvent").dialog("open");
            }
        }
    });
    $("#editQuestEvent").dialog({
        autoOpen: false,
        modal: true,
        title: "Edytuj zdarzenie",
        width: $(window).width()*0.8, 
        open: function(event, ui) {
            if(edytowanyEventObject==null) $("#editQuestEvent").html(stringMainBlock());
            else {
                //alert(edytowanyEvent);
                rozpiszEvent($("#editQuestEvent"), edytowanyEventObject);
            }
            addButtonFunctions();
            
        },
        close: function() {
            showQuestList();
        },
        buttons: {
            "Zatwierdź" : function () {
                eventJestPoprawny=true;
                if(!hasEvent(edytowanyObject, edytowanyEvent)) {
                    newEvent=new Event(edytowanyEvent);
                    newEvent.actions=deeper($("#editQuestEvent > .actionBlock"));
                    if(eventJestPoprawny==true) edytowanyObject.events.push(newEvent);
                }
                else {
                    newEvent=getEvent(edytowanyObject, edytowanyEvent);
                    if(eventJestPoprawny==true) newEvent.actions=deeper($("#editQuestEvent > .actionBlock"));
                }                   
                if(eventJestPoprawny) {
                    $(this).dialog("close");
                    generate_xml();
                }
                else {
                    alert("Zdarzenie jest niepoprawne.");
                }
            }
        }
    });

}

function readyMediaEventsFunctions() {
    $("#addMediaEvent").dialog({
        autoOpen: false,
        modal: true,
        width: $(window).width()*0.4, 
        title: "Wybierz zdarzenie",
        buttons: {
            "Edytuj" : function () {
                $(this).dialog("close");
                edytowanyEventObject=null;
                $("#editMediaEvent").dialog("open");
            }
        }
    });
    $("#editMediaEvent").dialog({
        autoOpen: false,
        modal: true,
        title: "Edytuj zdarzenie",
        width: $(window).width()*0.8, 
        open: function(event, ui) {
            if(edytowanyEventObject==null) $("#editMediaEvent").html(stringMainBlock());
            else {
                //alert(edytowanyEvent);
                rozpiszEvent($("#editMediaEvent"), edytowanyEventObject);
            }
            addButtonFunctions();
            
        },
        close: function() {
            showMediaList();
        },
        buttons: {
            "Zatwierdź" : function () {
                eventJestPoprawny=true;
                if(!hasEvent(edytowanyObject, edytowanyEvent)) {
                    newEvent=new Event(edytowanyEvent);
                    newEvent.actions=deeper($("#editMediaEvent > .actionBlock"));
                    if(eventJestPoprawny==true) edytowanyObject.events.push(newEvent);
                }
                else {
                    newEvent=getEvent(edytowanyObject, edytowanyEvent);
                    if(eventJestPoprawny==true) newEvent.actions=deeper($("#editMediaEvent > .actionBlock"));
                }                   
                if(eventJestPoprawny) {
                    $(this).dialog("close");
                    generate_xml();
                }
                else {
                    alert("Zdarzenie jest niepoprawne.");
                }
            }
        }
    });

}

function readyTimerEventsFunctions() {
    $("#addTimerEvent").dialog({
        autoOpen: false,
        modal: true,
        width: $(window).width()*0.4, 
        title: "Wybierz zdarzenie",
        buttons: {
            "Edytuj" : function () {
                $(this).dialog("close");
                edytowanyEventObject=null;
                $("#editTimerEvent").dialog("open");
            }
        }
    });
    $("#editTimerEvent").dialog({
        autoOpen: false,
        modal: true,
        title: "Edytuj zdarzenie",
        width: $(window).width()*0.8, 
        open: function(event, ui) {
            if(edytowanyEventObject==null) $("#editTimerEvent").html(stringMainBlock());
            else {
                //alert(edytowanyEvent);
                rozpiszEvent($("#editTimerEvent"), edytowanyEventObject);
            }
            addButtonFunctions();
            
        },
        close: function() {
            showTimerList();
        },
        buttons: {
            "Zatwierdź" : function () {
                eventJestPoprawny=true;
                if(!hasEvent(edytowanyObject, edytowanyEvent)) {
                    newEvent=new Event(edytowanyEvent);
                    newEvent.actions=deeper($("#editTimerEvent > .actionBlock"));
                    if(eventJestPoprawny==true) edytowanyObject.events.push(newEvent);
                }
                else {
                    newEvent=getEvent(edytowanyObject, edytowanyEvent);
                    if(eventJestPoprawny==true) newEvent.actions=deeper($("#editTimerEvent > .actionBlock"));
                }                   
                if(eventJestPoprawny) {
                    $(this).dialog("close");
                    generate_xml();
                }
                else {
                    alert("Zdarzenie jest niepoprawne.");
                }
            }
        }
    });

}

function readyVariableEventsFunctions() {
    $("#addVariableEvent").dialog({
        autoOpen: false,
        modal: true,
        width: $(window).width()*0.4, 
        title: "Wybierz zdarzenie",
        buttons: {
            "Edytuj" : function () {
                $(this).dialog("close");
                edytowanyEventObject=null;
                $("#editVariableEvent").dialog("open");
            }
        }
    });
    $("#editVariableEvent").dialog({
        autoOpen: false,
        modal: true,
        title: "Edytuj zdarzenie",
        width: $(window).width()*0.8, 
        open: function(event, ui) {
            if(edytowanyEventObject==null) $("#editVariableEvent").html(stringMainBlock());
            else {
                //alert(edytowanyEvent);
                rozpiszEvent($("#editVariableEvent"), edytowanyEventObject);
            }
            addButtonFunctions();
            
        },
        close: function() {
            showVariableList();
        },
        buttons: {
            "Zatwierdź" : function () {
                eventJestPoprawny=true;
                if(!hasEvent(edytowanyObject, edytowanyEvent)) {
                    newEvent=new Event(edytowanyEvent);
                    newEvent.actions=deeper($("#editVariableEvent > .actionBlock"));
                    if(eventJestPoprawny==true) edytowanyObject.events.push(newEvent);
                }
                else {
                    newEvent=getEvent(edytowanyObject, edytowanyEvent);
                    if(eventJestPoprawny==true) newEvent.actions=deeper($("#editVariableEvent > .actionBlock"));
                }                   
                if(eventJestPoprawny) {
                    $(this).dialog("close");
                    generate_xml();
                }
                else {
                    alert("Zdarzenie jest niepoprawne.");
                }
            }
        }
    });

}

function readyScenarioEventsFunctions() {
    $("#addScenarioEvent").dialog({
        autoOpen: false,
        modal: true,
        width: $(window).width()*0.4, 
        title: "Wybierz zdarzenie",
        buttons: {
            "Edytuj" : function () {
                $(this).dialog("close");
                edytowanyEventObject=null;
                $("#editScenarioEvent").dialog("open");
            }
        }
    });
    $("#editScenarioEvent").dialog({
        autoOpen: false,
        modal: true,
        title: "Edytuj zdarzenie",
        width: $(window).width()*0.8, 
        open: function(event, ui) {
            if(edytowanyEventObject==null) $("#editScenarioEvent").html(stringMainBlock());
            else {
                //alert(edytowanyEvent);
                rozpiszEvent($("#editScenarioEvent"), edytowanyEventObject);
            }
            addButtonFunctions();
            
        },
        close: function() {
            showScenarioList();
        },
        buttons: {
            "Zatwierdź" : function () {
                eventJestPoprawny=true;
                if(!hasEvent(edytowanyObject, edytowanyEvent)) {
                    newEvent=new Event(edytowanyEvent);
                    newEvent.actions=deeper($("#editScenarioEvent > .actionBlock"));
                    if(eventJestPoprawny==true) edytowanyObject.events.push(newEvent);
                }
                else {
                    newEvent=getEvent(edytowanyObject, edytowanyEvent);
                    if(eventJestPoprawny==true) newEvent.actions=deeper($("#editScenarioEvent > .actionBlock"));
                }                   
                if(eventJestPoprawny) {
                    $(this).dialog("close");
                    generate_xml();
                }
                else {
                    alert("Zdarzenie jest niepoprawne.");
                }
            }
        }
    });

}

function readyEventsFunctions() {
    readyZoneEventsFunctions();  
    readyCharacterEventsFunctions();
    readyItemEventsFunctions();
    readyQuestEventsFunctions();
    readyMediaEventsFunctions();
    readyTimerEventsFunctions();
    readyVariableEventsFunctions();
    readyScenarioEventsFunctions();
}
