var Actions = new Array();
var Conditions = new Array();
var Items = new Array();
var Zones = new Array();
var Characters = new Array();
var Quests = new Array();
var Medias = new Array();
var Timers = new Array();
var Integers = new Array();
var Chars = new Array();
var Strings = new Array();
var Booleans = new Array();
var Doubles = new Array();
var Variables = new Array();
var GUIList = new Array();
var MathList = new Array();
var StringList = new Array();
var GroupList = new Array();

var edytowanaZona=-1;
var edytowanyItem=-1;
var edytowanyCharacter=-1;
var edytowanyQuest=-1;
var edytowaneMedia=-1;
var edytowanyTimer=-1;
var edytowanaVariable=-1;

var oryginalnyPolygon;
var edytowanyPolygon;
var kopiaKopiiPolygonu;

var edytowanyAction;
var edytowanyEvent; //jego nazwa - onEnter
var edytowanyEventObject; // object eventa

var globalId=0;
var lastItemId=0;
var lastZoneId=0;
var lastCharacterId=0;
var lastQuestId=0;
var lastMediaId=0;
var lastTimerId=0;
var lastVariableId=0;
var lastBlockId=0;

var newEvent;
//var scenarioJSON={"private":false,"name":$("#scenarioNameText").text(),"description":"Uzyskaj wszystkie wpisy na karcie obiegowej","tags":"","splash":22,"assets":[],"xml":"<scenario \/>","password":null};


function Scenario() {
    this.events=new Array();
}
var scenario=new Scenario();

function Event(name) {
    this.name=name;
    this.actions="";
}

function Action() {
    this.type="action";
    this.id=-1;
    this.action="";
}

function Condition() {
    this.type="condition";
    this.id=-1;
    this.condition="";
}

function ForBlock(){
    this.type="for";
    this.cond="";
    this.toDo="";
}

function WhileBlock(){
    this.type="while";
    this.cond="";
    this.toDo="";
}

function IfBlock(){
    this.type="if";
    this.cond="";
    this.doIf="";
    this.doElse="";
}

function ActionBlock()
{
    this.type="block";
    this.actions=new Array();
}

function newItemId()
{
    return "item"+(++lastItemId);
}
function newZoneId()
{
    return "zone"+(++lastZoneId);
}
function newCharacterId()
{
    return "character"+(++lastCharacterId);
}
function newQuestId()
{
    return "quest"+(++lastQuestId);
}
function newMediaId()
{
    return "media"+(++lastMediaId);
}
function newVariableId()
{
    return "variable"+(++lastVariableId);
}
function newTimerId()
{
    return "timer"+(++lastTimerId);
}
function newBlockId()
{
    return (++lastBlockId);
}

function newId() {
    return (++globalId);
}

function Variable(idname, type, value) {
    this.id=newId();
    this.idname=idname;
    this.type=type;
    this.value=value;
    this.events=new Array();
    if(type=="INTEGER"){
        Integers.push(this)
    }
    else if(type=="STRING"){
        Strings.push(this)
    }
    else if(type=="DOUBLE"){
        Doubles.push(this)
    }
    else if(type=="BOOLEAN"){
        Booleans.push(this)
    }
    else if(type=="CHAR"){
        Chars.push(this)
    }
}

function Item2(idname, name)
{
    this.id=newItemId();
    this.idname=idname;
    this.name=name;
    this.events=new Array();
}

function Item(idname, name, description, zone, visible, state, owned, icon) {
    this.id=newId();
    this.idname=idname;
    this.name=name;
    this.description=description;
    this.zone=zone;
    this.visible=visible;
    this.state=state;
    this.owned=owned;
    this.icon=icon;
    this.events=new Array();
}

function Zone(idname, name, description, state, visible, points, owned, icon) {
    this.id=newId();
    this.idname=idname;
    this.name=name;
    this.description=description;
    this.state=state;
    this.visible=visible;
    this.poly=points;
    this.owned=owned;
    this.icon=icon;
    this.events=new Array();
}

function Character(idname, name, sex, description, zone, state, visible, owned, icon) {
    this.id=newId();
    this.idname=idname;
    this.name=name;
    this.sex=sex;
    this.description=description;
    this.zone=zone;
    this.state=state;
    this.visible=visible;
    this.owned=owned;
    this.icon=icon;
    this.events=new Array();
}

function Quest(idname, name, description, state,visible, icon) {
    this.id=newId();
    this.idname=idname;
    this.name=name;
    this.description=description;
    this.state=state;
    this.visible=visible;
    this.icon=icon;
    this.events=new Array();
}

function Media(idname, pathid,pathname, type) {
    this.id=newId();
    this.idname=idname;
    this.pathid=pathid;
    this.pathname=pathname;
    this.type=type;
    this.events=new Array();
}
function Timer(idname, interval) {
    this.id=newId();
    this.idname=idname;
    this.interval=interval;
    this.events=new Array();
}

function GroupDefinition(type, name, values, fields) {    
    this.name=name;
    this.type=type;
    this.values=values;   
    this.fields=fields;      
}

function FieldDefinition(type, field, visibleName) {    
    this.field=field; 
    this.visibleName=visibleName;
    this.type=type;     
}

function CallFunction(name, target, type, arguments) {    
    this.name=name; 
    this.target=target;
    this.type=type;     
    this.arguments=arguments;
}

function Argument(type, value){
    this.type=type;     
    this.value=value;     
}
function SimpleCondition(type, eastSideType,eastDataType, eastSideValue, 
    operator, westSideType, westDataType,westSideValue){
    this.lol="simple";
    this.type=type;
    this.eastSideType=eastSideType;     
    this.eastDataType=eastDataType;     
    this.eastSideValue=eastSideValue;   
    this.operator=operator;
    this.westSideType=westSideType;
    this.westDataType=westDataType;
    this.westSideValue=westSideValue;
}
function ComplexCondition(type, conditions){
    this.lol="complex";
    this.type=type;
    this.conditions=conditions;
}
