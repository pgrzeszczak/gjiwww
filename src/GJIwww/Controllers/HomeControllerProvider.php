<?php
namespace GJIwww\Controllers;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\Request;
use GJIwww\Entities\User;
use GJIwww\Forms\RegisterType;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Form\FormError;

class HomeControllerProvider implements ControllerProviderInterface
{
    public function connect(Application $app)
    {
        $controllers = $app['controllers_factory'];
        
        $controllers->get('/', function(Application $app) {
            return $app['twig']->render('User/home.html.twig');
        })->bind('homepage');
        
        $controllers->get('/zoneventure', function(Application $app) {
            return $app['twig']->render('User/home.html.twig');
        })->bind('zoneventure');
        
        $app->get('/login', function(Request $request) use ($app) {
            if ($app['security']->isGranted('ROLE_USER')) {
                return $app->redirect($app['url_generator']->generate('homepage'));
            }
            return $app['twig']->render('User/login.html.twig', array(
                'error'         => $app['security.last_error']($request),
                'last_username' => $app['session']->get('_security.last_username'),
            ));
        })->bind('login');        
        
        
        $controllers->get('/changeLanguage/{language}', function(Application $app, Request $request, $language) {
            $url = $request->server->get('HTTP_REFERER');
            if ($url == null) {
              $url = $app['url_generator']->generate('homepage');
            }
            $app["session"]->set('locale', $language);
            return $app->redirect($url);
        })->bind('changeLanguage');
        
        
        
        $controllers->match('/register', function (Request $request) use ($app) {
            $user = new User();
            $form = $app['form.factory']->createBuilder(new RegisterType(), $user)->getForm();
            if ('POST' == $request->getMethod()) {
                $form->bind($request);
                if ($form->isValid()) {
                    $user = $form->getData();
                    
                    if (!$app['db.orm.em']->getRepository('GJIwww\Entities\User')->userExists($user->getUsername())) {

                        // find the encoder for a UserInterface instance
                        $encoder = $app['security.encoder_factory']->getEncoder($user);

                        // compute the encoded password for foo
                        $password = $encoder->encodePassword($user->getPlainPassword(), $user->getSalt());
                        $user->setPassword($password);
                        $user->setConfirmationToken('');
                        $user->setEnabled(true);
                        $app['db.orm.em']->persist($user);
                        $app['db.orm.em']->flush();

                        // Create a new token
                        $token = new UsernamePasswordToken($user, $user->getPassword(), "public", $user->getRoles());

                        // Retrieve the security context and set the token
                        $context = $app['security'];
                        $context->setToken($token);
                        
                        $app['session']->setFlash('notify', 'notify.register.complete');
                        return $app->redirect($app['url_generator']->generate('homepage'));
                    } else {
                        $error = new FormError('form.register.errors.user_exists');
                        $form->addError($error);
                    }
                }
            }

            // display the form
            return $app['twig']->render('User/register.html.twig', array('form' => $form->createView()));
        })->bind('register');

        return $controllers;
    }
}
