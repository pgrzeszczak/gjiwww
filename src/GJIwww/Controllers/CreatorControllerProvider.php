<?php
namespace GJIwww\Controllers;

use Silex\Application;
use Silex\ControllerProviderInterface;
use GJIwww\Entities\Scenario;
use Symfony\Component\HttpFoundation\Request;

class CreatorControllerProvider implements ControllerProviderInterface
{
    public function connect(Application $app)
    {
        $controllers = $app['controllers_factory'];

        $controllers->get('/edit/{id}', function (Application $app, $id) {
            if ($id === -1) {
                $jsonObject = new \StdClass();
                $json = json_encode($jsonObject);
            } else {
                $jsonObject = new \StdClass();
                $repo = $app['db.orm.em']->getRepository('GJIwww\Entities\Scenario');
                $scenario = $repo->find($id);
                $user = $app['security']->getToken()->getUser();
                if ($scenario === null) {
                    $app->abort(404);
                }
                if ($scenario->getAuthor()->getId() !== $user->getId()) {
                    $app->abort(403);
                }
                $jsonObject->private = $scenario->isPrivate();
                $jsonObject->name = $scenario->getName();
                $jsonObject->description = $scenario->getDescription();
                $jsonObject->tags = $scenario->getTags();
                $splash = null;
                if ($scenario->getSplashAsset() !== null) {
                    $splash = $scenario->getSplashAsset()->getId();
                }
                $jsonObject->splash = $splash;
                $jsonObject->assets = $scenario->getArrayOfAssetIds();
                $jsonObject->xml = $scenario->getContent();
                $jsonObject->password = $scenario->getPassword();
                $json = json_encode($jsonObject);
            }
            return $app['twig']->render('User/creator.html.twig', array('scenarioJSONDef' => $json, 'scenarioId' => $id));
        })->bind('creator_edit')
          ->value('id', -1);
        
        $controllers->post('/save/{id}', function (Application $app, Request $request, $id) {
            $repo = $app['db.orm.em']->getRepository('GJIwww\Entities\Scenario');
            $scenario = $repo->find($id);
            $user = $app['security']->getToken()->getUser();
            if ($scenario === null) {
                $app->abort(404);
            }
            if ($scenario->getAuthor()->getId() !== $user->getId()) {
                $app->abort(403);
            }
            $data = $request->request->all();
            $scenario->setPrivate($data['private'] === 'true');
            $scenario->setName($data['name']);
            $scenario->setDescription($data['description']);
            $scenario->setTags($data['tags']);
            $assetRepo = $app['db.orm.em']->getRepository('GJIwww\Entities\Asset');
            if ($data['splash'] !== '') {
                $splash = $assetRepo->find($data['splash']);
                if ($splash !== null) {
                    $scenario->setSplashAsset($splash);
                }
            }
            $scenario->setPassword($data['password']);
            $scenario->setContent($data['xml']);
            $assets = $data['assets'];
            $assetsList = new \Doctrine\Common\Collections\ArrayCollection();
            
            foreach ($assets as $assetId) {
                $asset = $assetRepo->find($assetId);
                if ($asset !== null) {
                    $assetsList->add($asset);
                }
            }
            $scenario->setUsedAssets($assetsList);
            $app['db.orm.em']->merge($scenario);
            $app['db.orm.em']->flush();
            return 'OK';
        })->bind('creator_save');
        
        $controllers->get('/new', function (Application $app) {
            $user = $app['security']->getToken()->getUser();
            $scenario = new Scenario();
            $scenario->setAuthor($user);
            $scenario->setCreated(new \DateTime('now'));
            $scenario->setName('New scenario');
            $scenario->setPrivate(true);
            $app['db.orm.em']->persist($scenario);
            $app['db.orm.em']->flush();
            return $app->redirect($app['url_generator']->generate('creator_edit', array('id' => $scenario->getId())));
        })->bind('create_new_scenario');
        
        $controllers->get('/delete/{id}', function (Application $app, $id) {
            $repo = $app['db.orm.em']->getRepository('GJIwww\Entities\Scenario');
            $scenario = $repo->find($id);
            $user = $app['security']->getToken()->getUser();
            if ($scenario === null) {
                $app->abort(404);
            }
            if ($scenario->getAuthor()->getId() !== $user->getId()) {
                $app->abort(403);
            }
            $app['db.orm.em']->remove($scenario);
            $app['db.orm.em']->flush();
            return $app->redirect($app['url_generator']->generate('scenariolist'));
        })->bind('creator_delete');

        return $controllers;
    }
}
