<?php

use GJIwww\Providers\UserProvider;

require_once __DIR__.'/../app/config.php';

$app = new Silex\Application();

registerParameters($app); //add some config parameters to app

$app['debug'] = $app['params.debug_mode'];
//register providers
$app->register(new Silex\Provider\SessionServiceProvider(), array(
    'httponly' => true,
    'cookie_lifetime' => 0,
));
$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => $app['params.views_path'],
    'twig.form.templates'   => array("Form/form_div_layout.html.twig"),
));
$app->register(new Silex\Provider\TranslationServiceProvider(), array(
    'locale_fallback' => 'pl',
    'locale' => 'pl',
    'translator.domains' => array(),
));
$app->register(new Silex\Provider\UrlGeneratorServiceProvider());
$app->register(new Silex\Provider\DoctrineServiceProvider(), array(
    'db.options' => array(
        'driver'     => 'pdo_pgsql',
        'dbname'     => 'gjiwww',
        'host'       => 'localhost',
        'user'       => 'postgres',
        'password'   => 'postgres',
    ),
));
$app->register(new Nutwerk\Provider\DoctrineORMServiceProvider, array(
    'db.orm.proxies_dir'           => __DIR__ . '/../cache/doctrine/proxy',
    'db.orm.proxies_namespace'     => 'DoctrineProxy',
    'db.orm.auto_generate_proxies' => true,
    'db.orm.entities'              => array(array(
        'type'      => 'annotation',       // entity definition 
        'path'      => $app['params.entity_path'],   // path to your entity classes
        'namespace' => 'GJIwww\Entities', // your classes namespace
    )),
));
$app->register(new SilexAssetic\AsseticExtension(), array(
    'assetic.class_path' => __DIR__.'/vendor/assetic/src',
    'assetic.path_to_web' => $app['params.assets_path'],
    'assetic.options' => array(
        'debug' => $app['params.debug_mode'],
        'twig_support' => true
    ),
    'assetic.filters' => $app->protect(function($fm) {
        $fm->set('yui_css', new Assetic\Filter\Yui\CssCompressorFilter(
            '/usr/share/yui-compressor/yui-compressor.jar'
        ));
        $fm->set('yui_js', new Assetic\Filter\Yui\JsCompressorFilter(
            '/usr/share/yui-compressor/yui-compressor.jar'
        ));
    }),
    'assetic.assets' => $app->protect(function($am, $fm) use ($app) {
        $iterator = new DirectoryIterator($app['params.assets_src_path'] . '/css/cssimages');
        foreach ($iterator as $fileinfo) {
            if ($fileinfo->isFile()) {
                $am->set('cssimages_'.str_replace('.', '_', $fileinfo->getFilename()), new Assetic\Asset\FileAsset(
                        $app['params.assets_src_path'] . '/css/cssimages/' . $fileinfo->getFilename()
                ));
                $am->get('cssimages_'.str_replace('.', '_', $fileinfo->getFilename()))
                   ->setTargetPath('css/cssimages/' . $fileinfo->getFilename());
            }
        }
    })
));
$app->register(new \Silex\Provider\FormServiceProvider());
$app->register(new Silex\Provider\ValidatorServiceProvider());
$app->register(new Silex\Provider\SecurityServiceProvider());
$app['security.firewalls'] = array(
    'for_mobile' => array(
        'anonymous' => true,
        'pattern' => '^/mobile/.*$',
        'http' => true,
        'users' => $app->share(function () use ($app) {
            return new UserProvider($app['db.orm.em']);
        }),
    ),
    'secured' => array(
        'anonymous' => true,
        'pattern' => '^.*$',
        'form' => array('login_path' => '/login', 'check_path' => '/login_check'),
        'logout' => array('logout_path' => '/logout'),
        'users' => $app->share(function () use ($app) {
            return new UserProvider($app['db.orm.em']);
        }),
    ),
);
$app['security.access_rules'] = array(
    array('^/creator', 'ROLE_USER'),
    array('^/scenariolist', 'ROLE_USER'),
    array('^.*$', 'IS_AUTHENTICATED_ANONYMOUSLY'),
);
    
$app['translator'] = $app->share($app->extend('translator', function($translator, $app) {
    $translator->addLoader('yaml', new Symfony\Component\Translation\Loader\YamlFileLoader());

    $translator->addResource('yaml', $app['params.trans_path'] . '/en.yml', 'en');
    $translator->addResource('yaml', $app['params.trans_path'] . '/pl.yml', 'pl');
    $translator->addResource('yaml', $app['params.trans_path'] . '/validators/en.yml', 'en', 'validators');
    $translator->addResource('yaml', $app['params.trans_path'] . '/validators/pl.yml', 'pl', 'validators');

    return $translator;
}));

return $app;
