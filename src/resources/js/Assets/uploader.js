$(document).ready(function() {
    var assetUploadOptions = {
        beforeSubmit: function(formData, jqForm, options) {
            $('#frmUploadAssetSubmit').spinner();
        }, // pre-submit callback
        success: function(responseText, statusText, xhr, $form) {
            $.ajax({
              url: $('#linkToAssetsWidget').attr('href')
            }).done(function(data) {
              $('#assetsWidget').replaceWith(data);
              createAssetsList();
            });
            $('#frmUploadAssetSubmit').spinner('remove');
        }, // post-submit callback
        error: function() {
            $('#frmUploadAssetSubmit').spinner('remove');
        }
    };

    $(document).on("submit", "#frmUploadAsset", function(event){
        $(this).ajaxSubmit(assetUploadOptions);
        return false;
    });
});