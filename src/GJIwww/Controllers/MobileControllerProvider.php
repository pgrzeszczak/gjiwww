<?php
namespace GJIwww\Controllers;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\Response;

class MobileControllerProvider implements ControllerProviderInterface
{
    public function connect(Application $app)
    {
        $controllers = $app['controllers_factory'];
        
        $controllers->get('/publiclist/{start}/{limit}', function(Application $app, $start, $limit) {
//            if ($app['security']->isGranted('IS_AUTHENTICATED_FULLY')) {
//                return 'ZALOGOWANY!';
//            } else {
//                return 'NIEZALOGOWANY!';
//            }
            $scenarios = $app['db.orm.em']->getRepository('GJIwww\Entities\Scenario')->getPublicScenarios($start, $limit);
            $xml = $this->convertScenariosArrayToXMLInfo($scenarios);
            $headers = array('Content-Type' => 'text/xml');
            return new Response($xml, 200, $headers);
        })->bind('mobile_publiclist')
          ->value('start', 0)
          ->value('limit', 10);
        
        $controllers->get('/userscenarios/{start}/{limit}', function(Application $app, $start, $limit) {
            if (!$app['security']->isGranted('IS_AUTHENTICATED_FULLY')) {
                return $app->abort(403);
            }
            $user = $app['security']->getToken()->getUser();
            $scenarios = $app['db.orm.em']->getRepository('GJIwww\Entities\Scenario')->getUserScenarios($user, $start, $limit);
            $xml = $this->convertScenariosArrayToXMLInfo($scenarios);
            $headers = array('Content-Type' => 'text/xml');
            return new Response($xml, 200, $headers);
        })->bind('mobile_userscenrios')
          ->value('start', 0)
          ->value('limit', 10);
        
        $controllers->get('/getsplash/{id}', function (Application $app, $id) {
            $scenario = $app['db.orm.em']->getRepository('GJIwww\Entities\Scenario')->find($id);
            if ($scenario === null) {
                $app->abort(404);
            }            
            if ($scenario->isPrivate()) {
                if (!$app['security']->isGranted('IS_AUTHENTICATED_FULLY')) {
                    $app->abort(403);
                }
                $user = $app['security']->getToken()->getUser();
                if ($scenario->getAuthor()->getId() !== $user->getId()) {
                    $app->abort(403);
                }
            }
            $asset = $scenario->getSplashAsset();
            if ($asset === null) {
                $app->abort(404);
            }
            $length = filesize($asset->getAbsolutePath($app));
            $content = file_get_contents($asset->getAbsolutePath($app));
            $headers = array('Content-Type' => $asset->getMimeType(),
                             'Content-Length' => $length,
                             'Content-Disposition' => 'inline; filename="splash' . $id . '.' . $asset->getFileExtension() . '"');
            return new Response($content, 200, $headers);
        })->bind('mobile_getsplash');
        
        $controllers->get('/getscenario/{id}/{pass}', function (Application $app, $id, $pass) {
            $scenario = $app['db.orm.em']->getRepository('GJIwww\Entities\Scenario')->find($id);
            if ($scenario === null) {
                $app->abort(404);
            }
            if ($scenario->isPrivate()) {
                if (!$app['security']->isGranted('IS_AUTHENTICATED_FULLY')) {
                    $password = $scenario->getPassword();
                    if ($password === null || $password === '' || $password !== $pass) {
                        $app->abort(403);
                    }
                } else {
                    $user = $app['security']->getToken()->getUser();
                    if ($scenario->getAuthor()->getId() !== $user->getId()) {
                        $app->abort(403);
                    }
                }
            }
            $tmpfname = tempnam("/tmp", "scenario");
            $zip = new \ZipArchive();
            $zip->open($tmpfname, \ZIPARCHIVE::CREATE);
            
            $zip->addFromString('scenario.xml', $scenario->getContent());
            
            $splash = $scenario->getSplashAsset();
            if ($splash !== null) {
                $ext = $splash->getFileExtension();
                if ($ext != '') {
                    $ext = '.' . $ext;
                }
                $zip->addFile($splash->getAbsolutePath($app), 'splash' . $ext);
            }
            
            $assets = $scenario->getUsedAssets();
            foreach ($assets as $asset) {
                $zip->addFile($asset->getAbsolutePath($app), $app['params.assets_scenario_path'] . '/' . $asset->getPath());
            }
            
            $zip->close();
            
            $content = file_get_contents($tmpfname);
            $length = filesize($tmpfname);
            unlink($tmpfname);
            $headers = array('Content-Type' => 'application/zip',
                             'Content-Length' => $length,
                             'Content-Disposition' => 'attachment; filename="scenario' . $id . '.zip"');
            return new Response($content, 200, $headers);
        })->bind('mobile_getscenario')
          ->value('pass', null);
        
        return $controllers;
    }
    
    protected function convertScenariosArrayToXMLInfo($scenarios)
    {
        $dd = new \DOMDocument('1.0', 'UTF-8');
        $scenariosElement = $dd->createElement('scenarios');
        foreach ($scenarios as $scenario) {
            $scenariosElement->appendChild($scenario->getXMLInfoNode($dd));
        }
        $dd->appendChild($scenariosElement);
        $dd->formatOutput = true;
        return $dd->saveXML();
    }
}
