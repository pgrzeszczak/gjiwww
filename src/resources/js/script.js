var edytowanyObject="";
var eventJestPoprawny=true;
var stringBlockId;

jQuery.fn.exists = function(){
    return this.length>0;
}

function getMedia(id) {
    for(i=0; i<Medias.length; i++) {
        if(Medias[i].id==id) return Medias[i];
    }
    return null;
}

function getAction(id) {
    for(var i=0; i<Actions.length; i++) {
        if(Actions[i].id==id) return Actions[i].action;
    }
    return null;
}

function hasCondition(id) {
    for(var i=0; i<Conditions.length; i++) {
        if(Conditions[i].id==id) return true;
    }
    return false;
}

function getCondition(id) {
    for(var i=0; i<Conditions.length; i++) {
        if(Conditions[i].id==id) return Conditions[i].condition;
    }
    return null;
}

function rmCondition(id) {
    for(var i=0; i<Conditions.length; i++) {
        if(Conditions[i].id==id) {
            Conditions.splice(i, 1);
            return;
        }
    }

}

function deeper(element) {
    //var result=$(element).attr("class")+"... "+$(element).attr("data-blockId")+"\n";
    var result="";
    var abek=new ActionBlock();
    //var child = $(element).children().first();
    if($(element).attr("data-blockType")=="main") {
        $(element).children().each(function(index) {
            $(this).children(".actionBlock").each(function() {
                //result+=deeper(this);
                abek.actions.push(deeper(this));
                if(abek.actions.length==0) eventJestPoprawny=false;
            });
        });
    }
    else if($(element).attr("data-blockType")=="action") {
        result+="<action id=\""+$(element).attr("data-blockId")+"\"></action>\n"
        abek=new Action();
        abek.action=getAction($(element).attr("data-blockId"));
        if(abek.action==null) eventJestPoprawny=false;
    //alert($(element).attr("data-blockId"));
    }
    else if($(element).attr("data-blockType")=="for") {
        result+="<for id=\""+$(element).attr("data-blockId")+"\">\n";
        var el=$(element).children(".actionBlock").first();
        //result+=deeper(el);
        abek=new ForBlock();
        abek.toDo=deeper(el);
        result+="</for>\n";
    }    
    else if($(element).attr("data-blockType")=="while") {
        result+="<while id=\""+$(element).attr("data-blockId")+"\">\n";
        var el=$(element).children(".actionBlock").first();
        //result+=deeper(el); 
        abek=new WhileBlock();
        abek.toDo=deeper(el);
        result+="</while>\n";
    }        
    else if($(element).attr("data-blockType")=="if") {
        result+="<ifStatement id=\""+$(element).attr("data-blockId")+"\">\n<if>\n"
        var el=$(element).children(".actionBlock").first();
        //result+=deeper(el);
        abek=new IfBlock();
        abek.cond=getCondition($(element).attr("data-blockId"));
        abek.doIf=deeper(el);
        result+="</if>\n<else>\n";
        //result+=deeper($(el).nextAll(".actionBlock").first());
        abek.doElse=deeper($(el).nextAll(".actionBlock").first());
        result+="</else>\n</ifStatement>\n"
    }

    //return result;
    return abek;
}

function stringMainBlock() {
    var result="<div class=\"actionBlock\" data-blockType=\"main\" data-blockId=\""+newBlockId()+"\"><div class=\"actualAction\"></div><button class=\"addActionBlock\">Dodaj kolejną akcję</button></div>"
    return result;
}

function stringBlock() {
    var result="";
    stringBlockId=newBlockId();
    result+="<div class=\"actionBlock aaa\" data-blockType=\"action\" data-blockId=\""+stringBlockId+"\"><div class=\"actualAction\"><button class=\"removeActionBlock\">Usuń</button><button class=\"addActionButton\">Wybierz akcję</button><button class=\"addIfStatement\">If/Else</button><button class=\"addForStatement\">For</button><button class=\"addWhileStatement\">While</button></div></div>"
    return result;
}

function stringSeriousActionBlock(action) {
    var result="";
    var srodeczek="";
    var newId=newBlockId();
    result+="<div class=\"actionBlock aaa\" data-blockType=\"action\" data-blockId=\""+newId+"\"><div class=\"actualAction\"><button class=\"removeActionBlock\">Usuń</button>"+"\t";
    if(action.name=="message") {
        srodeczek="Wyświetl wiadomość o tytule \""+action.arguments[0].value+"\" i treści \""+action.arguments[1].value+"\".";
    }
    else if(action.name=="warning") {
        srodeczek="Wyświetl ostrzeżęnie o tytule \""+action.arguments[0].value+"\" i treści \""+action.arguments[1].value+"\".";
    }
    else {
        srodeczek=action.name;
    }
    result+=srodeczek+"</div></div>";
    
    var newAction=new Action();
    newAction.id=newId;
    newAction.action=action;
    Actions.push(newAction);
    return result;
}

function stringIfBlock() {
    var result="";
    result+="<div class=\"actionBlock ifElseBlock\" data-blockType=\"if\" data-blockId=\""+newBlockId()+"\"><button class=\"removeActionBlock\">Usuń</button><br />"
    result+="WARUNEK: <button class=\"addConditionButton\">Wybierz warunek</button><br />"
    result+="IF:<br />"
    result+=stringMainBlock();
    result+="ELSE:<br >"
    result+=stringMainBlock();
    result+="</div>"
    return result;
}

function stringForBlock() {
    var result="";
    result+="<div class=\"actionBlock forBlock\" data-blockType=\"for\" data-blockId=\""+newBlockId()+"\"><button class=\"removeActionBlock\">Usuń</button><br />"
    result+="OD i = 0 do i = 10:<br />"
    result+=stringMainBlock();
    result+="</div>"
    return result;
}

function stringWhileBlock() {
    var result="";
    result+="<div class=\"actionBlock whileBlock\" data-blockType=\"while\" data-blockId=\""+newBlockId()+"\"><button class=\"removeActionBlock\">Usuń</button><br />"
    result+="Rób dopóki: <button class=\"addConditionButton\">Wybierz warunek</button><br />"
    result+=stringMainBlock();
    result+="</div>"
    return result;
}

var edytowanyActionBlock;
var edytowanyConditionBlock;

function poprawNazweAkcji(where, action) {
    
    $(where).replaceWith(stringSeriousActionBlock(action));
    addButtonFunctions();
}

function poprawNazweWarunku(where, condition) {
    //alert("Poprawiam nazwę warunku");
    $(".addConditionButton", where).replaceWith("<button class=\"addConditionButton editConditionButton\">Zmień warunek</button><br />");
    if(!hasCondition($(where).attr("data-blockId"))) {
        //alert("Dodaje nowy warunek."+condition.conditions[0].westSideValue);
        var newCondition=new Condition();
        newCondition.id=$(where).attr("data-blockId");    
        newCondition.condition=condition;
        Conditions.push(newCondition);  
    }
    else {
        //alert("Poprawiam warunek."+condition.conditions[0].westSideValue);
        var newCondition=getCondition($(where).attr("data-blockId"));
        rmCondition($(where).attr("data-blockId"));
        newCondition.condition=condition;
        newCondition.id=$(where).attr("data-blockId");
        Conditions.push(newCondition);  
    }

    addButtonFunctions();
//alert("Dodaję condition o id "+newCondition.id + " i typie "+newCondition.type+" a w niej typ "+newCondition.condition.lol);
}

function rozpiszEvent(where, event) {
    $(where).html(stringMainBlock());
    for(var i=0; i<event.actions.actions.length; i++) {
        var newWhere=$(".actionBlock .actualAction",where).first();
        rozpiszAction(newWhere, event.actions.actions[i]);
    }
}

function rozpiszAction(where, abek) {
    if(abek.type=="block") {
        //alert("block");
        if(abek.actions.length==0) return;
        //$(where).parent().replaceWith(stringMainBlock());
        for(var i=0; i<abek.actions.length; i++) {
            //var newWhere=$(".actionBlock .actualAction",where).last();    
            var newWhere=$(where);
            rozpiszAction(newWhere, abek.actions[i]);
        }
    }
    else if(abek.type=="for") {
        //alert("for");
        $(where).append(stringForBlock());
        var newWhere=$(".actionBlock .actualAction",where).last(); 
        rozpiszAction(newWhere, abek.toDo);
    }
    else if(abek.type=="while") {
        //alert("while");
        $(where).append(stringWhileBlock());
        var newWhere=$(".actionBlock .actualAction",where).last(); 
        rozpiszAction(newWhere, abek.toDo);      
        
    }
    else if(abek.type=="if") {
        $(where).append(stringIfBlock());
        poprawNazweWarunku($(".ifElseBlock",where).last(), abek.cond);
        var element=$(".ifElseBlock", where).last();
        var eelement=$(".actionBlock", element).first();
        var newWhere=$(".actualAction", eelement).first();
        if(abek.doIf.actions.length>0) {
        
            rozpiszAction(newWhere, abek.doIf);
        }
        var el=$(eelement).nextAll(".actionBlock").first();
        newWhere=$(".actualAction", el).first();
        if(abek.doElse.actions.length>0) {
            //alert("doElse>0");
            rozpiszAction(newWhere, abek.doElse);
        }
    
    }    
    else if(abek.type=="action") {
        $(where).append(stringSeriousActionBlock(abek.action));   
        
    }
}

function addButtonFunctions() {
    $("button.addActionBlock").button()
    $(".addActionBlock").unbind("click");
    $(".addActionBlock").click(function() {
        $(this).parent().children(".actualAction").append(stringBlock());
        addButtonFunctions();
    });
    $(".removeActionBlock").unbind("click");
    $(".removeActionBlock").click(function() {
        $(this).closest(".actionBlock").remove();
    });
    $(".addActionButton").unbind("click");
    $(".addActionButton").click(function() {
        $('#dialog-callfunction').dialog('open');
        edytowanyActionBlock=$(this);
        edytowanyAction=null;
    });
    $(".addConditionButton").click(function() {
        
        edytowanyConditionBlock=$(this).parent();
        if($(".editConditionButton", edytowanyConditionBlock).exists()) {
            edytowanyCondition=getCondition($(edytowanyConditionBlock).attr("data-blockId"));
        }
        else {
            edytowanyCondition=null;    
        }
        $("#dialog-complex-condition").dialog('open');
        
    });

    $(".addIfStatement").unbind("click");
    $(".addIfStatement").click(function() {
        $(this).parent().parent().replaceWith(stringIfBlock());
        addButtonFunctions();
    });
    $(".addForStatement").click(function() {
        $(this).parent().parent().replaceWith(stringForBlock());
        addButtonFunctions();
    });
    $(".addWhileStatement").click(function() {
        $(this).parent().parent().replaceWith(stringWhileBlock());
        addButtonFunctions();
    });
    $(".actualAction button").button();
    $(".actualAction").sortable();
}

function hasEvent(object, event) {
    for(var i=0; i<object.events.length; i++) {
        if(object.events[i].name==event) {
            return true;
        }
    }
    return false;
}

function getEvent(object, event) {
    for(var i=0; i<object.events.length; i++) {
        if(object.events[i].name==event) {
            return object.events[i];
        }
    }
    return null;
}

function showScenarioList() {
    
    var wnetrze="";
  
    wnetrze+="<div><center>[<a class=\"addScenarioEvent\" href=\"#\">Dodaj obsługę nowego zdarzenia</a>]</center><br />"+                   
    "<ol style=\"width: 100%;\" class=\"addedScenarioEvents selectable\" id=\"addedScenarioEvents\">"+
    "<li class=\"ui-widget-content scenarioEventEditSelect\" data-event=\"onBegin\">Rozpoczęcie scenariusza</li>"+
    "<li class=\"ui-widget-content scenarioEventEditSelect\" data-event=\"onEnd\">Zakończenie scenariusza</li>"+
    "</ol>"+      
    "</div>";
    
    $("#scenariosList").html(wnetrze);

    addScenarioEventsFunctions();
    
}



function showZoneList() {
    $("#zonesList").accordion("destroy");
    var n=Zones.length;
    var wnetrze="";
    for(var x=0; x<n; x++) {
        //var imgSRC="http://cerber.cs.put.poznan.pl/~inf94318/mapa.png";
        var imgTXT="";
        var imgSRC="";
        if(Zones[x].icon!=null && Zones[x].icon!="" && Zones[x].icon>0) {
            var media=getMedia(Zones[x].icon);
            console.log("img: id: "+Zones[x].icon);
            console.log("img: pid: "+media.pathid);
            //imgSRC="http://iris.cs.put.poznan.pl/"+
            imgSRC=$("#listAssetsDiv").find("[data-id="+media.pathid+"]").children(".asset_preview").first().attr("data-href");
            console.log("img: SRC: "+imgSRC);
            imgTXT="<img style=\"height: 100px; width: 100px\"src=\""+imgSRC+"\" />";
            //imgSRC=
        }
        wnetrze+=
        "<h3>"+
        Zones[x].name+
        " ("+
        Zones[x].idname+
        ")</h3><div>[<a data-idek=\""+
        x+
        "\"class=\"editZone\" href=\"#\">Edytuj</a>]&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[<a data-idek=\""+
        x+
        "\"class=\"deleteZone"+
        "\" href=\"#\">Usuń</a>]<hr><table style=\"width: 100%\"><tr><td style=\"width: 15%\">"+imgTXT+"</td><td><table><tr><td style=\"width: 25%;\"><b>Nazwa:</b> </td><td style=\"width: 25%;\">"+
        Zones[x].name+
        "</td><td style=\"width: 25%;\"><b>Stan: </b></td><td style=\"width: 25%;\">"+
        Zones[x].state+
        "</td></tr><tr><td><b>Identyfikator:</b> </td><td>"+
        Zones[x].idname+
        "</td><td><b>Widoczność: </b></td><td>"+
        Zones[x].visible+
        "</td></tr><tr><td><b>ZoneVentureID:</b>  </td><td>"+
        Zones[x].id+
        "</td></tr></table></td></tr></table><b>Opis: </b>"+
        Zones[x].description+
        "<hr><center>[<a data-idek=\""+
        x+
        "\"class=\"addZoneEvent\" href=\"#\">Dodaj obsługę nowego zdarzenia</a>]</center><br />"+                   
        "<ol data-idek=\""+
        x+
        "\" style=\"width: 100%;\" class=\"addedZoneEvents selectable\" id=\"addedZoneEvents\">"+
        "<li class=\"ui-widget-content zoneEventEditSelect\" data-event=\"onEnter\">Wejście do strefy</li>"+
        "<li class=\"ui-widget-content zoneEventEditSelect\" data-event=\"onQuit\">Wyjście ze strefy</li>"+
        "<li class=\"ui-widget-content zoneEventEditSelect\" data-event=\"onVisibleChange\">Zmiana widoczności strefy</li>"+
        "<li class=\"ui-widget-content zoneEventEditSelect\" data-event=\"onStateChange\">Zmiana stanu strefy</li>"+
        "</ol>"+            
        "</div>";
    }
    $("#zonesList").html(wnetrze);
    $(".editZone").click(function(){
        edytowanaZona=this.getAttribute("data-idek");
        edytowanyObject=Zones[this.getAttribute("data-idek")];
        $("#dialog-zone").dialog("open");
    });
    $(".deleteZone").click(function(){
        removeZone(parseInt(this.getAttribute("data-idek")));
    });
    
    addZoneEventsFunctions();
    
    $("#zonesList").accordion({
        collapsible: true
    });
}

function showItemList() {
    $("#itemsList").accordion("destroy");
    var n=Items.length;
    var wnetrze="";
    for(var x=0; x<n; x++) {

        wnetrze+="<h3>"+Items[x].name+" ("+Items[x].idname+")</h3><div>[<a data-idek=\""+x+"\"class=\"editItem"+"\" href=\"#\">Edytuj</a>]&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[<a data-idek=\""+x+"\"class=\"deleteItem"+"\" href=\"#\">Usuń</a>]<hr><table style=\"width: 100%\"><tr><td style=\"width: 15%\"><img style=\"height: 100px; width: 100px\"src=\"http://cerber.cs.put.poznan.pl/~inf94318/mapa.png\" /></td><td><table><tr><td style=\"width: 25%;\"><b>Nazwa:</b> </td><td style=\"width: 25%;\">"+Items[x].name+"</td><td style=\"width: 25%;\"><b>Stan: </b></td><td style=\"width: 25%;\">"+Items[x].state+"</td></tr><tr><td><b>Identyfikator:</b> </td><td>"+Items[x].idname+"</td><td><b>Widoczność: </b></td><td>"+Items[x].visible+"</td></tr><tr><td><b>ZoneVentureID:</b>  </td><td>"+Items[x].id+"</td><td><b>Strefa: </b></td><td>"+Items[x].zone+"</td></tr></table></td></tr></table><b>Opis: </b>"+Items[x].description+
        "<hr><center>[<a data-idek=\""+
        x+
        "\"class=\"addItemEvent\" href=\"#\">Dodaj obsługę nowego zdarzenia</a>]</center><br />"+                   
        "<ol data-idek=\""+
        x+
        "\" style=\"width: 100%;\" class=\"addedItemEvents selectable\" id=\"addedItemEvents\">"+
        "<li class=\"ui-widget-content itemEventEditSelect\" data-event=\"onFind\">Znalezienie przedmiotu</li>"+
        "<li class=\"ui-widget-content itemEventEditSelect\" data-event=\"onPick\">Wzięcie przedmiotu</li>"+
        "<li class=\"ui-widget-content itemEventEditSelect\" data-event=\"onDrop\">Zostawienie przedmiotu</li>"+
        "<li class=\"ui-widget-content itemEventEditSelect\" data-event=\"onVisibleChange\">Zmiana widoczności przedmiotu</li>"+
        "<li class=\"ui-widget-content itemEventEditSelect\" data-event=\"onStateChange\">Zmiana stanu przedmiotu</li>"+
        "<li class=\"ui-widget-content itemEventEditSelect\" data-event=\"onZoneChange\">Zmiana strefy przedmiotu</li>"+
        "</ol>"+            
        "</div>";
    }
    $("#itemsList").html(wnetrze);
    $(".editItem").click(function(){
        edytowanyItem=this.getAttribute("data-idek");
        edytowanyObject=Items[this.getAttribute("data-idek")];
        $("#dialog-item").dialog("open");
    });
    $(".deleteItem").click(function(){
        removeItem(parseInt(this.getAttribute("data-idek")));
    });
    
    addItemEventsFunctions();
    
    $("#itemsList").accordion({
        collapsible: true
    });
}

function showCharacterList() {
    $("#charactersList").accordion("destroy");
    var n=Characters.length;
    var wnetrze="";
    for(var x=0; x<n; x++) {
        wnetrze+=
        "<h3>"+
        Characters[x].name+
        " ("+
        Characters[x].idname+
        ")</h3><div>[<a data-idek=\""+
        x+
        "\"class=\"editCharacter\" href=\"#\">Edytuj</a>]&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[<a data-idek=\""+
        x+
        "\"class=\"deleteCharacter"+
        "\" href=\"#\">Usuń</a>]<hr><table style=\"width: 100%\"><tr><td style=\"width: 15%\"><img style=\"height: 100px; width: 100px\"src=\"http://cerber.cs.put.poznan.pl/~inf94318/mapa.png\" /></td><td><table><tr><td style=\"width: 25%;\"><b>Nazwa:</b> </td><td style=\"width: 25%;\">"+
        Characters[x].name+
        "</td><td style=\"width: 25%;\"><b>Stan: </b></td><td style=\"width: 25%;\">"+
        Characters[x].state+
        "</td></tr><tr><td><b>Identyfikator:</b> </td><td>"+
        Characters[x].idname+
        "</td><td><b>Widoczność: </b></td><td>"+
        Characters[x].visible+
        "</td></tr><tr><td><b>ZoneVentureID:</b>  </td><td>"+
        Characters[x].id+
        "</td><td><b>Strefa: </b></td><td>"+
        Characters[x].zone+
        "</td></tr><tr><td><b>Płeć:</b> </td><td>"+
        Characters[x].sex+
        "</td></tr></table></td></tr></table><b>Opis: </b>"+
        Characters[x].description+
        "<hr><center>[<a data-idek=\""+
        x+
        "\"class=\"addCharacterEvent\" href=\"#\">Dodaj obsługę nowego zdarzenia</a>]</center><br />"+                   
        "<ol data-idek=\""+
        x+
        "\" style=\"width: 100%;\" class=\"addedCharacterEvents selectable\" id=\"addedCharacterEvents\">"+
        "<li class=\"ui-widget-content characterEventEditSelect\" data-event=\"onGoodMorning\">Spotkanie z postacią</li>"+
        "<li class=\"ui-widget-content characterEventEditSelect\" data-event=\"onDialog\">Rozmowa z postacią</li>"+
        "<li class=\"ui-widget-content characterEventEditSelect\" data-event=\"onVisibleChange\">Zmiana widoczności postaci</li>"+
        "<li class=\"ui-widget-content characterEventEditSelect\" data-event=\"onStateChange\">Zmiana stanu postaci</li>"+
        "<li class=\"ui-widget-content characterEventEditSelect\" data-event=\"onZoneChange\">Zmiana strefy postaci</li>"+
        "</ol>"+            
        "</div>";
    }
    $("#charactersList").html(wnetrze);
    $(".editCharacter").click(function(){
        edytowanyCharacter=this.getAttribute("data-idek");
        edytowanyObject=Characters[this.getAttribute("data-idek")];
        $("#dialog-character").dialog("open");
    });
    $(".deleteCharacter").click(function(){
        removeCharacter(parseInt(this.getAttribute("data-idek")));
    });
    
    addCharacterEventsFunctions();
    
    $("#charactersList").accordion({
        collapsible: true
    });
}
    
function showQuestList() {
    $("#questsList").accordion("destroy");
    var n=Quests.length;
    var wnetrze="";
    for(var x=0; x<n; x++) {
        wnetrze+=
        "<h3>"+
        Quests[x].name+
        " ("+
        Quests[x].idname+
        ")</h3><div>[<a data-idek=\""+
        x+
        "\"class=\"editQuest\" href=\"#\">Edytuj</a>]&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[<a data-idek=\""+
        x+
        "\"class=\"deleteQuest"+
        "\" href=\"#\">Usuń</a>]<hr><table style=\"width: 100%\"><tr><td style=\"width: 15%\"><img style=\"height: 100px; width: 100px\"src=\"http://cerber.cs.put.poznan.pl/~inf94318/mapa.png\" /></td><td><table><tr><td style=\"width: 25%;\"><b>Nazwa:</b> </td><td style=\"width: 25%;\">"+
        Quests[x].name+
        "</td><td style=\"width: 25%;\"><b>Stan: </b></td><td style=\"width: 25%;\">"+
        Quests[x].state+
        "</td></tr><tr><td><b>Identyfikator:</b> </td><td>"+
        Quests[x].idname+
        "</td><td><b>ZoneVentureID:</b>  </td><td>"+
        Quests[x].id+
        "</td></tr></table></td></tr></table><b>Opis: </b>"+
        Quests[x].description+

        "<hr><center>[<a data-idek=\""+
        x+
        "\"class=\"addQuestEvent\" href=\"#\">Dodaj obsługę nowego zdarzenia</a>]</center><br />"+                   
        "<ol data-idek=\""+
        x+
        "\" style=\"width: 100%;\" class=\"addedQuestEvents selectable\" id=\"addedQuestEvents\">"+
        "<li class=\"ui-widget-content questEventEditSelect\" data-event=\"onBegin\">Rozpoczęcie wykonywania zadania</li>"+
        "<li class=\"ui-widget-content questEventEditSelect\" data-event=\"onComplete\">Zakończenie wykonywania zadania</li>"+
        "<li class=\"ui-widget-content questEventEditSelect\" data-event=\"onStateChange\">Zmiana stanu zadania</li>"+
        "</ol>"+            
        "</div>";
    }
    $("#questsList").html(wnetrze);
    $(".editQuest").click(function(){
        edytowanyQuest=this.getAttribute("data-idek");
        edytowanyObject=Quests[this.getAttribute("data-idek")];
        $("#dialog-quest").dialog("open");
    });
    $(".deleteQuest").click(function(){
        removeQuest(parseInt(this.getAttribute("data-idek")));
    });
    
    addQuestEventsFunctions();
    
    $("#questsList").accordion({
        collapsible: true
    });


}

function showMediaList() {
    $("#mediasList").accordion("destroy");
    var n=Medias.length;
    var wnetrze="";
    for(var x=0; x<n; x++) {
        //wnetrze+="<h3>"+Medias[x].idname+"</h3><div></div>";
        wnetrze+=
        "<h3>"+
        Medias[x].idname+
        "</h3><div>[<a data-idek=\""+
        x+
        "\"class=\"editMedia\" href=\"#\">Edytuj</a>]&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[<a data-idek=\""+
        x+
        "\"class=\"deleteMedia"+
        "\" href=\"#\">Usuń</a>]<hr><table><tr><td style=\"width: 25%;\"><b>Identyfikator:</b> </td><td style=\"width: 25%;\">"+
        Medias[x].idname+
        "</td><td style=\"width: 25%;\"><b>Typ: </b></td><td style=\"width: 25%;\">"+
        Medias[x].type+
        "</td></tr><tr><td><b>ZoneVentureID: </b> </td><td>"+
        Medias[x].id+
        "</td><td><b>Ścieżka: </b>  </td><td>"+
        Medias[x].pathname+
        "</td></tr></table>"+
        "<hr><center>[<a data-idek=\""+
        x+
        "\"class=\"addMediaEvent\" href=\"#\">Dodaj obsługę nowego zdarzenia</a>]</center><br />"+                   
        "<ol data-idek=\""+
        x+
        "\" style=\"width: 100%;\" class=\"addedMediaEvents selectable\" id=\"addedMediaEvents\">"+
        "<li class=\"ui-widget-content mediaEventEditSelect\" data-event=\"onPlay\">Odtworzenie mediów</li>"+
        "</ol>"+            
        "</div>";
    }
    $("#mediasList").html(wnetrze);
    $(".editMedia").click(function(){
        edytowaneMedia=this.getAttribute("data-idek");
        edytowanyObject=Medias[this.getAttribute("data-idek")];
        $("#dialog-media").dialog("open");
    });
    $(".deleteMedia").click(function(){
        removeMedia(parseInt(this.getAttribute("data-idek")));
    });
    
    addMediaEventsFunctions();
    
    $("#mediasList").accordion({
        collapsible: true
    });    
    
    var imgSRC=$("#listAssetsDiv").find("[data-id=11]").children(".asset_preview").first().attr("data-href");
    console.log("img: SRC: "+imgSRC);
}

function showTimerList() {
    $("#timersList").accordion("destroy");
    var n=Timers.length;
    var wnetrze="";
    for(var x=0; x<n; x++) {
        wnetrze+="<h3>"+Timers[x].idname+"</h3><div>[<a data-idek=\""+
        x+
        "\"class=\"editTimer\" href=\"#\">Edytuj</a>]&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[<a data-idek=\""+
        x+
        "\"class=\"deleteTimer"+
        "\" href=\"#\">Usuń</a>]<hr><table><tr><td style=\"width: 25%;\"><b>Identyfikator:</b> </td><td style=\"width: 25%;\">"+
        Timers[x].idname+
        "</td><td style=\"width: 25%;\"><b>Czas: </b></td><td style=\"width: 25%;\">"+
        Timers[x].interval+
        "</td></tr><tr><td><b>ZoneVentureID: </b> </td><td>"+
        Timers[x].id+
        "</td><td></td><td>"+
        "</td></tr></table>"+
        "<hr><center>[<a data-idek=\""+
        x+
        "\"class=\"addTimerEvent\" href=\"#\">Dodaj obsługę nowego zdarzenia</a>]</center><br />"+                   
        "<ol data-idek=\""+
        x+
        "\" style=\"width: 100%;\" class=\"addedTimerEvents selectable\" id=\"addedTimerEvents\">"+
        "<li class=\"ui-widget-content timerEventEditSelect\" data-event=\"onStart\">Rozpoczęcie odliczania</li>"+
        "<li class=\"ui-widget-content timerEventEditSelect\" data-event=\"onStop\">Zatrzymanie odliczania</li>"+
        "<li class=\"ui-widget-content timerEventEditSelect\" data-event=\"onTimer\">Zakończenie odliczania</li>"+
        "</ol>"+            
        "</div>";
    }
    $("#timersList").html(wnetrze);
    
    $(".editTimer").click(function(){
        edytowanyTimer=this.getAttribute("data-idek");
        edytowanyObject=Variables[this.getAttribute("data-idek")];
        $("#dialog-timer").dialog("open");
    });
    $(".deleteTimer").click(function(){
        removeTimer(parseInt(this.getAttribute("data-idek")));
    });
    
    addTimerEventsFunctions();
    
    $("#timersList").accordion({
        collapsible: true
    });
}

function showVariableList() {
    $("#variablesList").accordion("destroy");
    var n=Variables.length;
    var wnetrze="";
    for(var x=0; x<n; x++) {
        wnetrze+="<h3>"+Variables[x].idname+"</h3>"+
        "<div>[<a data-idek=\""+
        x+
        "\"class=\"editVariable\" href=\"#\">Edytuj</a>]&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[<a data-idek=\""+
        x+
        "\"class=\"deleteVariable"+
        "\" href=\"#\">Usuń</a>]<hr><table><tr><td style=\"width: 25%;\"><b>Identyfikator:</b> </td><td style=\"width: 25%;\">"+
        Variables[x].idname+
        "</td><td style=\"width: 25%;\"><b>Typ: </b></td><td style=\"width: 25%;\">"+
        Variables[x].type+
        "</td></tr><tr><td><b>ZoneVentureID: </b> </td><td>"+
        Variables[x].id+
        "</td><td><b>Wartość </b>  </td><td>"+
        Variables[x].value+
        "</td></tr></table>"+
        "<hr><center>[<a data-idek=\""+
        x+
        "\"class=\"addVariableEvent\" href=\"#\">Dodaj obsługę nowego zdarzenia</a>]</center><br />"+                   
        "<ol data-idek=\""+
        x+
        "\" style=\"width: 100%;\" class=\"addedVariableEvents selectable\" id=\"addedVariableEvents\">"+
        "<li class=\"ui-widget-content variableEventEditSelect\" data-event=\"onValueChange\">Zmiana wartości zmiennej</li>"+
        "</ol>"+                   
        "</div>";
    } 
    $("#variablesList").html(wnetrze);
    $(".editVariable").click(function(){
        edytowanaVariable=this.getAttribute("data-idek");
        edytowanyObject=Variables[this.getAttribute("data-idek")];
        $("#dialog-variable").dialog("open");
    });
    $(".deleteVariable").click(function(){
        removeVariable(parseInt(this.getAttribute("data-idek")));
    });
    
    addVariableEventsFunctions();
    
    $("#variablesList").accordion({
        collapsible: true
    });
}


function removeZone(index) {
    Zones.splice(index,1);
    showZoneList();
}

function removeItem(index) {
    Items.splice(index,1);
    showItemList();
}

function removeCharacter(index) {
    Characters.splice(index,1);
    showCharacterList();
}

function removeQuest(index) {
    Quests.splice(index,1);
    showQuestList();
}

function removeMedia(index) {
    Medias.splice(index,1);
    showMediaList();
}

function removeTimer(index) {
    Timers.splice(index,1);
    showTimerList();
}
function removeFromList(id,list){
    var index=-1;
    for(var i=0;i<list.length;i++){
        if(list[i].id==id){
            index=i;
            break;
        }
    }
    if(index!=-1){
        list.splice(index,1)
    }
}

function removeVariable(index) {
    var id=Variables[index].id;
    var type=Variables[index].type;
    Variables.splice(index,1);
    if(type=="INTEGER"){
        removeFromList(id,Integers)
    }
    else if(type=="STRING"){
        removeFromList(id,Strings)
    }
    else if(type=="DOUBLE"){
        removeFromList(id,Doubles)
    }
    else if(type=="BOOLEAN"){
        removeFromList(id,Booleans)
    }
    else if(type=="CHAR"){
        removeFromList(id,Chars)
    }
    showVariableList();
}

function show_map() {
    $("#map_canvas").dialog('destroy');
    $("#map_canvas").dialog({
        title: "Mapa",
        autoOpen: false,    
        modal: true,
        resizeStop: function(event, ui) {
            google.maps.event.trigger(map, 'resize')
        },
        open: function(event, ui) {
                     
            google.maps.event.trigger(map, 'resize');
                       
        },
        close: function(event, ui){
            $("#map_canvas").dialog('destroy');
            set_map_dialog();
        },
        height: $(window).height()*0.9,
        width: $(window).width()*0.9
        
        
    });
}

function set_map_dialog() {
    $("#map_canvas").dialog({
        title: "Zaznacz strefę na mapie",
        autoOpen: false,    
        modal: true,
        resizeStop: function(event, ui) {
            google.maps.event.trigger(map, 'resize')
        },
        open: function(event, ui) {
            zatwierdzone=false;
            if(newZone!=null) newZone.setMap(null);
            if(edytowanyPolygon!=null) {
                var pathekCopy=new google.maps.MVCArray();
                for(var i=0; i<edytowanyPolygon.getPath().getLength(); i++) {
                    pathekCopy.push(edytowanyPolygon.getPath().getAt(i));
                }
                kopiaKopiiPolygonu=new google.maps.Polygon({
                    paths: pathekCopy,
                    strokeColor: "#0000FF",
                    strokeOpacity: 0.8,
                    strokeWeight: 2,
                    fillColor: "#0000FF",
                    fillOpacity: 0.35
                });
                edytowanyPolygon.setMap(null);
                kopiaKopiiPolygonu.setMap(map);
            }
            else kopiaKopiiPolygonu=null;
            
            google.maps.event.trigger(map, 'resize');
            
           
        },
        close: function(event, ui){
            if(kopiaKopiiPolygonu!=null) {
                kopiaKopiiPolygonu.setOptions({
                    fillColor: "#FF0000",
                    strokeColor: "#FF0000"
                });
            }
            
            if(newZone!=null) newZone.setMap(null);
            newZone=null;
            
            if(zatwierdzone) {
                if(kopiaKopiiPolygonu!=null) {
                    if(edytowanyPolygon!=null) edytowanyPolygon.setMap(null);
                    edytowanyPolygon=kopiaKopiiPolygonu;
                    kopiaKopiiPolygonu=null;
                }
            }

            if(kopiaKopiiPolygonu!=null) {
                kopiaKopiiPolygonu.setMap(null);
                kopiaKopiiPolygonu=null;
            }
            poly.getPath().clear();
            clearOverlays();
            odcinki.length=0;   
        },
        height: $(window).height()*0.8,
        width: $(window).width()*0.8,
        buttons: {
            "Cofnij": function() {
                if(poly.getPath().getLength()>0) {
                    poly.getPath().pop();
                    odcinki.pop();
                    var mark=markersArray.pop();
                    mark.setMap(null);
                }                
            },
            "Zacznij od początku": function() {
                if(newZone!=null) newZone.setMap(null);
                if(poly.getPath().getLength()>0) {
                    poly.getPath().clear();
                    clearOverlays();
                    odcinki.length=0;                    
                }
                else if(kopiaKopiiPolygonu!=null) {
                    kopiaKopiiPolygonu.setMap(null);
                    kopiaKopiiPolygonu=null;
                }
                zaznaczamyZone=true;
            },
            "Zatwierdź": function() {
                zatwierdzone=true;
                if(newZone != null) {
                    kopiaKopiiPolygonu=newZone;
                    zaznaczamyZone=false;
                    $( this ).dialog( "close" );
                    newZone=null;
                }
                else if(!zaznaczamyZone) {
                    $( this ).dialog( "close" );
                }
                else alert("Strefa nie zaznaczona, tej!");
            }
        }
    });
}


function showLists() {
    $("#zonesList").accordion({
        collapsible: true
    });
    $("#itemsList").accordion({
        collapsible: true
    });
    $("#charactersList").accordion({
        collapsible: true
    });
    $("#questsList").accordion({
        collapsible: true
    });
    $("#mediasList").accordion({
        collapsible: true
    });
    $("#timersList").accordion({
        collapsible: true
    });
    $("#variablesList").accordion({
        collapsible: true
    });    
    
    showScenarioList();
    showZoneList();
    showItemList();
    showCharacterList();
    showQuestList();
    showMediaList();
    showTimerList();
    showVariableList();
}

function addSampleObjects() {
    Items.push(new Item("mapa", "Tajemnicza mapa", "Przyglądasz się w skupieniu mapie.. Czyżby.. Tak! To jest to! Kraina przez wielu uważana za mityczną, nieistniejącą, pochodzącą z bajek opowiadanych dzieciom na dobranoc.. Kraina miodem i kakałem płynąca, kraina wiecznych deszczów.. Cytadela! Mapa wskazuje dokładną drogę do tej tajemniczej krainy. I długi długi długaśny tekst, żeby na przykład sie nie zmieściło na tym całym TextView, i najlepiej żeby dało scrollView do tego, ehh.. Chyba jednak nadal trochę za krótkie było. Teraz może już będzie git.", "zone", "visible", "state"));
    Items.push(new Item2("itemek", "Inny śmieszny item"));
    Characters.push(new Character("jan", "Pan Jan", "sex", "description", "zone", "state", "visible"));
    Characters.push(new Character("john", "John Carbon", "sex", "description", "zone", "state", "visible"));
    Zones.push(new Zone("cytadela", "Cytadela", "description", "stan1", true, "points",true,""));
    Zones.push(new Zone("rynek", "Rynek", "description", "stan2", true, "points"));
    Zones.push(new Zone("domek", "Mój domek", "description", "state", false, "points"));
    Quests.push(new Quest("idname1", "Quest 1", "Opis questa 1..", "state"));
    Quests.push(new Quest("idname2", "Quest 2", "Opis questa 2..", "state"));
    Medias.push(new Media("idname1", "path1", "type1"));
    Medias.push(new Media("idname2", "path2", "type2"));
    Variables.push(new Variable("var1", "STRING", "Ala ma kota"));
    Variables.push(new Variable("var2", "INTEGER", "10")); 
}


$(document).ready(function(){

    //addSampleObjects();
    $("#saveScenario").button();
    if(scenarioJSON!=null && scenarioJSON.xml!=null && scenarioJSON.xml!="") parse_xml(scenarioJSON.xml);
    
    /*GroupList.push(new GroupDefinition(new Array("STRING", "BOOLEAN","INTEGER","ZONE","DOUBLE"), "Strefa", Zones, new Array(new FieldDefinition("STRING","idname","Identyfikator"),new FieldDefinition("BOOLEAN","visibility","Widoczność"),new FieldDefinition("DOUBLE","test","TEST"),new FieldDefinition("INTEGER","id","ID"))));
    GroupList.push(new GroupDefinition(new Array("STRING", "BOOLEAN","ITEM"), "Przedmioty", Items,new Array(new FieldDefinition("STRING","idname","Identyfikators"),new FieldDefinition("BOOLEAN","visibility","Widocznośćs"))));
    GroupList.push(new GroupDefinition(new Array("STRING", "BOOLEAN"), "Cos tam", Items,""));
     */
    GroupList.push(new GroupDefinition(new Array("STRING", "BOOLEAN","ZONE"), "Strefy", Zones, 
        new Array(new FieldDefinition("STRING","idname","Identyfikator"),
            new FieldDefinition("BOOLEAN","visible","Widoczność"),
            new FieldDefinition("BOOLEAN","owned","Widoczność"),
            new FieldDefinition("STRING","name","Nazwa"),
            new FieldDefinition("STRING","description","Opis"),
            new FieldDefinition("STRING","state","Stan"))));
    GroupList.push(new GroupDefinition(new Array("STRING", "BOOLEAN","CHARACTER"), "Postacie", Characters,
        new Array(new FieldDefinition("STRING","idname","Identyfikator"),
            new FieldDefinition("BOOLEAN","visible","Widoczność"),
            new FieldDefinition("BOOLEAN","owned","Widoczność"),
            new FieldDefinition("STRING","name","Nazwa"),
            new FieldDefinition("STRING","description","Opis"),
            new FieldDefinition("STRING","state","Stan"))));
    GroupList.push(new GroupDefinition(new Array("STRING", "BOOLEAN","ITEM"), "Przedmioty", Items,
        new Array(new FieldDefinition("STRING","idname","Identyfikator"),
            new FieldDefinition("BOOLEAN","visible","Widoczność"),
            new FieldDefinition("BOOLEAN","owned","Widoczność"),
            new FieldDefinition("STRING","name","Nazwa"),
            new FieldDefinition("STRING","description","Opis"),
            new FieldDefinition("STRING","state","Stan"))));
    GroupList.push(new GroupDefinition(new Array("STRING", "BOOLEAN","QUEST"), "Zadania", Quests,
        new Array(new FieldDefinition("STRING","idname","Identyfikator"),
            new FieldDefinition("BOOLEAN","visible","Widoczność"),
            new FieldDefinition("STRING","name","Nazwa"),
            new FieldDefinition("STRING","description","Opis"),
            new FieldDefinition("STRING","state","Stan"))));
    GroupList.push(new GroupDefinition(new Array("MEDIA"), "Media", Medias,""));
    GroupList.push(new GroupDefinition(new Array("INTEGER","TIMER"), "Timery", Timers,
        new Array(new FieldDefinition("STRING","idname","Identyfikator"),
            new FieldDefinition("INTEGER","interval","Interwał"))));
    GroupList.push(new GroupDefinition(new Array("INTEGER"), "Zm. liczbowe", Integers,""));
    GroupList.push(new GroupDefinition(new Array("DOUBLE"), "Zm. rzeczywiste", Doubles,""));
    GroupList.push(new GroupDefinition(new Array("STRING"), "Zm. tekstowe", Strings,""));
    GroupList.push(new GroupDefinition(new Array("CHAR"), "Zm. znakowe", Chars,""));
    GroupList.push(new GroupDefinition(new Array("BOOLEAN"), "Zm. logiczne", Booleans,""));



    showLists();

    initializeDialogs();

    var zatwierdzone=false;
    $("#tabs").tabs();
    
    set_map_dialog();
    $("#createZoneButton").button();
    $("#createItemButton").button();
    $("#createQuestButton").button();
    $("#createCharacterButton").button();
    $("#createTimerButton").button();
    $("#createVariableButton").button();
    $("#createMediaButton").button();
    
    $("#show_map").click(function() {
        show_map();
        zaznaczamyZone=false;
        $("#map_canvas").dialog("open");
    });
    $("#points").click(function(){
        if(edytowanyPolygon!=null) zaznaczamyZone=false;
        else zaznaczamyZone=true;
        $("#map_canvas").dialog("open");
        return false;
    });
    $("#generateXML").click(function(){
        generate_xml();
    });
    

    



    //
    //    $("#addCharacterEvent").dialog({
    //        autoOpen: false,
    //        modal: true,
    //        width: $(window).width()*0.4, 
    //        title: "Wybierz zdarzenie",
    //        buttons: {
    //            "Edytuj" : function () {
    //                $(this).dialog("close");
    //                $("#editCharacterEvent").dialog("open");
    //            }
    //        }
    //    });
    
    
    readyEventsFunctions();
    
    
});

