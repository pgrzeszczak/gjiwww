<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
/*
 * Trochę to okroiłem na nasze potrzeby i dodałem adnotacje doctrine
 */
namespace GJIwww\Entities;

use Symfony\Component\Security\Core\User\UserInterface as SecurityUserInterface;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Silex\Application;

/**
 * Storage agnostic user object
 *
 * @author Thibault Duplessis <thibault.duplessis@gmail.com>
 * @author Johannes M. Schmitt <schmittjoh@gmail.com>
 * @Table(name="user_user")
 * @Entity(repositoryClass="UserRepository")
 */
class User implements SecurityUserInterface, AdvancedUserInterface
{
    const ROLE_DEFAULT = 'ROLE_USER';
    const ROLE_SUPER_ADMIN = 'ROLE_SUPER_ADMIN';

    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @Column(name="username", type="string")
     */
    protected $username;

    /**
     * @var string
     * @Column(name="email", type="string")
     */
    protected $email;

    /**
     * @Column(name="firstname", type="string")
     */
    protected $firstname;

    /**
     * @Column(name="lastname", type="string")
     */
    protected $lastname;
  
    /**
     * @var boolean
     * @Column(name="enabled", type="boolean")
     */
    protected $enabled;

    /**
     * The salt to use for hashing
     *
     * @var string
     * @Column(name="salt", type="string")
     */
    protected $salt;

    /**
     * Encrypted password. Must be persisted.
     *
     * @var string
     * @Column(name="password", type="string")
     */
    protected $password;

    /**
     * Plain password. Used for model validation. Must not be persisted.
     *
     * @var string
     */
    protected $plainPassword;

    /**
     * Random string sent to the user email address in order to verify it
     *
     * @var string
     * @Column(name="confirmation_token", type="string")
     */
    protected $confirmationToken;

    /**
     * @var array
     * @Column(name="roles", type="array")
     */
    protected $roles;
    
    /**
     * @var boolean
     * @Column(name="terms_accepted", type="boolean")
     */
    protected $termsAccepted;
    
    /**
     * @OneToMany(targetEntity="GJIwww\Entities\Asset", mappedBy="owner")
     * @OrderBy({"id" = "ASC"})
     */
    protected $assets;
    
    /**
     * @OneToMany(targetEntity="GJIwww\Entities\Scenario", mappedBy="author")
     * @OrderBy({"id" = "ASC"})
     */
    protected $scenarios;


    public function __construct()
    {
        $this->salt = base_convert(sha1(uniqid(mt_rand(), true)), 16, 36);
        $this->generateConfirmationToken();
        $this->enabled = false;
        $this->roles = array();
        $this->assets = new \Doctrine\Common\Collections\ArrayCollection;
        $this->scenarios = new \Doctrine\Common\Collections\ArrayCollection;
    }

    /**
     * Adds a role to the user.
     *
     * @param string $role
     */
    public function addRole($role)
    {
        $role = strtoupper($role);
        if ($role === static::ROLE_DEFAULT) {
            return;
        }

        if (!in_array($role, $this->roles, true)) {
            $this->roles[] = $role;
        }
    }

    /**
     * Implementation of SecurityUserInterface.
     *
     * @param \Symfony\Component\Security\Core\User\UserInterface $user
     * @return Boolean
     */
    public function equals(SecurityUserInterface $user)
    {
        if (!$user instanceof User) {
            return false;
        }

        if ($this->getPassword() !== $user->getPassword()) {
            return false;
        }
        if ($this->getSalt() !== $user->getSalt()) {
            return false;
        }
        if ($this->isEnabled() !== $user->isEnabled()) {
            return false;
        }

        return true;
    }

    /**
     * Serializes the user.
     *
     * The serialized data have to contain the fields used by the equals method and the username.
     *
     * @return string
     */
    public function serialize()
    {
        return serialize(array(
            $this->password,
            $this->salt,
            $this->username,
            $this->enabled,
        ));
    }

    /**
     * Unserializes the user.
     *
     * @param string $serialized
     */
    public function unserialize($serialized)
    {
        list(
            $this->password,
            $this->salt,
            $this->username,
            $this->enabled
        ) = unserialize($serialized);
    }

    /**
     * Removes sensitive data from the user.
     *
     * Implements SecurityUserInterface
     */
    public function eraseCredentials()
    {
        $this->plainPassword = null;
    }

    /**
     * Returns the user unique id.
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }
    
    /**
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }
    
    /**
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Implementation of SecurityUserInterface
     *
     * @return string
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * Gets email.
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Gets the encrypted password.
     *
     * Implements SecurityUserInterface
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Gets the plain password.
     *
     * @return string
     */
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    /**
     * Gets the confirmation token.
     *
     * @return string
     */
    public function getConfirmationToken()
    {
        return $this->confirmationToken;
    }

    /**
     * Returns the user roles
     *
     * Implements SecurityUserInterface
     *
     * @return array The roles
     */
    public function getRoles()
    {
        $roles = $this->roles;
        
        // we need to make sure to have at least one role
        $roles[] = static::ROLE_DEFAULT;

        return array_unique($roles);
    }

    /**
     * Never use this to check if this user has access to anything!
     *
     * Use the SecurityContext, or an implementation of AccessDecisionManager
     * instead, e.g.
     *
     *         $securityContext->isGranted('ROLE_USER');
     *
     * @param string $role
     * @return Boolean
     */
    public function hasRole($role)
    {
        return in_array(strtoupper($role), $this->getRoles(), true);
    }

    /**
     * Checks whether the user is enabled.
     *
     * Implements AdvancedUserInterface
     *
     * @return Boolean true if the user is enabled, false otherwise
     */
    public function isEnabled()
    {
        return $this->enabled;
    }
    
    public function isTermsAccepted()
    {
        return $this->termsAccepted;
    }

    /**
     * Tells if the the given user has the super admin role.
     *
     * @return Boolean
     */
    public function isSuperAdmin()
    {
        return $this->hasRole(static::ROLE_SUPER_ADMIN);
    }

    /**
     * Tells if the the given user is this user.
     *
     * Useful when not hydrating all fields.
     *
     * @param UserInterface $user
     * @return Boolean
     */
    public function isUser(SecurityUserInterface $user = null)
    {
        return null !== $user && $this->getId() === $user->getId();
    }

    /**
     * Removes a role to the user.
     *
     * @param string $role
     */
    public function removeRole($role)
    {
        if (false !== $key = array_search(strtoupper($role), $this->roles, true)) {
            unset($this->roles[$key]);
            $this->roles = array_values($this->roles);
        }
    }

    /**
     * Sets the username.
     *
     * @param string $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }
    
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    }
    
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    }
    
    /**
     * Sets the email.
     *
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @param Boolean $boolean
     */
    public function setEnabled($boolean)
    {
        $this->enabled = (Boolean) $boolean;
    }
    /**
     * @param Boolean $boolean
     */
    public function setTermsAccepted($boolean)
    {
        $this->termsAccepted = (Boolean) $boolean;
    }
    /**
     * Sets the hashed password.
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * Sets the super admin status
     *
     * @param Boolean $boolean
     */
    public function setSuperAdmin($boolean)
    {
        if (true === $boolean) {
            $this->addRole(static::ROLE_SUPER_ADMIN);
        } else {
            $this->removeRole(static::ROLE_SUPER_ADMIN);
        }
    }

    /**
     * Sets the plain password.
     *
     * @param string $password
     */
    public function setPlainPassword($password)
    {
        $this->plainPassword = $password;
    }

    /**
     * Sets the confirmation token
     *
     * @param string $confirmationToken
     */
    public function setConfirmationToken($confirmationToken)
    {
        $this->confirmationToken = $confirmationToken;
    }

    /**
     * Generates the confirmation token if it is not set.
     */
    public function generateConfirmationToken()
    {
        if (null === $this->getConfirmationToken()) {
            $this->setConfirmationToken($this->generateToken());
        }
    }

    /**
     * Sets the roles of the user.
     *
     * This overwrites any previous roles.
     *
     * @param array $roles
     */
    public function setRoles(array $roles)
    {
        $this->roles = array();

        foreach ($roles as $role) {
            $this->addRole($role);
        }
    }
    
    public function __toString()
    {
        return (string) $this->getUsername();
    }

    /**
     * Generates a token.
     *
     * @return string
     */
    protected function generateToken()
    {
        $bytes = false;
        if (function_exists('openssl_random_pseudo_bytes') && 0 !== stripos(PHP_OS, 'win')) {
            $bytes = openssl_random_pseudo_bytes(32, $strong);

            if (true !== $strong) {
                $bytes = false;
            }
        }

        // let's just hope we got a good seed
        if (false === $bytes) {
            $bytes = hash('sha256', uniqid(mt_rand(), true), true);
        }

        return base_convert(bin2hex($bytes), 16, 36);
    }

    public function isAccountNonExpired() {
        return true;
    }

    public function isAccountNonLocked() {
        return true;
    }

    public function isCredentialsNonExpired() {
        return true;
    }
    
    public function getAssets()
    {
        return $this->assets;
    }
    
    public function setAssets(\Doctrine\Common\Collections\ArrayCollection $assets)
    {
        $this->assets = $assets;
    }
    
    public function addAsset(Asset $asset)
    {
        $this->assets->add($asset);
    }
    
    public function getScenarios()
    {
        return $this->scenarios;
    }
    
    public function setScenarios(\Doctrine\Common\Collections\ArrayCollection $scenarios)
    {
        $this->scenarios = $scenarios;
    }
    
    public function addScenario(Scenario $scenario)
    {
        $this->scenarios->add($scenario);
    }

    /**
     * Get enabled
     *
     * @return boolean 
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set salt
     *
     * @param string $salt
     * @return User
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;
    
        return $this;
    }

    /**
     * Get termsAccepted
     *
     * @return boolean 
     */
    public function getTermsAccepted()
    {
        return $this->termsAccepted;
    }

    /**
     * Remove assets
     *
     * @param \GJIwww\Entities\Asset $assets
     */
    public function removeAsset(\GJIwww\Entities\Asset $assets)
    {
        $this->assets->removeElement($assets);
    }

    /**
     * Remove scenarios
     *
     * @param \GJIwww\Entities\Scenario $scenarios
     */
    public function removeScenario(\GJIwww\Entities\Scenario $scenarios)
    {
        $this->scenarios->removeElement($scenarios);
    }
    
    /**
     * Podaje sciezke do assetow tego uzytkownika
     * 
     * @param \Silex\Application $app
     * 
     * @return string
     */
    public function getAssetsPath(Application $app)
    {
        return $app['params.user_assets_path'] . '/' . $this->getId();
    }
}